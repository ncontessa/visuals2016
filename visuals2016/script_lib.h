//
//  script_lib.hpp
//  visuals2016mac
//
//  Created by Niccolò on 10/01/16.
//  Copyright © 2016 sonictooth. All rights reserved.
//

#ifndef script_lib_hpp
#define script_lib_hpp

int vsl_loadlib(lua_State *L);

#endif /* script_lib_hpp */
