#include <stdio.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <math.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include "file.h"
#include "gfx.h"
#include "log.h"

// HACK HACK HACK
void draw();



static int g_numScreenshots = 0;
static char g_screenshotTimestamp[128];
static const int SCREENSHOT_WIDTH = 1920 * 2;
static const int SCREENSHOT_HEIGHT = 1080 * 2;
static const int SCREENSHOT_COMPONENTS = 3;
static const int SCREENSHOT_SIZE = SCREENSHOT_WIDTH * SCREENSHOT_HEIGHT * SCREENSHOT_COMPONENTS;

static uint8_t g_fbData[SCREENSHOT_SIZE];
static uint8_t g_flippedBuffer[SCREENSHOT_SIZE];

static GLuint g_fbIdentifier;

void screenshot_init(const char *prefix) {
	char timestampFormat[] = "%Y%m%d%H%M%S";
	char tempString[128];
	time_t currentTime;
	struct tm *timeInfo;
	time(&currentTime);
	timeInfo = localtime(&currentTime);
	strftime(tempString, sizeof(tempString), timestampFormat, timeInfo);
	snprintf(g_screenshotTimestamp, sizeof(g_screenshotTimestamp), "%s_%s", prefix, tempString);

    assert(!glGetError());
	// The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
	glGenFramebuffers(1, &g_fbIdentifier);
    assert(!glGetError());
	glBindFramebuffer(GL_FRAMEBUFFER, g_fbIdentifier);
    assert(!glGetError());
	// The texture we're going to render to
	GLuint renderedTexture;
	glGenTextures(1, &renderedTexture);

	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, renderedTexture);
    assert(!glGetError());

	// Give an empty image to OpenGL ( the last "0" )
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, SCREENSHOT_WIDTH, SCREENSHOT_HEIGHT, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
    assert(!glGetError());

	// Poor filtering. Needed !
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    assert(!glGetError());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    assert(!glGetError());

	// Set "renderedTexture" as our colour attachement #0
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderedTexture, 0);
    assert(!glGetError());

	// Set the list of draw buffers.
	GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
    assert(!glGetError());
	glDrawBuffers(1, DrawBuffers); // "1" is the size of DrawBuffers
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		assert(0);
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

}

static void _flip_image(const uint8_t* input, int width, int height, int components, uint8_t* output) {
	for (int i = 0; i < height; i++) {
		const int rowSize = width * components;
		memcpy(output + (((height - 1) - i) * rowSize), input + (i * rowSize), rowSize);
	}
}

void screenshot_take() {
	double wasTime = glfwGetTime();
    const gfx_render_settings* settings = gfx_get_render_settings();
    int oldWidth = settings->fbWidth, oldHeight = settings->fbHeight;
    glViewport(0, 0, SCREENSHOT_WIDTH, SCREENSHOT_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, g_fbIdentifier);
//	draw();
	glFinish();
	glReadPixels(0, 0, SCREENSHOT_WIDTH, SCREENSHOT_HEIGHT, GL_RGB, GL_UNSIGNED_BYTE, g_fbData);
	char filename[128];
	snprintf(filename, sizeof(filename), "%s_%d.png", g_screenshotTimestamp, g_numScreenshots++);
	_flip_image(g_fbData, SCREENSHOT_WIDTH, SCREENSHOT_HEIGHT, SCREENSHOT_COMPONENTS, g_flippedBuffer);
	stbi_write_png(filename, SCREENSHOT_WIDTH, SCREENSHOT_HEIGHT, SCREENSHOT_COMPONENTS, g_flippedBuffer, 0);
    glViewport(0, 0, oldWidth, oldHeight);
	log_info("Saved screenshot %s in %f milliseconds...", filename, (glfwGetTime() - wasTime) * 1000.0);
	glfwSetTime(wasTime);
    
}
