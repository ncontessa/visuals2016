//
//  script_lib.cpp
//  visuals2016mac
//
//  Created by Niccolò on 10/01/16.
//  Copyright © 2016 sonictooth. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <lua.hpp>
#include "log.h"
#include "gfx.h"
#include "parameters.h"
#include "render.h"
#include "script_lib.h"


static render_command_buffer _rcb;

//static int vsl_bind_program(program_t *program);
//static int vsl_bind_texture(int unit, texture_t *texture);
//static int vsl_bind_uniform(program_t *program, const char* name, void* value);
//static int vsl_bind_uniform_int(program_t *program, const char* name, void* value);
//static int vsl_bind_framebuffer(framebuffer_t *framebuffer);
//static int vsl_render_fullscreen_quad();
//static int vsl_framebuffer_get(); // returns framebuffer_t *
//static int vsl_program_get(const char* name); // returns program_t *
//static int vsl_texture_get(const char* name); // returns texture_t *
//static int vsl_texture_from_framebuffer(framebuffer_t *framebuffer); // returns texture_t*
//static int vsl_log(const char* string);
//static int vsl_reset_frame();
//static int vsl_render();

static int vsl_bind_program(lua_State *L) {
    program_t *program = (program_t*)lua_touserdata(L, 1);
    render_command_bind_program(&_rcb, program);
    return 0;
}
static int vsl_bind_texture(lua_State *L) {
    int unit = (int)luaL_checknumber(L, 1);
    texture_t *texture = (texture_t*)lua_touserdata(L, 2);
    render_command_bind_texture(&_rcb, unit, texture);
    return 0;
}

/* assume that table is on the stack top */
static double _getfield (lua_State *L, int key) {
    double result;
    lua_pushnumber(L, key);
    lua_gettable(L, -2);  /* get background[key] */
    if (!lua_isnumber(L, -1))
        log_fatal("invalid component in background color");
    result = lua_tonumber(L, -1);
    lua_pop(L, 1);  /* remove number */
    return result;
}

static int vsl_bind_uniform(lua_State *L) {
    program_t *program = (program_t*)lua_touserdata(L, 1);
    const char* name = luaL_checkstring(L, 2);
    if(lua_type(L, 3) == LUA_TNUMBER) {
        render_command_bind_uniform1f(&_rcb, program, name, luaL_checknumber(L, 3));
    } else if(lua_type(L, 3) == LUA_TTABLE) {
        lua_len(L, 3);
        int tableLength = (int)luaL_checknumber(L, 4);
        lua_pop(L, 1);
        if(tableLength == 2) {
            render_command_bind_uniform2f(&_rcb, program, name,
                                          _getfield(L, 1),
                                          _getfield(L, 2));
            
        } else if(tableLength == 3) {
            render_command_bind_uniform3f(&_rcb, program, name,
                                          _getfield(L, 1),
                                          _getfield(L, 2),
                                          _getfield(L, 3));
            
        } else if(tableLength == 4) {
            render_command_bind_uniform4f(&_rcb, program, name,
                                          _getfield(L, 1),
                                          _getfield(L, 2),
                                          _getfield(L, 3),
                                          _getfield(L, 4));
            
        } else {
            return luaL_error(L, "invalid args to vsl_bind_uniform");
        }
    } else {
		return luaL_error(L, "invalid args to vsl_bind_uniform");
	}
    return 0;
}
static int vsl_bind_uniform_int(lua_State *L) {
    program_t *program = (program_t*)lua_touserdata(L, 1);
    const char* name = luaL_checkstring(L, 2);
    render_command_bind_uniform1i(&_rcb, program, name, (int)luaL_checknumber(L, 3));
    return 0;
}

static int vsl_bind_framebuffer(lua_State *L) {
    framebuffer_t* fb = (framebuffer_t*)lua_touserdata(L, 1);
    render_command_bind_framebuffer(&_rcb, fb);
    return 0;
}

static int vsl_render_fullscreen_quad(lua_State *L) {
    render_command_fullscreen_quad(&_rcb);
    return 0;
}

static int vsl_framebuffer_get(lua_State *L) {
    lua_pushlightuserdata(L, (void*)framebuffer_get());
    return 1;
}
static int vsl_program_get(lua_State *L) {
    const char *name = luaL_checkstring(L, 1);
    program_t *result = program_get(name);
    if(!result) {
		lua_pushstring(L, "Error loading program");
		lua_error(L);
		return 1;
	}
    lua_pushlightuserdata(L, result);
    return 1;
}
 // returns program_t *
static int vsl_texture_get(lua_State *L) {
    const char *name = luaL_checkstring(L, 1);
    texture_t *result = texture_get(name);
    if(!result) {
		lua_pushstring(L, "Error loading texture");
		lua_error(L);
		return 1;
	}
    lua_pushlightuserdata(L, result);
    lua_pushnumber(L, result->width);
    lua_pushnumber(L, result->height);
    return 3;
}
 // returns texture_t *
static int vsl_texture_from_framebuffer(lua_State *L) {
    framebuffer_t* fb = (framebuffer_t*)lua_touserdata(L, 1);
	if (fb) {
		lua_pushlightuserdata(L, fb->texture);
	}
	else {
		lua_pushlightuserdata(L, NULL);
	}
    return 1;
}

static int vsl_log(lua_State *L) {
    const char* s = luaL_checkstring(L, 1);
    log_info("%s", s);
    return 0;
}

static int vsl_reset_frame(lua_State *L) {
//    log_debug("vsl_reset_frame");
    render_command_buffer_reset(&_rcb);
    return 0;
}

static int vsl_render(lua_State *L) {
    render_command_buffer_render(&_rcb);
//    render_command_buffer_dump(&_rcb);
    return 0;
}

static int vsl_framebuffer_default(lua_State *L) {
    lua_pushlightuserdata(L, NULL);
    return 1;
}

static int vsl_texture_null(lua_State *L) {
	lua_pushlightuserdata(L, NULL);
	return 1;
}

static int vsl_parameter_set_value(lua_State *L) {
    parameter_t *parameter = (parameter_t*)lua_touserdata(L, 1);
    
    switch(parameter->type) {
        case PARAM_FLOAT:
            parameter->value.x = luaL_checknumber(L, 2);
            break;
        case PARAM_COLOR3F:
        case PARAM_DIRECTION3F:
            parameter->value.v3.x = _getfield(L, 1);
            parameter->value.v3.y = _getfield(L, 2);
            parameter->value.v3.z = _getfield(L, 3);
            break;
        case PARAM_COLOR4F:
            parameter->value.v4.x = _getfield(L, 1);
            parameter->value.v4.y = _getfield(L, 2);
            parameter->value.v4.z = _getfield(L, 3);
            parameter->value.v4.w = _getfield(L, 4);
            break;
        default:
            log_fatal("unimplemented type %d in vsl_parameter_get_value", parameter->type);
            break;
    }
    return 0;
}


typedef struct {
    const char* name;
    int value;
} parameterMapping_t;

const static parameterMapping_t mappings[] = {
    {"float", PARAM_FLOAT},
    {"dir3f", PARAM_DIRECTION3F},
    {"color3f", PARAM_COLOR3F},
    {"color4f", PARAM_COLOR4F}
};

static double _get_field_default(lua_State *L, int tableIndex, const char* fieldName, double defaultValue) {
    double result = defaultValue;
    lua_getfield(L, tableIndex, fieldName);
    if(!lua_isnoneornil(L, -1)) {
        result = lua_tonumber(L, -1);
    }
    lua_pop(L, 1);
    return result;
}

static int vsl_parameter_create(lua_State *L) {
    int type = -1;
    const char* name = luaL_checkstring(L, 1);
    const char* typeName = luaL_checkstring(L, 2);
    
    double min = 0.0;
    double max = 10.0;
    double step = 0.1;
    
    for(int i = 0; i < sizeof(mappings) / sizeof(parameterMapping_t); i++) {
        if(!strcmp(typeName, mappings[i].name)) {
            type = mappings[i].value;
        }
    }
    if(type == -1) {
        log_error("vsl_parameter_create: invalid typeName %s", typeName);
        lua_pushnil(L);
        return 1;
    }
    
    char options[64] = "";
    if(lua_istable(L, 4)) {
        min = _get_field_default(L, 4, "min", min);
        max = _get_field_default(L, 4, "max", max);
        step = _get_field_default(L, 4, "step", step);
    }
	if (type == PARAM_FLOAT) {
		snprintf(options, sizeof(options), " min=%f max=%f step=%f", min, max, step);
	}
    
    parameter_t *result = parameter_get(name, type, options);
    if(!result) {
        log_error("vsl_parameter_create: couldn't create parameter");
        lua_pushnil(L);
        return 1;
    }
    
    if(!lua_isnoneornil(L, 3)) {
        lua_pushcfunction(L, vsl_parameter_set_value);
        lua_pushlightuserdata(L, result);
        lua_pushvalue(L, 3);
        lua_pcall(L, 2, 0, 0);
    }

    
    lua_pushlightuserdata(L, result);
  
    return 1;
}


static void _setfield(lua_State *L, int index, float value) {
    lua_pushnumber(L, index);
    lua_pushnumber(L, value);
    lua_settable(L, -3);
}

static int vsl_parameter_get_value(lua_State *L) {
    parameter_t *parameter = (parameter_t*)lua_touserdata(L, 1);

    switch(parameter->type) {
        case PARAM_FLOAT:
            lua_pushnumber(L, parameter->value.x);
        break;
        case PARAM_COLOR3F:
        case PARAM_DIRECTION3F:
            lua_newtable(L);
            _setfield(L, 1, parameter->value.v3.x);
            _setfield(L, 2, parameter->value.v3.y);
            _setfield(L, 3, parameter->value.v3.z);
            break;
        case PARAM_COLOR4F:
            lua_newtable(L);
            _setfield(L, 1, parameter->value.v4.x);
            _setfield(L, 2, parameter->value.v4.y);
            _setfield(L, 3, parameter->value.v4.z);
            _setfield(L, 4, parameter->value.v4.w);
            break;
        default:
            log_fatal("unimplemented type %d in vsl_parameter_get_value", parameter->type);
            break;
    }
    return 1;
}

static int vsl_framebuffer_static_get(lua_State *L) {
    int width = (int)luaL_checknumber(L, 1);
    int height = (int)luaL_checknumber(L, 2);
    bool nearest = lua_isboolean(L, 3) ? lua_toboolean(L, 3) : false;
    const framebuffer_t* result = framebuffer_get_static(width, height, nearest);
    lua_pushlightuserdata(L, (void*)result);
    return 1;
}

static const luaL_Reg _vsl_lib[] = {
    {"bind_program", vsl_bind_program},
    {"bind_texture", vsl_bind_texture},
    {"bind_uniform", vsl_bind_uniform},
    {"bind_uniform_int", vsl_bind_uniform_int},
    {"bind_framebuffer", vsl_bind_framebuffer},
    {"render_fullscreen_quad", vsl_render_fullscreen_quad},
    {"framebuffer_get", vsl_framebuffer_get},
    {"program_get", vsl_program_get},
    {"texture_get", vsl_texture_get},
    {"texture_from_framebuffer", vsl_texture_from_framebuffer},
    {"log", vsl_log},
    {"reset_frame", vsl_reset_frame},
    {"render", vsl_render},
    {"framebuffer_default", vsl_framebuffer_default},
    {"parameter_create", vsl_parameter_create},
    {"parameter_get", vsl_parameter_get_value},
    {"parameter_set", vsl_parameter_set_value},
	{"texture_null", vsl_texture_null},
    {"framebuffer_static_get", vsl_framebuffer_static_get},
    {NULL, NULL}  /* sentinel */
};


int vsl_loadlib(lua_State *L) {
    luaL_newlib(L, _vsl_lib);
    lua_setglobal(L, "vsl");
    return 1;
}

