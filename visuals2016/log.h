#ifndef __LOG_H__
#define __LOG_H__


void log_init(const char* filename);
void log_close();
void log_info(const char* formatString, ...);
void log_warning(const char* formatString, ...);
void log_error(const char* formatString, ...);
void log_fatal(const char* formatString, ...);
void log_debug(const char* formatString, ...);

#endif