//
//  render.h
//  visuals2016mac
//
//  Created by Niccolò on 04/01/16.
//  Copyright © 2016 sonictooth. All rights reserved.
//

#ifndef render_h
#define render_h

const int RENDER_COMMAND_BUFFER_SIZE = 65536;
typedef struct {
    uint8_t buffer[RENDER_COMMAND_BUFFER_SIZE];
    uint8_t *pointer;
} render_command_buffer;

enum {
    RENDER_COMMAND_BIND_FRAMEBUFFER = 1,
    RENDER_COMMAND_BIND_TEXTURE,
    RENDER_COMMAND_BIND_PROGRAM,
    RENDER_COMMAND_BIND_UNIFORM_FLOAT,
    RENDER_COMMAND_BIND_UNIFORM_FLOAT2,
    RENDER_COMMAND_BIND_UNIFORM_FLOAT3,
    RENDER_COMMAND_BIND_UNIFORM_FLOAT4,
    RENDER_COMMAND_RENDER_FULLSCREEN_QUAD,
    RENDER_COMMAND_BIND_UNIFORM_INT
};

void render_init();
void render_fullscreen_quad();

void render_command_buffer_reset(render_command_buffer *rcb);
void render_command_buffer_push_int(render_command_buffer *rcb, int value);
void render_command_buffer_push_float(render_command_buffer *rcb, float value);
void render_command_buffer_push_pointer(render_command_buffer *rcb, void* pointer);
void render_command_buffer_render(render_command_buffer *rcb);

void render_command_bind_framebuffer(render_command_buffer *rcb, const framebuffer_t *framebuffer);
void render_command_bind_texture(render_command_buffer *rcb, int target, const texture_t *texture);
void render_command_bind_program(render_command_buffer *rcb, const program_t *program);
void render_command_bind_uniform1f(render_command_buffer *rcb, const program_t *program, const char* uniformName, float x);
void render_command_bind_uniform1i(render_command_buffer *rcb, const program_t *program, const char* uniformName, int i);
void render_command_bind_uniform2f(render_command_buffer *rcb, const program_t *program, const char* uniformName, float x, float y);
void render_command_bind_uniform2f(render_command_buffer *rcb, int location, float x, float y);

void render_command_bind_uniform3f(render_command_buffer *rcb, const program_t *program, const char* uniformName, float x, float y, float z);

void render_command_bind_uniform4f(render_command_buffer *rcb, const program_t *program, const char* uniformName, float x, float y, float z, float w);
void render_command_fullscreen_quad(render_command_buffer *rcb);

void render_command_buffer_dump(render_command_buffer *rcb);

#endif /* render_h */
