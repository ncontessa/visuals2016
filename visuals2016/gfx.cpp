#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <math.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <memory.h>
#include "log.h"
#include "file.h"
#include "gfx.h"
#include "stb_image.h"
#include "config.h"

static const int MAX_TEXTURES = 512;
static texture_t _textures[MAX_TEXTURES] = { 0 };
static int _numTextures = 0;
const int MAX_PROGRAMS = 128;
static program_t _programs[MAX_PROGRAMS] = { 0 };
static int _numPrograms = 0;
const int MAX_FRAMEBUFFERS = 64;
static framebuffer_t _framebuffers[MAX_FRAMEBUFFERS] = { 0 };
static int _numFramebuffers = 0;
const int MAX_STATIC_FRAMEBUFFERS = 256;
static framebuffer_t _staticFramebuffers[MAX_STATIC_FRAMEBUFFERS] = { 0 };
static int _numStaticFramebuffers = 0;
static int _generatedTexture = 0;
static int _maxFramebuffers = 0;

static GLuint _tonemappingTexture;
static const char *_tonemappingTextureFilename = "data/textures/rgb_siennablue.tga";

void shader_free(GLuint shader) {
	glDeleteShader(shader);
}

void program_free(program_t* program) {
	if (program) {
		if (program->vertexShader) {
			glDeleteShader(program->vertexShader);
		}
		if (program->fragmentShader) {
			glDeleteShader(program->fragmentShader);
		}
		if (program->handle) {
			glDeleteProgram(program->handle);
		}
		memset(program, 0, sizeof(*program));
	}
}

GLuint shader_load(const char* filename, GLenum shaderType) {
	log_info("Loading shader %s...", filename);

	char *data = file_read_as_string(filename);
	if (!data) {
		log_error("File %s not found!", filename);
		return 0;
	}
	GLuint vs = glCreateShader(shaderType);
	glShaderSource(vs, 1, &data, NULL);
	glCompileShader(vs);
	GLint result;
	glGetShaderiv(vs, GL_COMPILE_STATUS, &result);
	if (result == GL_FALSE) {
		assert(!glGetError());
		char infoLog[1024];
		glGetShaderInfoLog(vs, sizeof(infoLog), NULL, infoLog);
		log_error("Error compiling shader: %s", infoLog);
		shader_free(vs);
		return 0;
	}

	free(data);
	return vs;
}

#define _GL_TYPE_NAME(typ) case typ: return #typ; break;
static const char *_get_gl_type_name(GLenum type) {
	switch (type) {
		_GL_TYPE_NAME(GL_FLOAT)
			_GL_TYPE_NAME(GL_FLOAT_VEC2)
			_GL_TYPE_NAME(GL_FLOAT_VEC3)
			_GL_TYPE_NAME(GL_FLOAT_VEC4)
			_GL_TYPE_NAME(GL_FLOAT_MAT2)
			_GL_TYPE_NAME(GL_FLOAT_MAT3)
			_GL_TYPE_NAME(GL_FLOAT_MAT4)
			_GL_TYPE_NAME(GL_FLOAT_MAT2x3)
			_GL_TYPE_NAME(GL_FLOAT_MAT2x4)
			_GL_TYPE_NAME(GL_FLOAT_MAT3x2)
			_GL_TYPE_NAME(GL_FLOAT_MAT3x4)
			_GL_TYPE_NAME(GL_FLOAT_MAT4x2)
			_GL_TYPE_NAME(GL_FLOAT_MAT4x3)
			_GL_TYPE_NAME(GL_INT)
			_GL_TYPE_NAME(GL_INT_VEC2)
			_GL_TYPE_NAME(GL_INT_VEC3)
			_GL_TYPE_NAME(GL_INT_VEC4)
			_GL_TYPE_NAME(GL_UNSIGNED_INT)
			_GL_TYPE_NAME(GL_UNSIGNED_INT_VEC2)
			_GL_TYPE_NAME(GL_UNSIGNED_INT_VEC3)
			_GL_TYPE_NAME(GL_UNSIGNED_INT_VEC4)
			_GL_TYPE_NAME(GL_DOUBLE)
			_GL_TYPE_NAME(GL_DOUBLE_VEC2)
			_GL_TYPE_NAME(GL_DOUBLE_VEC3)
			_GL_TYPE_NAME(GL_DOUBLE_VEC4)
			_GL_TYPE_NAME(GL_DOUBLE_MAT2)
			_GL_TYPE_NAME(GL_DOUBLE_MAT3)
			_GL_TYPE_NAME(GL_DOUBLE_MAT4)
			_GL_TYPE_NAME(GL_DOUBLE_MAT2x3)
			_GL_TYPE_NAME(GL_DOUBLE_MAT2x4)
			_GL_TYPE_NAME(GL_DOUBLE_MAT3x2)
			_GL_TYPE_NAME(GL_DOUBLE_MAT3x4)
			_GL_TYPE_NAME(GL_DOUBLE_MAT4x2)
			_GL_TYPE_NAME(GL_DOUBLE_MAT4x3)
            _GL_TYPE_NAME(GL_SAMPLER_2D)
            _GL_TYPE_NAME(GL_SAMPLER_CUBE)
            
	};
    return "UNKNOWN";
}

#undef _GL_TYPE_NAME

bool _printProgramInfo = false;

bool program_load(const char* path, program_t *result) {
	memset(result, 0, sizeof(*result));
	char fullPath[128];
	log_info("Loading program %s...", path);
	result->handle = 0;
	snprintf(fullPath, 128, "%s_vert.glsl", path);
	GLuint shader = shader_load(fullPath, GL_VERTEX_SHADER);
	if (!shader) {
		program_free(result);
		return false;
	}
	assert(!glGetError());
	result->vertexShader = shader;
	snprintf(fullPath, 128, "%s_frag.glsl", path);
	shader = shader_load(fullPath, GL_FRAGMENT_SHADER);
	if (!shader) {
		program_free(result);
		return false;
	}
	assert(!glGetError());
	result->fragmentShader = shader;
	result->handle = glCreateProgram();
	if (!result->handle) {
		program_free(result);
		return false;
	}
	assert(!glGetError());
	glAttachShader(result->handle, result->vertexShader);
	glAttachShader(result->handle, result->fragmentShader);
	glLinkProgram(result->handle);

	GLint testVal;
	glGetProgramiv(result->handle, GL_LINK_STATUS, &testVal);
	if (testVal == GL_FALSE) {
		char log[1024];
		glGetError();
		glGetProgramInfoLog(result->handle, 1024, NULL, log);
		log_error("Error linking program:\n%s", log);
		program_free(result);
		return false;
	}
	glGetProgramiv(result->handle, GL_ACTIVE_ATTRIBUTES, &result->nAttributes);
	if (result->nAttributes > PROGRAM_MAX_ATTRIBUTES) {
		log_warning("program has %d attributes > PROGRAM_MAX_ATTRIBUTES = %d, truncating...", result->nAttributes, PROGRAM_MAX_ATTRIBUTES);
		result->nAttributes = PROGRAM_MAX_ATTRIBUTES;
	}

	for (int i = 0; i < result->nAttributes; i++) {
		int length;
		glGetActiveAttrib(result->handle, i, MAX_NAME_SIZE, &length, &result->attributes[i].size, &result->attributes[i].type, result->attributes[i].name);
		if (length > MAX_NAME_SIZE) {
			assert(0);
		}
		result->attributes[i].location = glGetAttribLocation(result->handle, result->attributes[i].name);

		if (_printProgramInfo) {
			log_info("Attribute #%d '%s': size %d type %s location %d", i, result->attributes[i].name, result->attributes[i].size, _get_gl_type_name(result->attributes[i].type), result->attributes[i].location);
		}
	}
	glGetProgramiv(result->handle, GL_ACTIVE_UNIFORMS, &result->nUniforms);
	if (result->nUniforms> PROGRAM_MAX_UNIFORMS) {
		log_warning("program has %d uniforms > PROGRAM_MAX_UNIFORMS = %d, truncating...", result->nUniforms, PROGRAM_MAX_ATTRIBUTES);
		result->nUniforms = PROGRAM_MAX_UNIFORMS;
	}
	for (int i = 0; i < result->nUniforms; i++) {
		int length;
		glGetActiveUniform(result->handle, i, MAX_NAME_SIZE, &length, &result->uniforms[i].size, &result->uniforms[i].type, result->uniforms[i].name);
		if (length > MAX_NAME_SIZE) {
			assert(0);
		}
		result->uniforms[i].location = glGetUniformLocation(result->handle, result->uniforms[i].name);
		if (_printProgramInfo) {
			log_info("Uniform #%d '%s': size %d type %s location %d", i, result->uniforms[i].name, result->uniforms[i].size, _get_gl_type_name(result->uniforms[i].type), result->uniforms[i].location);
		}
	}
    
    strncpy(result->name, path, sizeof(result->name));
	return true;
}

GLuint program_get_attrib_location(const program_t* program, const char* name) {
	for (int i = 0; i < program->nAttributes; i++) {
		if (!strcmp(program->attributes[i].name, name)) {
			return program->attributes[i].location;
		}
	}
#ifdef DEBUG
    log_warning("Attribute %s not found in program %s", name, program->name);
#endif
    return 0xFFFFFFFF;
}

GLuint program_get_uniform_location(const program_t* program, const char* name) {
	for (int i = 0; i < program->nUniforms; i++) {
		if (!strcmp(program->uniforms[i].name, name)) {
			return program->uniforms[i].location;
		}
	}
#ifdef DEBUG
    log_warning("Uniform %s not found in program %s", name, program->name);
#endif
    return 0xFFFFFFFF;
}

const char *program_get_uniform_name_from_location(const program_t *program, int location) {
    for(int i = 0; i < PROGRAM_MAX_UNIFORMS; i++) {
        if(program->uniforms[i].location == location) {
            return program->uniforms[i].name;
        }
    }
    return "<not found>";
}

const char *program_get_attribute_name_from_location(const program_t *program, int location) {
    for(int i = 0; i < PROGRAM_MAX_ATTRIBUTES; i++) {
        if(program->attributes[i].location == location) {
            return program->attributes[i].name;
        }
    }
    return "<not found>";
}

static gfx_render_settings _render_settings;

const gfx_render_settings* gfx_get_render_settings() {
    return &_render_settings;
}



static GLint _gl_format_from_components(int components) {
    switch(components) {
        case 1:
            return GL_RED;
            break;
        case 2:
            return GL_RG;
            break;
        case 3:
            return GL_RGB;
            break;
        case 4:
            return GL_RGBA;
            break;
        default:
            assert(0);
            return -1;
            break;
    }
}

bool texture_alloc(texture_t* result, const char* name, int width, int height, int components, int filter, int wrap, bool srgb = false) {
    GLuint texId;
    glGenTextures(1, &texId);
    if(glGetError()) {
        return false;
    }
    glBindTexture(GL_TEXTURE_2D, texId);
    if(glGetError()) {
        return false;
    }
    if(wrap == WRAP_REPEAT) {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    } else if(wrap == WRAP_CLAMP) {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    }
    
    if(filter == FILTER_LINEAR) {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    } else if(filter == FILTER_MIPMAP) {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    } else if(filter == FILTER_NEAREST) {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    }
    
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
//    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    GLint internalFormat;
    if(srgb) {
        internalFormat = components == 3 ? GL_SRGB8 : GL_SRGB8_ALPHA8;
    } else {
        internalFormat = components == 3 ? GL_RGB8 : GL_RGBA8;
    }
    glTexImage2D(GL_TEXTURE_2D, 0,
                 internalFormat,
                 width,
                 height,
                 0,
                 _gl_format_from_components(components),
                 GL_UNSIGNED_BYTE,
                 NULL);
//    glGenerateMipmap(GL_TEXTURE_2D);

    if(glGetError()) {
        return false;
    }
    result->texId = texId;
    strncpy(result->name, name, sizeof(result->name));
    result->components = components;
    result->width = width;
    result->height = height;
    result->filter = filter;
    result->wrap = wrap;
    return true;
}


void texture_upload_data(const texture_t *result, void *data) {
    glBindTexture(GL_TEXTURE_2D, result->texId);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0,
                    result->width, result->height,
                    _gl_format_from_components(result->components),
                    GL_UNSIGNED_BYTE, data);
    if(result->filter == FILTER_MIPMAP) {
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    assert(!glGetError());
}

texture_t *texture_get(const char *filename, int filter, int wrap) {
    assert(_numTextures < MAX_TEXTURES);
    for(int i = 0; i < _numTextures; i++) {
        if(!strncmp(filename, _textures[i].name, sizeof(_textures[i].name))) {
            log_info("Already loaded texture %s...", filename);
            return &_textures[i];
        }
    }
    
    log_info("Loading texture %s...", filename);
    int w, h, c;
    uint8_t *data = (uint8_t*)stbi_load(filename, &w, &h, &c, 0);
    if(!data) {
        log_fatal("Error loading texture %s...", filename);
        return NULL;
    }
    log_info("Texture %s: %d x %d, %s", filename, w, h, c == 3 ? "RGB" : "RGBA");
    texture_t *result = &_textures[_numTextures];
    if(!texture_alloc(result, filename, w, h, c, filter, wrap, false)) {
        log_error("Error loading texture %s...");
        stbi_image_free(data);
        return NULL;
    }
    texture_upload_data(result, data);
    stbi_image_free(data);
    _numTextures++;
    return result;
}

texture_t *texture_get(int width, int height, int components, int filter, int wrap) {
    assert(_numTextures < MAX_TEXTURES);
    char name[32];
    snprintf(name, sizeof(name), "#generated_%d#", _generatedTexture++);
    texture_t *result = &_textures[_numTextures];
    if(texture_alloc(&_textures[_numTextures], name, width, height, components, filter, wrap)) {
        _numTextures++;
        return result;
    } else {
        return NULL;
    }
}



static void _alloc_framebuffer(framebuffer_t *result, int width, int height, bool filterNearest) {
    assert(!glGetError());
    GLuint fbId;
    // The framebuffer, which regroups 0, 1, or more textures, and 0 or 1 depth buffer.
    glGenFramebuffers(1, &fbId);
    assert(!glGetError());
    glBindFramebuffer(GL_FRAMEBUFFER, fbId);
    assert(!glGetError());

    texture_t *texture;
    if(result->texture == NULL) {
        texture = texture_get(width, height, 4, filterNearest ? FILTER_NEAREST : FILTER_LINEAR, WRAP_CLAMP);
    } else {
        texture_alloc(result->texture, result->texture->name, width, height, 4, result->texture->filter, result->texture->wrap);
        texture = result->texture;
    }
    assert(texture);
    // "Bind" the newly created texture : all future texture functions will modify this texture
    glBindTexture(GL_TEXTURE_2D, texture->texId);
    assert(!glGetError());
    // Poor filtering. Needed !
    if(filterNearest) {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        assert(!glGetError());
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        assert(!glGetError());
    } else {
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        assert(!glGetError());
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        assert(!glGetError());
    }
    
    // Set "renderedTexture" as our colour attachement #0
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, texture->texId, 0);
    assert(!glGetError());
    
    // Set the list of draw buffers.
    GLenum DrawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
    assert(!glGetError());
    glDrawBuffers(1, DrawBuffers); // "1" is the size of DrawBuffers
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
        assert(0);
    }

	glClearColor(.0f, .0f, .0f, .0f);
	glClear(GL_COLOR_BUFFER_BIT);
	assert(!glGetError());
    
    result->texture = texture;
    result->fbId = fbId;
    
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


const framebuffer_t *framebuffer_get_static(int width, int height, bool filterNearest) {
    assert(_numStaticFramebuffers < MAX_STATIC_FRAMEBUFFERS);
    framebuffer_t *result = &_staticFramebuffers[_numStaticFramebuffers];
    
    _alloc_framebuffer(result, width, height, filterNearest);
    
    _numStaticFramebuffers++;
    return result;
}


static void _alloc_framebuffers() {
    const gfx_render_settings *settings = gfx_get_render_settings();
    for(int i = 0; i < MAX_FRAMEBUFFERS; i++) {
        _alloc_framebuffer(&_framebuffers[i], script_get_config()->effectiveWidth, script_get_config()->effectiveHeight, false);
    }
    
}


static void _free_framebuffers() {
    for(int i = 0; i < MAX_FRAMEBUFFERS; i++) {
        if(_framebuffers[i].fbId) {
            glDeleteFramebuffers(1, &(_framebuffers[i].fbId));
            _framebuffers[i].fbId = 0;
        }
    }
}

void framebuffer_reset() {
    _numFramebuffers = 0;
}

const framebuffer_t* framebuffer_get() {
    assert(_numFramebuffers < MAX_FRAMEBUFFERS);
	if (_numFramebuffers + 1 > _maxFramebuffers) {
		_maxFramebuffers = _numFramebuffers + 1;
		log_info("maxFramebuffers = %d", _maxFramebuffers);
	}
	return &_framebuffers[_numFramebuffers++];
}

static void _tonemapping_load() {
	log_info("loading tonemapping texture %s...", _tonemappingTextureFilename);
	glActiveTexture(GL_TEXTURE8);

	glGenTextures(1, &_tonemappingTexture);
	glBindTexture(GL_TEXTURE_3D, _tonemappingTexture);
	int w, h, c;
	uint8_t *data = stbi_load(_tonemappingTextureFilename, &w, &h, &c, 3);
	if (!data) {
		log_fatal("couldn't find texture %s", _tonemappingTextureFilename);
		return;
	}
	glTexImage3D(GL_TEXTURE_3D, 0, GL_RGB, h, h, h, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	stbi_image_free(data);
	glActiveTexture(GL_TEXTURE0);
	assert(!glGetError());
}

void gfx_init() {
    log_info("gfx_init()");
	_tonemapping_load();
	stbi_set_flip_vertically_on_load(1);
}

void gfx_free_assets() {
	log_info("gfx_free_assets()...");
//	assert(!glGetError());
	_free_framebuffers();
    for(int i = 0; i < _numStaticFramebuffers; i++){
        if(_staticFramebuffers[i].fbId) {
            glDeleteFramebuffers(1, &_staticFramebuffers[i].fbId);
        }
    }
    
    
    for(int i = 0; i < _numTextures; i++) {
        if(_textures[i].texId) {
            glDeleteTextures(1, &_textures[i].texId);
        }
    }
    
    for(int i = 0; i < _numPrograms; i++) {
        if(_programs[i].handle) {
            glDeleteProgram(_programs[i].handle);
        }
        if(_programs[i].vertexShader) {
            glDeleteShader(_programs[i].vertexShader);
        }
        if(_programs[i].fragmentShader) {
            glDeleteShader(_programs[i].fragmentShader);
        }
    }

    _numStaticFramebuffers = 0;
    _numTextures = 0;
	_numFramebuffers = 0;
	_numPrograms = 0;
	_generatedTexture = 0;
	memset(_textures, 0, sizeof(_textures));
	memset(_programs, 0, sizeof(_programs));
	memset(_framebuffers, 0, sizeof(_framebuffers));
    memset(_staticFramebuffers, 0, sizeof(_staticFramebuffers));
    _alloc_framebuffers();

}

void gfx_reset(int fbWidth, int fbHeight, int windowWidth, int windowHeight) {
	log_info("gfx_reset(%d, %d, %d, %d)", fbWidth, fbHeight, windowWidth, windowHeight);
    if(!_render_settings.forceVirtual) {
        _render_settings.virtualWidth = windowWidth;
        _render_settings.virtualHeight = windowHeight;
    }
    _render_settings.fbWidth = fbWidth;
    _render_settings.fbHeight = fbHeight;
    _render_settings.windowWidth = windowWidth;
    _render_settings.windowHeight = windowHeight;

    glViewport(0, 0, _render_settings.fbWidth, _render_settings.fbHeight);
    _free_framebuffers();
    _alloc_framebuffers();
    
}

void gfx_config(bool forceVirtual, int virtualWidth, int virtualHeight) {
    _render_settings.forceVirtual = forceVirtual;
    _render_settings.virtualWidth = virtualWidth;
    _render_settings.virtualHeight = virtualHeight;
}


program_t* program_get(const char *filename) {
    for(int i = 0; i < _numPrograms; i++) {
        if(!strncmp(filename, _programs[i].name, sizeof(_programs[i].name))) {
            return &(_programs[i]);
        }
    }
    assert(_numPrograms < MAX_PROGRAMS);
    program_t* result = &_programs[_numPrograms];
    if(program_load(filename, result)) {
        _numPrograms++;
        return result;
    } else {
        return NULL;
    }
}



