dofile("tableprint.lua")
vsl = {}
mt = {}
local index = 1
mt.__index = function(table, key)
   return function(...)
      local res = key .. "("

      for i, v in ipairs({...}) do
	 if(type(v) == "table") then
	    res = res .. "{ "
	    for i, e in ipairs(v) do
	       res = res .. e .. " "
	    end
	    res = res .. "}"
	 else
	    res = res .. v
	 end
	 
	 if i ~= #{...} then
	    res = res .. ", "
	 end
      end
      index = index + 1
      -- HACK HACK HACK
      if string.find(key, "_get") or string.find(key, "framebuffer_default") then
	 print(res .. ") ~-> #" .. index)
      else
	 print(res .. ")")
      end
      return "#"..index, 2, 3
   end
end

setmetatable(vsl, mt)

vsl.parameter_get = function () return 1.0 end

