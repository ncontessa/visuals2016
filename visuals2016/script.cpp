//
//  script.cpp
//  visuals2016mac
//
//  Created by Niccolò on 10/01/16.
//  Copyright © 2016 sonictooth. All rights reserved.
//

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <lua.hpp>
#include "log.h"
#include "gfx.h"
#include "script.h"
#include "midi.h"
#include "script_lib.h"
#include "config.h"

static lua_State *_L;
static config_t _config;

#define DEBUG_SCRIPT

int script_init() {
	if (_L) {
		log_warning("script_init() called with LUA already loaded...");
		script_shutdown();
	}

    _L = luaL_newstate();
    luaL_openlibs(_L);
    vsl_loadlib(_L);

    return 1;
}

static double _get_config_number(lua_State *L, const char* name) {
	lua_getfield(L, -1, name);

	if (lua_isnil(L, -1)) {
		log_warning("Field %s in config is nil or not set", name);
		return 0;
	}
	if (!lua_isnumber(L, -1)) {
		log_fatal("Field %s in config is not a number", name);
		return 0;
	}
	return lua_tonumber(L, -1);
}

static bool _get_config_boolean(lua_State *L, const char* name) {
	lua_getfield(L, -1, name);
	if (lua_isnil(L, -1)) {
		log_warning("Field %s in config is nil or not set", name);
		return false;
	}
	
	if (!lua_isboolean(L, -1)) {
		log_fatal("Error: field %s in config is not a number", name);
		return false;
	}
	return lua_toboolean(L, -1);
}

int script_load_assets() {
	if (!_L) {
		log_warning("script_load_assets() called without calling script_init() first");
		return 0;
	}
	gfx_free_assets();
	script_load_config();
	int error = luaL_dofile(_L, "data/scripts/init.lua");
	if (error) {
		log_error("%s", lua_tostring(_L, -1));
		lua_pop(_L, 1);  /* pop error message from the stack */
		return 0;
	}
	return 1;
}

int script_load_config() {
	if (!_L) {
		log_warning("script_load_config() called without calling script_init() first");
		return 0;
	}
	luaL_dostring(_L, "config = {}");
	int error = luaL_dofile(_L, "data/scripts/config.lua");
	if (error) {
		log_error("%s", lua_tostring(_L, -1));
		lua_pop(_L, 1);  /* pop error message from the stack */
		return 0;
	}

	lua_getglobal(_L, "config");

#define GET_CONFIG_INT(_name) \
	_config._name = (int)_get_config_number(_L, #_name); lua_pop(_L, 1);

#define GET_CONFIG_BOOLEAN(_name) \
	_config._name = _get_config_boolean(_L, #_name); lua_pop(_L, 1);

	GET_CONFIG_INT(virtualWidth);
	GET_CONFIG_INT(virtualHeight);
	GET_CONFIG_INT(fbWidth);
	GET_CONFIG_INT(fbHeight);
	GET_CONFIG_INT(monitor);
	GET_CONFIG_INT(midiInput);
	GET_CONFIG_INT(defaultScene);
	GET_CONFIG_INT(programChangeChannel);
	GET_CONFIG_INT(effectiveWidth);
	GET_CONFIG_INT(effectiveHeight);

	GET_CONFIG_BOOLEAN(showUi);
	GET_CONFIG_BOOLEAN(forceVirtual);
	GET_CONFIG_BOOLEAN(fullscreen);
	GET_CONFIG_BOOLEAN(dumpMidi);

#undef GET_CONFIG_INT
#undef GET_CONFIG_BOOLEAN
	lua_pop(_L, 1);
	_config.loaded = true;
	return 1;
}

const config_t *script_get_config() {
	if (!_config.loaded) {
		return NULL;
	}
	return &_config;
}

int script_frame(double time, const gfx_render_settings *settings) {
	assert(lua_gettop(_L) == 0);
#ifdef DEBUG_SCRIPT
    lua_getglobal(_L, "debug");
    lua_getfield(_L, -1, "traceback");
#endif
    lua_getglobal(_L, "render");
    lua_pushnumber(_L, time);
    lua_newtable(_L);
#define PUSH_GFX_SETTING(_name) \
    lua_pushnumber(_L, settings->_name); \
    lua_setfield(_L, -2, #_name);
    
    PUSH_GFX_SETTING(virtualWidth);
    PUSH_GFX_SETTING(virtualHeight);
    PUSH_GFX_SETTING(fbWidth);
    PUSH_GFX_SETTING(fbHeight);
    PUSH_GFX_SETTING(windowWidth);
    PUSH_GFX_SETTING(windowHeight);
#undef PUSH_GFX_SETTINGS

	// midi input
	{
		lua_newtable(_L);
		const midiEvent_t *events;
		int midiEvents = midi_get_frame_data(&events);
		for (int i = 0; i < midiEvents; i++) {
			lua_newtable(_L);
			switch (events[i].midiData & 0xf0) {
			case MIDI_STATUS_NOTE_OFF:
				lua_pushstring(_L, "NOTE_OFF");
				lua_setfield(_L, -2, "type");
				lua_pushnumber(_L, events[i].timestamp);
				lua_setfield(_L, -2, "timestamp");
				lua_pushinteger(_L, (events[i].midiData & 0xff00) >> 8);
				lua_setfield(_L, -2, "note");
				lua_pushinteger(_L, (events[i].midiData & 0x0f) + 1);
				lua_setfield(_L, -2, "channel");
				break;
			case MIDI_STATUS_NOTE_ON:
				lua_pushstring(_L, "NOTE_ON");
				lua_setfield(_L, -2, "type");
				lua_pushnumber(_L, events[i].timestamp);
				lua_setfield(_L, -2, "timestamp");
				lua_pushinteger(_L, (events[i].midiData & 0xff00) >> 8);
				lua_setfield(_L, -2, "note");
				lua_pushinteger(_L, (events[i].midiData & 0x0f) + 1);
				lua_setfield(_L, -2, "channel");
				break;
			case MIDI_STATUS_PROGRAM_CHANGE:
				lua_pushstring(_L, "PROGRAM_CHANGE");
				lua_setfield(_L, -2, "type");
				lua_pushnumber(_L, events[i].timestamp);
				lua_setfield(_L, -2, "timestamp");
				lua_pushinteger(_L, (events[i].midiData & 0xff00) >> 8);
				lua_setfield(_L, -2, "program");
				lua_pushinteger(_L, (events[i].midiData & 0x0f) + 1);
				lua_setfield(_L, -2, "channel");
				break;
			default:
				log_warning("script_frame(): Unknown event: %x %f", events[i].midiData, events[i].timestamp);
				break;
			}
			lua_rawseti(_L, -2, i + 1);
		}
	}

	midi_frame_reset();

    int error;
#ifdef DEBUG_SCRIPT
    error = lua_pcall(_L, 3, 0, 2);
#else
    error = lua_pcall(_L, 3, 0, 0);
#endif
    if(error) {
        log_error("script_frame() %s", lua_tostring(_L, -1));
        lua_pop(_L, 1);  /* pop error message from the stack */
#ifdef DEBUG_SCRIPT
		lua_pop(_L, 2); // pop global from stack
#endif
		assert(lua_gettop(_L) == 0);
		return 0;
    }

#ifdef DEBUG_SCRIPT
	lua_pop(_L, 2); // pop global from stack
#endif
	assert(lua_gettop(_L) == 0);

    return 1;
}

void script_shutdown() {
	if (!_L) {
		log_warning("script_shutdown() called with LUA already unloaed...");
	}
    lua_close(_L);
}