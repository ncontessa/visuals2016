
local input_texture = tex("space.jpg")
local input_ratio = input_texture.width / input_texture.height

local ratio = var_f(function (zoom) return { 1.0 / zoom * (_iResolution[1] / _iResolution[2]) * (1.0 / input_ratio), (1.0 / zoom)} end, {par("zoom", "float", 1.0)})

local vec2_zero = {0, 0}
local blit = quad("blit", {tOffset = vec2_zero,  tScale = ratio, input0 = input_texture})
-- local threshold = par("threshold", "float", "min=0.0 max=1.0 step=0.01")
par_freq = par("freq", "float", 1.0, {min = -3, max = 6, step = 0.01})
par_blend0 = par("blend0", "float", 1.0, {min = 0, max = 1, step = 0.01})
par_blend1 = par("blend1", "float", 1.0, {min = 0, max = 1, step = 0.01})


local select = quad("select_by_intensity", {threshold = par("threshold", "float", 0.0), input0 = blit})
local gaussian = blur(select)
local flicker = var_f(function(freq) return (math.sin(_iGlobalTime * freq) + 0.0) * 1.0 end, {par_freq})

local clouds = quad("clouds", {iChannel0 = tex("tex16.png"), iResolution = iResolution, iGlobalTime = iGlobalTime})

local trigger = var_m(57, 1)


return quad("blend", {input0 = clouds, input1 = gaussian, blend0 = 0.0, blend1 = 1.0})