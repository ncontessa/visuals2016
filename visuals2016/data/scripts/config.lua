-- SENIGALLIA

config.forceVirtual = false
-- virtualWidth = width passed to shaders
x = 1024
y = 576

-- 1024 576

config.virtualWidth = x
config.virtualHeight = y
-- effectiveWidth = resolution shaders render to
-- DONT CHANGE
config.effectiveWidth = x
config.effectiveHeight = y

-- cut size
config.cutWidth = 1920
config.cutHeight = 1080

-- fbWidth = canvas width
config.fbWidth = 1920
config.fbHeight = 1080
config.cutFramebuffer = true
config.framebufferFilterNearest = false
-- cut offset = cut offet in fbSize from top left
config.cutOffsetX = 0
config.cutOffsetY = 0

config.showUi = false
config.fullscreen = true
config.monitor = 1
config.midiInput = 0
config.defaultScene = 123
config.programChangeChannel = 16
config.dumpMidi = false

