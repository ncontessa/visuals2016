CHANNEL_VISUALS = 16
CHANNEL_VISUALS2 = 13
CHANNEL_DRUMS = 5
TRIGGER_SNARE = 1
TRIGGER_PAD1 = 5
TRIGGER_PAD2 = 3
TRIGGER_KICK = 0
TRIGGER_POSTPUNK_COLORE = note_name_to_number("F1")
TRIGGER_POSTPUNK_ANIMAZIONE = note_name_to_number("C1")
TRIGGER_AURORA_PADS = {TRIGGER_SNARE, TRIGGER_PAD1, TRIGGER_PAD2}
TRIGGER_START_BABY_SOLDATO = "C1"
TRIGGER_FLASH2_BABY_SOLDATO = "G1"
TRIGGER_FLASH_BABY_SOLDATO = "F1"
TRIGGER_NONFINIRA_HYPERCUBE = "D1"
TRIGGER_NONFINIRA_MANDELBROT = "F1"
TRIGGER_NONFINIRA_MOBIUS = "C1"
TRIGGER_NONFINIRA_MOBIUS2 = "G1"
TRIGGER_NONFINIRA_RING = "E1"
TRIGGER_NONFINIRA_FLASH = "A1"
TRIGGER_BOND_RITORNELLO = "C1"
TRIGGER_COPPIE_SCENE = "F1"
TRIGGER_COPPIE_COLOR = "C1"
TRIGGER_COPPIE_START = "G1"
TRIGGER_ASPERGER_COLORE = "C1"
TRIGGER_POSTO_FADE = "C1"
TRIGGER_STUPIDA_FADE = "C1"
TRIGGER_PARIOLINI_START = "C1"
TRIGGER_PARIOLINI_BRIDGE = "F1"
TRIGGER_HIPSTERIA_ONOFF = "C1"
TRIGGER_HIPSTERIA_BIANCONERO = "F1"
TRIGGER_PROTO_BUDDHA = "F1"
TRIGGER_PROTO_STARS = "C1"
TRIGGER_WES_ZOOM = "C1"
TRIGGER_WES_FADE = "F1"
TRIGGER_FINIRA_FADE = "C1"
TRIGGER_FINIRA_INVERT = "F1"
TRIGGER_FINIRA_ACCEL = "G1"
TWEE_MIN_HEIGHT = -0.30 -- 0.30
UNACOSASTUPIDA_YSCALE = 1.0

-- questo nostro grande amore https://www.shadertoy.com/view/XsG3WR
-- finirà https://www.shadertoy.com/view/MsySRm
-- aurora ?
-- test screen

resolution = {config.effectiveWidth, config.effectiveHeight}
virtualResolution = {config.virtualWidth, config.virtualHeight}
standard_fb = {resolution[1], resolution[2], false}
double_fb = {resolution[1] *1.0, resolution[2] * 1.0, false}
quarter_fb = {resolution[1] / 4.0, resolution[2] / 4.0, true}
half_fb = {resolution[1] *0.5, resolution[2] * 0.5, false}
third_fb = {resolution[1] / 3, resolution[2] / 3, false}


function coppie() -- start, breaks?
--	local trigger = midi_elapsed(TRIGGER_COPPIE_FLASH, CHANNEL_VISUALS)
--	local bufa = quad_fb("grayscott_bufa", standard_fb, {iGlobalTime = iGlobalTime, doNoise = trigger, iResolution = iResolution})
--	bufa.inputs.iChannel0 = node_from_texture(vsl.texture_from_framebuffer(bufa.fb))
--	bufa.numPasses = 4
--	local image = quad("grayscott_image", {iGlobalTime = iGlobalTime, doNoise = trigger, iResolution= iResolution, iChannel0 = bufa})
	
	local kik = ms(TRIGGER_KICK, CHANNEL_DRUMS, 0.0, 0.5, 0.0, 0.2)
	local sn = accumulator(TRIGGER_SNARE, CHANNEL_DRUMS, 1.0)
	local start = mt(TRIGGER_COPPIE_START, CHANNEL_VISUALS, 0.0, 1.0, 0.1)
	local att = intensity(quad_fb("attraction" , half_fb,  {iResolution = iResolution,
									iGlobalTime = iGlobalTime,
									iChannel0 = tex("tex16.png"),
									amount = kik,
									mixColor = mt(TRIGGER_COPPIE_COLOR, CHANNEL_VISUALS, 0.0, 1.0, 0.5),
									scene = mt(TRIGGER_COPPIE_SCENE, CHANNEL_VISUALS, 0.0, 1.0, 0.4)}), 0.0)
	
	
	local accumFb = vsl.framebuffer_static_get(standard_fb[1], standard_fb[2], standard_fb[3])
	local blur = blur(nil, resolution)
	blur.fb = accumFb
	blur.input0 = node_from_texture(vsl.texture_from_framebuffer(blur.fb))
	local bl = quad("blend", {input0 = att, input1 = node_from_texture(vsl.texture_from_framebuffer(blur.fb)), blend0 = 1.0, blend1 = 0.8})
	bl.fb = accumFb
	
	local inv = invert(fade(bl, start), var_f(function(s) return sn.triggerCount % 2 end, {sn}))

	return inv
end

function aurora()

	local sn = ms(TRIGGER_SNARE, CHANNEL_DRUMS, 0.0, 1.6, 0.0, 1.0)
	local snA = accumulator(TRIGGER_SNARE, CHANNEL_DRUMS, 20.0)
	
	return fade(invert(quad("rorschach", {iResolution = iResolution,
									iGlobalTime = snA}), 0.0), sn)
end

function non_finira() -- cassa, break, cambio colori
		local trigger_kick = ms(TRIGGER_KICK, CHANNEL_DRUMS, 0.0, 1.0, 0.0, 0.4)
		
		
		local hypercube = quad_fb("hypercube", half_fb, {iResolution = iResolution,
															iGlobalTime = iGlobalTime,
															amount = trigger_kick})

		
		local ring = quad_fb("ring", half_fb, {iResolution = iResolution,
									iGlobalTime = iGlobalTime,
									amount = trigger_kick})

		local TRANSITION = 0.5
		local b0 = fade(fade(color({0.6, 0.6, 0.6}), trigger_kick), mt(TRIGGER_NONFINIRA_FLASH, CHANNEL_VISUALS, 1.0, 0.0, TRANSITION))
		local b1 = blend(hypercube, ring, mt(TRIGGER_NONFINIRA_HYPERCUBE, CHANNEL_VISUALS, 0.0, 1.0, TRANSITION),
										  mt(TRIGGER_NONFINIRA_RING, CHANNEL_VISUALS, 0.0, 1.0, TRANSITION))

		local mobius = quad_fb("mobius", half_fb, {iResolution = iResolution, iGlobalTime = function() return _iGlobalTime + 17.0 end, amount = trigger_kick})

		local b2 = blend(b1, mobius, 1.0, mt(TRIGGER_NONFINIRA_MOBIUS, CHANNEL_VISUALS, 0.0, 1.0, TRANSITION))

		local mobius2 = quad_fb("mobius2", half_fb, {iResolution = iResolution, iGlobalTime = iGlobalTime, amount = trigger_kick})
		local b3 = blend(b2, mobius2, 1.0, mt(TRIGGER_NONFINIRA_MOBIUS2, CHANNEL_VISUALS, 0.0, 1.0, TRANSITION))
		local mandelbrot = quad_fb("mandelbrot", half_fb, {iResolution = iResolution, iGlobalTime = midi_elapsed(TRIGGER_NONFINIRA_MANDELBROT, CHANNEL_VISUALS)})
		local b4 = blend(b3, mandelbrot, 1.0, mt(TRIGGER_NONFINIRA_MANDELBROT, CHANNEL_VISUALS, 0.0, 1.0, TRANSITION))
	
	return bloom(crt(sum(b4, b0)), virtualResolution, 1.5, {1.0, 1.2, 1.6})
end

function corso_trieste() -- parametri

	local time = make_iEffectTime()
	return quad_fb("sunset", half_fb, {iResolution = iResolution,
							amount1 = var_f(function(a) return smoothstep(a, 120.0, 120.0+180.0) * 0.2 end, {time})})
end

function protobodhisattva() -- aggiungere illustrazione jacopo

	local buddha = quad("mulbycolor", {input0 = fit_texture(tex("buddha.png"), 0.90), color = function() return hslToRgb(math.sin(_iGlobalTime * 2.0), 1.0, 0.5) end})
	buddha = bloom(buddha, virtualResolution, 2.2, {1.0, 1.0, 2.0})
	
	local stars =  bloom(quad_fb("starnest", half_fb, {iResolution = iResolution, iGlobalTime = make_iEffectTime()}), virtualResolution, 0.3, {0.0, 1.6, 1.6})
	return blend(stars, buddha, mt(TRIGGER_PROTO_STARS, CHANNEL_VISUALS, 0.0, 1.0, 0.5), mt(TRIGGER_PROTO_BUDDHA, CHANNEL_VISUALS, 0.0, 1.0, 8.5))
end

function asperger()
	
	local bsTrigger = ms(nil, CHANNEL_VISUALS2, 0.0, 1.0, 0.1, 0.4)

	return aberration(invert(quad("asperger", {iResolution = iResolution,
												iGlobalTime = iGlobalTime,
												iChannel0 = tex("tex16.png"),
												amount = bsTrigger,
												freq = midi_rand(nil, CHANNEL_VISUALS2, 1.0, 4.0),
												mixColor = mt(TRIGGER_ASPERGER_COLORE, CHANNEL_VISUALS, 0.0, 1.0, 0.5)}), 0.0), 1.00)
end

function pariolini()

	local fadeTrigger = mt(TRIGGER_PARIOLINI_START, CHANNEL_VISUALS, 0.0, 1.0, 0.2)
	local elapsed = midi_elapsed(TRIGGER_PARIOLINI_START, CHANNEL_VISUALS)
	local BRIDGE_TIME=  2.0
	local bridge = mt(TRIGGER_PARIOLINI_BRIDGE, CHANNEL_VISUALS, 2.0, 0.0, BRIDGE_TIME)
	local blo = mt(TRIGGER_PARIOLINI_BRIDGE, CHANNEL_VISUALS, 0.2, 2.0, BRIDGE_TIME)
	local blo1 = mt(TRIGGER_PARIOLINI_BRIDGE, CHANNEL_VISUALS, 1.0, 0.0, BRIDGE_TIME)
	
	local fb = {resolution[1] / 1.0, resolution[2] / 1.0, false}
	
	local res = {config.effectiveWidth, config.effectiveHeight}
	local bufa = quad_fb("diffusion_bufa", fb, {iChannel3 = tex("tex16.png"), iGlobalTime = iGlobalTime, iResolution = res, iEffectTime = make_iEffectTime()})
	bufa.inputs.iChannel0 = node_from_texture(vsl.texture_from_framebuffer(bufa.fb))
	local bufb = quad_fb("diffusion_bufb", fb, {iChannel0 = bufa, iResolution = res})
	local bufc = quad_fb("diffusion_bufc", fb, {iChannel0 = bufb, iResolution = res})
	bufa.inputs.iChannel1 = node_from_texture(vsl.texture_from_framebuffer(bufc.fb))
	local image = quad_fb("diffusion_image", fb, {iChannel0 = bufa, iChannel2 = bufc, iChannel3 = tex("tex16.png"), iResolution = res, iGlobalTime = make_iEffectTime()})
	
	return fade(bloom(intensity(image, bridge), virtualResolution, 1.5, {blo1, blo,blo}), fadeTrigger)
--	return image
end

function hipsteria()
	local trigger = mt(TRIGGER_HIPSTERIA_ONOFF, CHANNEL_VISUALS, 0.0, 1.0, 0.1)
	local trigger2 = mt(TRIGGER_HIPSTERIA_BIANCONERO, CHANNEL_VISUALS, 1.0, 0.0, 0.2)
	local accumulator = accumulator(nil, CHANNEL_VISUALS2, 1.0)
	local hips = intensity(quad_fb("hipsteria", {standard_fb[1] / 1.0, standard_fb[2] / 1.0, false}, {iGlobalTime = accumulator, seed = accumulator, iResolution = iResolution, iChannel0 = tex("tex16.png")}), trigger2)
	local bl = bloom(hips, virtualResolution, 1.9, {1.0, 1.0, 0.6})
	bl.inputs.blend1 = function() return 2.0 + math.cos(_iGlobalTime * 24.0) * 0.2 end
	return fade(bl, trigger)
end

function postpunk()
	
	local trigger = ms(TRIGGER_POSTPUNK_COLORE, CHANNEL_VISUALS, 0.0, 1.0, 0.5, -1.0)
	local trigger2 = mt(TRIGGER_POSTPUNK_ANIMAZIONE, CHANNEL_VISUALS, 0.0, 0.03, 1.1)
	local pp = quad("postpunk", {iGlobalTime = iGlobalTime, iResolution = iResolution, colorMix = trigger, intensity = trigger2, iChannel0 = tex("tex16.png")})
	local bl = bloom(pp, virtualResolution, 1.9, {2.0, 1.3, 0.6})
	return bl
end

function galaxy()
	local lunar = quad("lunar", {iResolution = iResolution, iGlobalTime = iGlobalTime, iChannel0 = tex("tex16.png")})

	local fireworks = fade(intensity(quad("fireworks", {iResolution = iResolution, iGlobalTime = iGlobalTime}), 0.0),
						mt(TRIGGER_BOND_RITORNELLO, CHANNEL_VISUALS, 0.0, 1.0, 0.3))

	return bloom(crt(sum(lunar, fireworks)), standard_fb, 1.0, {1.0, 1.0, 1.0})
end

function velleita()

	function trigger()
		return midi_rand(TRIGGER_KICK, CHANNEL_DRUMS)
	end

	local velleita = quad_fb("velleita", standard_fb, {iResolution = iResolution,
		iGlobalTime = iGlobalTime,
		setColor = {1.0, 0.0, 0.0},
		amount = var_f(multiply, {trigger(), ms(TRIGGER_KICK, CHANNEL_DRUMS, 0.0, 2.0, 0.0, 0.4)}),
		seed0 = trigger(),
		seed1 = trigger(),
		seed2 = trigger(),
		seed3 = trigger()})
	local accumFb = vsl.framebuffer_static_get(standard_fb[1], standard_fb[2], standard_fb[3])
	local blur = blur(nil, resolution)
	blur.fb = accumFb
	blur.input0 = node_from_texture(vsl.texture_from_framebuffer(blur.fb))
	local bl = quad("blend", {input0 = velleita, input1 = node_from_texture(vsl.texture_from_framebuffer(blur.fb)), blend0 = 1.0, blend1 = 0.9})
	bl.fb = accumFb
	return bl
end

function sfortuna()
	local trig = var_m({TRIGGER_KICK, TRIGGER_SNARE}, CHANNEL_DRUMS)

	
	
	function slice(tex, scale, xStart, xEnd, yStart, yEnd)
		local vScale
		local imgRatio = tex.width / tex.height
		local scrRatio = config.virtualWidth / config.virtualHeight
		
		if(imgRatio > scrRatio) then
		
			vScale = {scale, scale * scrRatio / imgRatio}
		else
			vScale = {scale / scrRatio * imgRatio, scale}
		end
	
		local vMin = {-1.0 * vScale[1] + 2.0 * vScale[1] * xStart, -vScale[2] + 2.0 * vScale[2] * yStart}
		local vMax = {-1.0 * vScale[1] + 2.0 * vScale[1] * xEnd, -vScale[2] + 2.0 * vScale[2] * yEnd}
		
		local tMin = {xStart, yStart}
		local tMax = {xEnd, yEnd}
		
		return vMin, vMax, tMin, tMax
	
	end

	function rand_f(freq, a, b)
		if(trig.triggerCount == 0 or (trig.triggerCount % freq) ~= 0) then
			return a
		else
			return b
		end
	end
	
	function letter(scale, xStart, xEnd, param1, param2, color2, yStart, yEnd)
		local vMin, vMax, tMin, tMax = slice(tex("logocani.png"), scale, xStart, xEnd, yStart, yEnd)
		local logo = quad("tex_blit", {input0 = tex("logocani.png"),
										vMin = vMin,
										vMax = vMax,
										tMin = var_f(function() return rand_f(param1, tMin, tMax) end),
										tMax = var_f(function() return rand_f(param1, tMax, tMin) end)})
		logo.fb = vsl.framebuffer_static_get(table.unpack(standard_fb))
		local col = quad("fixedcolor", {color = var_f(function() return rand_f(param2, {1.0, 1.0, 1.0}, color2) end)})
		return multiply_buffers(logo, col)
	end
	
	local letter_i = letter(0.95, 0.0, 0.12, 3, 4, {1.0, 0.6, 0.0}, 0.06, 0.96)
	local letter_c = letter(0.95, 0.22, 0.43, 7, 2, {1.0, 0.6, 0.2}, 0.04, 0.66)
	local letter_a = letter(0.95, 0.43, 0.64, 5, 6, {0.2, 0.8, 0.2}, 0.04, 0.66)
	local letter_n = letter(0.95, 0.65, 0.87, 4, 8, {0.2, 0.6, 1.0}, 0.04, 0.66)
	local letter_i2 = letter(0.95, 0.88, 1.00, 9, 3, {0.0, 0.6, 1.0}, 0.06, 0.96)
	
	local logo = sum(letter_i, sum(letter_c, sum(letter_a, sum(letter_n, letter_i2))))
	
	logo = invert(logo)
	
	local rain = quad("raindrop", {iResolution = iResolution, iGlobalTime = iGlobalTime, iChannel0 = aberration(logo, 2.0, 0.0), iChannel1 = tex("tex16.png")})
	local bl = bloom(rain, resolution, 1.2, {1.0, 0.2, 0.2})
	bl.inputs.blend1 = function() return 0.5 + math.cos(_iGlobalTime * 24.0) * 0.08 end
	bl.inputs.dummy = trig
	return bl
end

function twee()
	local main = quad("twee", {iGlobalTime = iGlobalTime, minHeight = TWEE_MIN_HEIGHT, iResolution = iResolution,
							amount_bd = ms(TRIGGER_KICK, CHANNEL_DRUMS, 0.0, 1.0, 0.0, 0.5),
							amount_hh = ms(TRIGGER_PAD1, CHANNEL_DRUMS, 0.0, 1.0, 0.0, 0.3),
							amount_sd = ms(TRIGGER_SNARE, CHANNEL_DRUMS, 0.0, 1.0, 0.0, 0.7),
							amount_sy = ms(nil, CHANNEL_VISUALS, 0.0, 1.0, 0.0, 0.5),
							amount_bs = ms(nil, CHANNEL_VISUALS2, 0.0, 1.0, 0.0, 0.2)})

	local ratio = (config.virtualWidth / config.virtualHeight) * 0.5
	
	local sy = fit_texture(tex("bd.png"), 0.22, nil, {-(1.0 / 3.0), -0.55 + TWEE_MIN_HEIGHT})
	local bd = fit_texture(tex("sy.png"), 0.22, nil, {-(1.0 / 3.0) * 2.0, -0.55 + TWEE_MIN_HEIGHT})
	local hh = fit_texture(tex("hh.png"), 0.22, nil, {0.0, -0.55 + TWEE_MIN_HEIGHT})
	local sd = fit_texture(tex("sd.png"), 0.22, nil, {(1.0 / 3.0), -0.55 + TWEE_MIN_HEIGHT})
	local bs = fit_texture(tex("bs.png"), 0.22, nil, {(1.0 / 3.0) * 2.0, -0.55 + TWEE_MIN_HEIGHT})
	local s = quad("vhs", {iChannel0 = sum(bd, sum(bs, sum(sy, sum(hh, sum(sd, main))))), iGlobalTime = function() return _iGlobalTime * 0.1 end, iResolution = iResolution})


	
	local bl =  bloom(quad("crt", {iResolution = iResolution, iChannel0 = s}), resolution, 1.2, {1.0, 0.1, 0.2})
	bl.inputs.input0.inputs.blend0 = function() return 1.2 + math.sin(math.cos(_iGlobalTime * 29.0) * 9.0) * 0.05 end
	return bl
end

function nabokov()

	local c = color({0.975, 0.0, 0.504})
	local tapeNoise = quad("tapenoise", {iGlobalTime = iGlobalTime, iResolution = iResolution})
	local q = sum(fit_texture(tex("logocani.png"), 0.95), tapeNoise)
	local b = quad("blend", {input0 = q, input1 = c, blend0 = 1.0, blend1 = ms(TRIGGER_KICK, CHANNEL_DRUMS, 0.0, 0.7, 0.0, 1.0)})

	local vhs = quad("vhs", {iChannel0 = b, iGlobalTime = iGlobalTime, iResolution = iResolution})

	return quad("crt", {iChannel0 = vhs, iResolution = iResolution})

end

function baby_soldato()

	local time = midi_elapsed(TRIGGER_FLASH_BABY_SOLDATO, CHANNEL_VISUALS)
	local trigger1 = ms(TRIGGER_FLASH_BABY_SOLDATO, CHANNEL_VISUALS, 0.0, 1.0, 0.0, 2.0)
	local trigger2 = ms(TRIGGER_FLASH2_BABY_SOLDATO, CHANNEL_VISUALS, 0.01, 1.0, 0.0, 3.4)
	local triggerKik = ms(TRIGGER_KICK, CHANNEL_DRUMS, 0.0, 1.0, 0.0, 0.3)
	local res = {config.effectiveWidth, config.effectiveHeight}

	local bufa = quad_fb("rps_bufa", double_fb, {iChannel2 = tex("tex16.png"), iGlobalTime = iGlobalTime, iResolution = res, iEffectTime = make_iEffectTime()})	
	bufa.inputs.iChannel0 = node_from_texture(vsl.texture_from_framebuffer(bufa.fb))
	
	local bufb = quad_fb("rps_bufb", double_fb, {iResolution = res, iEffectTime = make_iEffectTime(),  iChannel2 = tex("tex16.png")})
	bufb.inputs.iChannel1 = node_from_texture(vsl.texture_from_framebuffer(bufb.fb))
	local bufd = quad_fb("rps_bufd", double_fb, {iResolution = res, iGlobalTime = iGlobalTime})
	local bufc = quad_fb("rps_bufc", double_fb, {iResolution = res, iChannel0 = bufa, iChannel1 = bufb, iChannel3 = bufd})
	bufd.inputs.iChannel2 = bufc
	bufa.inputs.iChannel3 = node_from_texture(vsl.texture_from_framebuffer(bufd.fb))
	bufb.inputs.iChannel3 = node_from_texture(vsl.texture_from_framebuffer(bufd.fb))
	bufc.inputs.iChannel3 = node_from_texture(vsl.texture_from_framebuffer(bufd.fb))
	local image = quad_fb("rps_image", double_fb, {iResolution = res, iGlobalTime = iGlobalTime, iChannel0 = bufa, iChannel1 = bufb, iChannel3 = bufd})
	
	return bloom(fade(image, trigger2), resolution, 2.0, {1.0, 0.4, 0.4})	
end

function calabi_yau()
	return fade(bloom(quad_fb("water", {standard_fb[1] * 2.0, standard_fb[2] * 2.0, false}, {iResolution = iResolution, iGlobalTime = iGlobalTime}), resolution, 4.0, {1.0, 0.0, 0.0}), var_f(function(a) return smoothstep(a, 0.0, 9.0) end, {make_iEffectTime()})) 
end

function posto()
	local trigger = mt(TRIGGER_POSTO_FADE, CHANNEL_VISUALS, 0.0, 1.0, 0.5)
	return fade(intensity(quad_fb("mountains", standard_fb, {iGlobalTime = make_iEffectTime(), iResolution = iResolution, amount = trigger, iChannel0 = tex("tex16.png")}), 1.8), trigger)
end

function una_cosa_stupida()
	local clouds = quad_fb("clouds", standard_fb, {iChannel0 = tex("tex16.png"), iResolution = iResolution, iGlobalTime = iGlobalTime})
	local blit = quad("blit", {input0 = clouds, tScale = {1.0, UNACOSASTUPIDA_YSCALE}, tOffset = {0, 0}})
	local trigger = mt(TRIGGER_STUPIDA_FADE, CHANNEL_VISUALS, 0.0, 1.0, 0.5)
	return fade(blit, trigger)
end

function sparire()
	local time = make_iEffectTime()

	return blend(quad_fb("scattering", double_fb, {iResolution = iResolution, iGlobalTime = time}),
						color({0.0, 0.0, 0.0}),
						var_f(function(a) return smoothstep(a, 0.0, 45.0) end, {time}),
						1.0)
end


function finira()
		
		
		local k = ms(TRIGGER_KICK, CHANNEL_DRUMS, 0.0, 1.0, 0.0, 0.4)
		local accelTrigger = mt(TRIGGER_FINIRA_ACCEL, CHANNEL_VISUALS, 3.00, 15.00, 1.5)
		local t = 0
		local lastTime = -300.0
		local q = var_f(function(time, speed) 
				local elapsed = time - lastTime
				t = t + (elapsed * speed)
				lastTime = time
				return t
				
		end, {iGlobalTime, accelTrigger})
		
		local finira = intensity(quad("tunnel", {iGlobalTime = q,
												iResolution = iResolution,
												iChannel0 = tex("tex18.jpg"),
												c = 1.6,
												SPEED_IN = 0.05,
												SPEED_ROTATE = 0.05}), 0.0)
		local fadeTrigger = mt(TRIGGER_FINIRA_FADE, CHANNEL_VISUALS, 0.0, 1.0, 0.3)
		return fade(invert(accumblur(invert(finira), 1.0, k), mt(TRIGGER_FINIRA_INVERT, CHANNEL_VISUALS, 0.0, 1.0, 0.001)), fadeTrigger)
		
end

function perdona()
	local c = color({1., 0.0, 0.0})
	local tapeNoise = quad("tapenoise", {iGlobalTime = iGlobalTime, iResolution = iResolution})
--	local q = sum(fit_texture(tex("logocani.png"), 0.95), tapeNoise)
	local b = quad("blend", {input0 = tapeNoise, input1 = c, blend0 = 1.0, blend1 = ms(TRIGGER_SNARE, CHANNEL_DRUMS, 0.2, 1.0, 0.0, 0.28)})

	local vhs = quad("vhs", {iChannel0 = b, iGlobalTime = iGlobalTime, iResolution = iResolution})

	return bloom(quad("crt", {iChannel0 = vhs, iResolution = iResolution}), resolution, 1.0, {1.0, 1.0, 3.0})
end

function wes()
	local wes_fb = bloom(crt(quad("wes", {iResolution = iResolution, iGlobalTime = var_f(add, {iGlobalTime, accumulator(TRIGGER_SNARE, CHANNEL_DRUMS, 2.0)}), zoomFactor = 
	mt(TRIGGER_WES_ZOOM, CHANNEL_VISUALS, 0.5, 1.4, 2.3)})), resolution, 1.0, {1.0, 1.0, 3.0})
	local accumFb = vsl.framebuffer_static_get(standard_fb[1], standard_fb[2], standard_fb[3])
	local blur = blur(nil, resolution)
	blur.fb = accumFb
	blur.input0 = node_from_texture(vsl.texture_from_framebuffer(blur.fb))
	local bl = quad("blend", {input0 = wes_fb, input1 = node_from_texture(vsl.texture_from_framebuffer(blur.fb)), blend0 = 1.0, blend1 = 0.9})
	bl.fb = accumFb
	return fade(bl, mt(TRIGGER_WES_FADE, CHANNEL_VISUALS, 0.0, 1.0, 0.5))
	
end

-- DIRE A FRANKIE NUM +1
scenes = {
	[99] = finira(),
	[100] = perdona(),
	[101] = wes(),
	[102] = color({0.0, 0.0, 0.0}),
	[103] = posto(),
	[104] = calabi_yau(),
	[105] = baby_soldato(),
	[106] = nabokov(),
	[107] = twee(),
	[108] = sfortuna(),
	[109] = velleita(),
	[110] = galaxy(), -- bond, questo nostro
	[111] = postpunk(),
	[112] = hipsteria(),
	[113] = aurora(),
	[114] = coppie(),
    [115] = non_finira(), -- non finirà
	[116] = corso_trieste(), -- corso trieste
	[117] = protobodhisattva(), -- protobodhisattva
	[118] = pariolini(),
	[119] = asperger(),
	[120] = una_cosa_stupida(), -- una cosa stupida 
	[121] = sparire(), -- sparire
	[122] = quad("fixedcolor", {color = {1.0, 1.0, 1.0}}),
	[123] = invert(fit_texture(tex("logocani.png"), 0.95))
}

-- program accumulator
-- fix effects reload
-- j
-- two more effects

-- coppie: https://www.shadertoy.com/view/4dV3Dh
-- VHS effect: https://www.shadertoy.com/view/4dBGzK https://www.shadertoy.com/view/MsK3zw
-- bond: fuochi d'artificio? https://www.shadertoy.com/view/ldy3Rw
-- lego https://www.shadertoy.com/view/MsGGzW
-- pallocchi https://www.shadertoy.com/view/MsG3zD
-- reaction diffusion https://www.shadertoy.com/view/XsG3z1 https://www.shadertoy.com/view/4dcGW2
-- trembling lines https://www.shadertoy.com/view/XdGGz1
-- cloud smoke lights https://www.shadertoy.com/view/MdyGzR PAGINA 30



