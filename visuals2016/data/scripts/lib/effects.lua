function blur(input, resolution)
   local pass1 = quad("blur", {input0 = input, direction = 0, iResolution = resolution})
   local pass2 = quad("blur", {input0 = pass1, direction = 1, iResolution = resolution})
   return pass2
end

function bloom(input, resolution, power, _blendingFactors)

	local blendingFactors = _blendingFactors or {1.0, 1.0, 1.0}

	local halfsettings = {resolution[1] / 2.0, resolution[2] / 2.0, false}
	local halfres = {resolution[1] / 2.0, resolution[2] / 2.0}
	
	local quarterres = {resolution[1] / 4.0, resolution[2] / 4.0}
	local quartersettings = {resolution[1] / 4.0, resolution[2] / 4.0, false}
	
	local bright = quad_fb("select_by_intensity", halfsettings, {input0 = input, power = power})
	
	local blurred = quad_fb("blur", halfsettings, {input0 = bright, direction = 0, iResolution = halfres})
	local blurred2 = quad_fb("blur", halfsettings, {input0 = blurred, direction = 1, iResolution = halfres})

	local bright2 = quad_fb("select_by_intensity", quartersettings, {input0 = input, power = power})

	local blurred3 = quad_fb("blur", quartersettings, {input0 = bright2, direction = 0, iResolution = quarterres})
	local blurred4 = quad_fb("blur", quartersettings, {input0 = blurred3, direction = 1, iResolution = quarterres})
	
	local blend1 = quad("blend", {input0 = input, input1 = blurred2, blend0 = blendingFactors[1], blend1 = blendingFactors[2]})
	local blend2 = quad("blend", {input0 = blend1, input1 = blurred4, blend0 = 1.0, blend1 = blendingFactors[3]})
	return blend2
end

function tonemapping(input, amount)
	return quad("tonemapping", {input0 = input, amount = amount or 0.0})
end

function aberration(input, aberration, vignette)
	return quad("aberration", {iChannel0 = input, iResolution = iResolution, aberration = aberration or 1.25, vignette = vignette or 0.0})
end

function invert(input, amount)
	return quad("invert", {input0 = input, amount = amount or 1.0})
end

function intensity(input, amount)
	return quad("intensity", {input0 = input, saturation = amount or 0.0})
end

function accumulator(note, channel, amount)
	local result = var_m(note, channel)
	
	result.eval_midi = function() return result.triggerCount * amount end
	
	return result
end

function make_iEffectTime()
	
	local result = var_m(nil, nil)
	
	result.eval_midi = function(node)
		if(node.effectStartTime == -1.0) then
			node.effectStartTime = _iGlobalTime
		end
			return _iGlobalTime - node.effectStartTime
	end
	return result
end


function fit_texture(tex, _scale, _resolution, _offset)
	local scale = _scale or 1.0
	local resolution = _resolution or {config.virtualWidth, config.virtualHeight}
	local tScale
	
	local imgRatio = tex.width / tex.height
	local scrRatio = resolution[1] / resolution[2]
	
	if(imgRatio > scrRatio) then
	
		tScale = {scale, scale * scrRatio / imgRatio}
	else
		tScale = {scale / scrRatio * imgRatio, scale}
	end
	return quad_fb("blit", {config.effectiveWidth, config.effectiveHeight, false}, {input0 = tex, tScale = tScale, tOffset = _offset or {0, 0}})
end

function sum(input0, input1)
	return quad("blend", {input0 = input0, input1 = input1, blend0 = 1.0, blend1 = 1.0})
end

function multiply_buffers(input0, input1)
	return quad("multiply", {input0 = input0, input1= input1})
end

function color(col)
	return quad("fixedcolor", {color = col})
end

function crt(i)
	return quad("crt", {iResolution = iResolution, iChannel0 = i})
end

function blend(i1, i2, b1, b2)
	return quad("blend", {input0 = i1, input1 = i2, blend0 = b1, blend1 = b2})
end

function fade(i, _amount)
	return quad("blend", {input0 = i, blend0 = _amount, input1 = vsl.texture_null(), blend1 = 0.0})
end

function hslToRgb(h, s, l)
  local r, g, b

  if s == 0 then
    r, g, b = l, l, l -- achromatic
  else
    function hue2rgb(p, q, t)
      if t < 0   then t = t + 1 end
      if t > 1   then t = t - 1 end
      if t < 1/6 then return p + (q - p) * 6 * t end
      if t < 1/2 then return q end
      if t < 2/3 then return p + (q - p) * (2/3 - t) * 6 end
      return p
    end

    local q
    if l < 0.5 then q = l * (1 + s) else q = l + s - l * s end
    local p = 2 * l - q

    r = hue2rgb(p, q, h + 1/3)
    g = hue2rgb(p, q, h)
    b = hue2rgb(p, q, h - 1/3)
  end

  return {r, g, b}
end

function accumblur(input0, blend1, blend2)
	local accumFb = vsl.framebuffer_static_get(resolution[1], resolution[2], false)
	local blur = blur(nil, resolution)
	blur.fb = accumFb
	blur.input0 = node_from_texture(vsl.texture_from_framebuffer(blur.fb))
	local bl = quad("blend", {input0 = input0, input1 = node_from_texture(vsl.texture_from_framebuffer(blur.fb)), blend0 = blend1, blend1 = blend2})
	bl.fb = accumFb
	return bl
end