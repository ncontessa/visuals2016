
dofile("tableprint.lua")

function clamp(val, lower, upper)
    if lower > upper then lower, upper = upper, lower end -- swap if boundaries supplied the wrong way
    return math.max(lower, math.min(upper, val))
end

function smoothstep(x, edge0, edge1)
    x = clamp((x - edge0)/(edge1 - edge0), 0.0, 1.0); 
   return x*x*(3 - 2*x)
end

function make_node(type, subtype, inputs)
   local result = {}
   result.type = type
   result.subtype = subtype
   result.inputs = inputs or {}
   return result
end

function make_render_node(inputs)
   local result = make_node("render", "quad", inputs)
   return result
end

function quad(program_name, inputs)
   local result = make_render_node(inputs)
   result.subtype = "quad"
   result.program = vsl.program_get("data/shaders/"..program_name)
   result.name = program_name
   return result
end

function quad_fb(program_name, fb, inputs)
   local result = make_render_node(inputs)
   result.subtype = "quad"
   result.program = vsl.program_get("data/shaders/"..program_name)
   result.name = program_name
   result.fb = vsl.framebuffer_static_get(fb[1], fb[2], fb[3])
   return result
end

function make_function_node(fun, inputs)
   local result =  make_node("var", "function", inputs)
   result.eval_function = fun
   return result
end

var_f = make_function_node

function make_constant_node(value)
   local result = make_node("var", "function", {})
   result.eval_function = function () return value end
   return result
end

function note_name_to_number(noteName)
	local result = 0
	if(#noteName ~= 2 and #noteName ~=3) then
		vsl.log("invalid noteName "..noteName)
		return result
	end
	local mapping = {C = 0, D = 2, E = 4, F = 5, G = 7, A = 9, B = 11}
	local noteLetter = string.upper(string.sub(noteName, 1, 1))
	result = mapping[noteLetter]
	local octave = tonumber(string.sub(noteName, -1), 10)
	if(#noteName == 3) then
		local alterazione = string.sub(noteName, 2, 2)
		if(alterazione == "b") then
			result = result - 1
		elseif(alterazione == "#") then
			result = result + 1
		end
	end
	return 24 + result + 12 * octave
end

var_k = make_constant_node

ALL_CHANNELS = {true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true}

function reset_midi(graph)
	local update = function(node)
		if(node.subtype ~= "midi") then
			return
		end
		node.lastOn = -1.0
		node.lastOff = -1.0
		node.triggerCount = 0
		node.direction = -1.0
		node.effectStartTime = -1.0
		node.randomValue = math.random()
	end
	walk_node_by_type(graph, "var", update)
end

function numbers_to_table(numbers)
	local result = {}
	for i, v in ipairs(numbers) do
		result[v] = true
	end
	return result
end

function make_midi_node(note, channel)
	local result = make_node("var", "midi", {})
	if(note == nil) then
		result.note = nil
	elseif(type(note) == "string") then
		result.note = numbers_to_table({note_name_to_number(note)})
	elseif( type(note) == "table") then
		result.note = numbers_to_table(note)
	else
		result.note = numbers_to_table({note})	
	end
	
	if(type(channel) == "number") then
		result.channel = {}
		result.channel[channel] = true
	elseif(type(channel) == "nil") then
		result.channel = ALL_CHANNELS
	elseif(type(channel) == "table") then
		result.channel = numbers_to_table(channel)
	else
		error("invalid value")
	end
	result.lastOn = -1.0
	result.lastOff = -1.0
	result.eval_midi = function(node)
		if(node.lastOn > node.lastOff) then
			return 1.0
		else
			return 0.0
		end
	end
	
	result.triggerCount = 0
	
	result.triggerOn = function()
		result.triggerCount = result.triggerCount + 1
	end
	
	result.triggerOff = function()
	
	end
	
	return result
end

var_m = make_midi_node

function midi_rand(note, channel, _min, _max)
	
	local result = make_midi_node(note, channel)
	result.min = _min or 0.0
	result.max = _max or 1.0
	result.triggerOn = function() 
		result.randomValue = math.random()
	end
	result.eval_midi = function()
		return result.min + (result.max - result.min) *result.randomValue
	end
	return result
end

function make_midi_node_smooth(note, channel, valueFrom, valueTo, timeUp, timeDown)
	local result = make_midi_node(note, channel)

		result.eval_midi = function(node)
			--- 4 cases: flat down, flat up, going up, going down
		if(_iGlobalTime >= node.lastOn and _iGlobalTime <= (node.lastOn + timeUp)) then
			return valueFrom + (valueTo - valueFrom) * smoothstep(_iGlobalTime, node.lastOn, node.lastOn + timeUp)
		elseif(node.lastOn >= 0.0 and _iGlobalTime >= (node.lastOn + timeUp) and timeDown < 0.0 ) then
			return valueTo
		elseif(_iGlobalTime >= (node.lastOn + timeUp) and _iGlobalTime <= (node.lastOn + timeUp + timeDown)) then
			return valueTo + (valueFrom - valueTo) * smoothstep(_iGlobalTime, node.lastOn + timeUp, node.lastOn + timeUp + timeDown)
		else
			return valueFrom
		end
	end
	return result
end

ms = make_midi_node_smooth

function make_midi_node_trigger(note, channel, _valueFrom, _valueTo, timeUp)
	local result = make_midi_node(note, channel)

		result.valueFrom = _valueFrom
		result.valueTo = _valueTo
		result.direction = -1.0
		
		result.triggerOn = function() 
		result.direction = result.direction * -1.0
		end
		result.eval_midi = function(node)

				local a, b
				if(node.direction == 1.0) then
					a = node.valueFrom
					b = node.valueTo
				else
					a = node.valueTo
					b = node.valueFrom
				end
				if(_iGlobalTime >= node.lastOn and _iGlobalTime <= (node.lastOn + timeUp)) then
					return a + (b - a) * smoothstep(_iGlobalTime, node.lastOn, node.lastOn + timeUp)
				else
					return b
				end

		end
	return result
end

mt = make_midi_node_trigger

function midi_elapsed(note, channel)
	local result = make_midi_node(note, channel)
	result.eval_midi = function()
		if(result.lastOn == -1.0) then
			return -1.0
		else
			return _iGlobalTime - result.lastOn
			end
	end
	return result
end

function add(a, b)
   return a + b
end

function multiply(a, b)
	return a * b
end

function eval_var(var_node)

   if(type(var_node) == "function") then
      return var_node()
   end

   if(type(var_node) == "number") then
      return var_node
   end

   if(type(var_node) == "table" and var_node.type == nil) then
      return var_node
   end
   -- bad, bad design
   if(var_node.eval_value and var_node.eval_value ~= true) then
      return var_node.eval_value
   end

   if(var_node.parameter) then
      return vsl.parameter_get(var_node.parameter)
   end
   
   local arguments = {}
   for i, child in ipairs(var_node.inputs) do
      table.insert(arguments, eval_var(child))
   end

   if(var_node.subtype == "function") then
      var_node.eval_value = var_node.eval_function(table.unpack(arguments))
      return var_node.eval_value
   elseif(var_node.subtype == "midi") then
		var_node.eval_value = var_node.eval_midi(var_node)
		return var_node.eval_value
   end
end

function is_render_node(node)
   if type(node) == "table" and (node.type == "render" or node.type == "texture") then
      return true
   else
      return false
   end
end

function render_walk_node(node)
   if(node.type == "render") then
      -- first of all collect inputs and render them...
      local result_tex = {}
      local result_vars = {}
	  
	  if(node.renderedTexture) then
		return node.renderedTexture
	end
	  
	if(node.name == "blend") then -- BLEND OPTIMIZATION (hackish)
	result_vars["blend0"] = eval_var(node.inputs["blend0"])
		if(result_vars["blend0"] == 0) then 
			table.insert(result_tex, {tex = vsl.texture_null(), uniformName = "input0"})
		else
			table.insert(result_tex, {tex = render_walk_node(node.inputs["input0"]), uniformName = "input0"})
		end

	result_vars["blend1"] = eval_var(node.inputs["blend1"])
		if(result_vars["blend1"] == 0) then 
			table.insert(result_tex, {tex = vsl.texture_null(), uniformName = "input1"})
		else
			table.insert(result_tex, {tex = render_walk_node(node.inputs["input1"]), uniformName = "input1"})
		end
	else
     for key, value in pairs(node.inputs) do
		if is_render_node(value) then
		
				table.insert(result_tex, {tex = render_walk_node(value), uniformName = key})
		else
			result_vars[key] = eval_var(value)
		end
     end
	end
	  

      -- then render this node...
      if(node.program) then
	 vsl.bind_program(node.program)
      end
      
      for i, tex in ipairs(result_tex) do
	 vsl.bind_texture(i - 1, tex.tex)
	 vsl.bind_uniform_int(node.program, tex.uniformName, i - 1)
      end

      for k,v in pairs(result_vars) do
	 vsl.bind_uniform(node.program, k, v)
      end

     local fb = nil
     if(node.fb) then
	 fb = node.fb
     else
	 fb = vsl.framebuffer_get()
     end
	  
	 vsl.bind_framebuffer(fb)
      
      if(node.subtype == "quad") then
	 
		local passes = node.numPasses or 1
		for i=1,passes do
			vsl.render_fullscreen_quad()
		end
      end

	  node.renderedTexture = vsl.texture_from_framebuffer(fb)
	
	 return vsl.texture_from_framebuffer(fb)
      
   elseif(node.type == "texture") then
      return node.texture
   elseif(node.type == "simulation") then
      print("running simulation..")
      if(#node.inputs ~= 1) then error("simulation nodes only can have 1 input!") end
      return render_walk_node(node.inputs[0])
   else
      tprint(node)
      error("Unimplemented node!")
   end
end

function walk_node_by_type(node, node_type, fun)

   if(type(node) ~= "table") then
      return
   end

   if(node.type == node_type) then
      fun(node)
   end

   if(node.inputs) then
      for k, v in pairs(node.inputs) do
	  if(v ~= node) then
		walk_node_by_type(v, node_type, fun)
		end
      end
   end
end

function make_texture_node(filename)
   local result = {}
   result.name = filename
   result.type = "texture"
   result.inputs = {}
   result.texture, result.width, result.height = vsl.texture_get("data/textures/".. filename)
   return result
end

function node_from_texture(tex)
	local result = {}
	result.name = ""
	result.type = "texture"
	result.inputs = {}
	result.texture = tex
	return result
end
	

tex = make_texture_node

function make_null_texture_node(filename)
   local result = {}
   result.name = filename
   result.type = "texture"
   result.inputs = {}
   result.width, result.height = 0, 0
   result.texture = vsl.texture_null()
   return result
end

function make_parameter_node(name, par_type, default, options)
   local result = {}
   if(type == "color3f" and not default) then
	default = {0.0, 0.0, 0.0}
   end
   result.type = "var"
   result.parameter = vsl.parameter_create(name, par_type, default or 0.0, options or {})
   result.subtype = "var"
   return result
end

par = make_parameter_node

function par_f(func, par)
   local result = var_f(func)
   result.input = {par}
   return result
end


iResolution = function() return _iResolution end
iGlobalTime = function() return _iGlobalTime end
iEffectTime = function() return _iEffectTime end

function update_midi_data(midi_data, graph)
	local update = function(node)
		if(node.subtype ~= "midi") then
			return
		end
		if(node.eval_value) then
			return
		end
		for i,midi_event in ipairs(midi_data) do
			if node.channel[midi_event.channel] then
				if (not node.note) or node.note[midi_event.note] then
					if(midi_event.type == "NOTE_ON") then
					   node.lastOn = _iGlobalTime
					   node.triggerOn()
					   node.eval_value = true
						elseif(midi_event.type == "NOTE_OFF") then
						   node.lastOff = _iGlobalTime
						   node.triggerOff()
						   node.eval_value = true
						end
				end
			end
		end
	end
	walk_node_by_type(graph, "var", update)
end



local activeBuffer = 0
lastChangeTime = -1.0
local changeDuration = 1.0
_iGlobalTime = 0.0
_iEffectTime = 0.0

function update_graph(midi_data)
	local toProgram = nil
	for i, midi_event in ipairs(midi_data) do
		if midi_event.type == "PROGRAM_CHANGE" then
			if (not config.programChangeChannel) or (config.programChangeChannel == midi_event.channel) then
			if(midi_event.program ~= currentProgram) and (scenes[midi_event.program]) then
					lastChangeTime = _iGlobalTime
					toProgram = midi_event.program
					currentScene = toProgram
				end
			end
		end
	end
	
	if (toProgram) then 
		activeBuffer = 1 - activeBuffer
		reset_midi(scenes[toProgram])
		graph.inputs["input" .. activeBuffer] = scenes[toProgram]
	end
	
	local blendValue = smoothstep(_iGlobalTime, lastChangeTime, lastChangeTime + changeDuration)
	graph.inputs["blend" .. activeBuffer] = blendValue
	graph.inputs["blend" .. (1 - activeBuffer)] = (1 - blendValue)
	
end

function render(time, render_settings, midi_data)

   _iResolution = {config.virtualWidth, config.virtualHeight}  
   _iGlobalTime = time
   update_graph(midi_data)
   _iEffectTime = _iGlobalTime - lastChangeTime
   
	walk_node_by_type(graph, "render", function (node) node.renderedTexture = nil end)
	walk_node_by_type(graph, "var", function (node) node.eval_value = nil end)
   update_midi_data(midi_data, graph)
   vsl.reset_frame()
	local vx, vy, ox, oy
	if(config.cutFramebuffer) then vx = config.cutWidth / render_settings.fbWidth else vx = 1.0 end
	if(config.cutFramebuffer) then vy = config.cutHeight / render_settings.fbHeight else vy = 1.0 end
	if(config.cutFramebuffer) then ox = -1.0 + vx + ((config.cutOffsetX / render_settings.fbWidth) * 2.0) else ox = 0.0 end
	if(config.cutFramebuffer) then oy = 1.0 - vy - ((config.cutOffsetY / render_settings.fbHeight) * 2.0) else oy = 0.0 end
	local blit = quad("blit", {input0 = graph, tScale = {vx, vy}, tOffset = {ox, oy}})
	blit.fb = vsl.framebuffer_default()
	render_walk_node(blit, true)
   vsl.render()
end

function init_graph()
	if not currentScene then
		currentScene = config.defaultScene
	end
		
	graph = quad("blend", {input0 = make_null_texture_node(), input1 = make_null_texture_node(), blend0 = 0, blend1 = 0})
	graph.fb = vsl.framebuffer_static_get(config.effectiveWidth, config.effectiveHeight, config.framebufferFilterNearest)
	local programChangeEvent = {type = "PROGRAM_CHANGE", program = currentScene, channel = config.programChangeChannel, timestamp = 0 }
	update_graph({programChangeEvent})

end
