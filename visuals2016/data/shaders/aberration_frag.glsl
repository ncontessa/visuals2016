#version 410
uniform vec2 iResolution;
uniform float iGlobalTime;
uniform sampler2D iChannel0;
out vec4 fragColor;
in vec2 vCoords;
in vec2 tCoords;
uniform float aberration;
uniform float vignette;

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
	float 	centerBuffer 		= 0.15,
			vignetteStrength 	= vignette,
			aberrationStrength 	= aberration;
	
	float 	chrDist,
			vigDist;
	
	vec2 uv = fragCoord.xy / iResolution.xy;
	
	//calculate how far each pixel is from the center of the screen
	vec2 vecDist = uv - ( 0.5 , 0.5 );
	chrDist = vigDist = length( vCoords );
	
	//modify the distance from the center, so that only the edges are affected
	chrDist	-= centerBuffer;
	if( chrDist < 0.0 ) chrDist = 0.0;
	
	//distort the UVs
	vec2 uvR = uv + ( pow(chrDist, 2.5) * 0.01 * aberrationStrength ),
		 uvB = uv - ( pow(chrDist, 2.5) * 0.01 * aberrationStrength );
	
	//get the individual channels using the modified UVs
	vec4 c;
	
	c.x = texture( iChannel0 , uvR ).x;
	c.y = texture( iChannel0 , uv ).y;
	c.z = texture( iChannel0 , uvB ).z;
	
	//apply vignette
	c *= 1.0 - vigDist* vignetteStrength;
	
	fragColor = c;
}

void main() {
	vec2 fragCoord = tCoords * iResolution;
	mainImage(fragColor, fragCoord);
}
