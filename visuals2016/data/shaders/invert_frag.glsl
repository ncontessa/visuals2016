#version 410

in vec2 vCoords;
in vec2 tCoords;
out vec4 fragColor;
uniform float amount;
uniform sampler2D input0;


void main()
{
    vec3 color = texture(input0, tCoords).rgb;
	vec3 invert = vec3(1., 1., 1.) - color;
	fragColor.a = texture(input0, tCoords).a;
	fragColor.rgb = mix(color, invert, amount);
	
}