#version 410
in vec2 coords;
out vec2 vCoords;
out vec2 tCoords;

uniform vec2 vMin;
uniform vec2 vMax;
uniform vec2 tMin;
uniform vec2 tMax;

void main() {
	vec2 t;
    t = (coords + vec2(1.0, 1.0)) * 0.5;
	vCoords = vMin + (vMax - vMin) * t;
	tCoords = tMin + (tMax - tMin) * t;
	
    gl_Position.xy = vCoords;
	gl_Position.zw = vec2(0.0, 1.0);
}