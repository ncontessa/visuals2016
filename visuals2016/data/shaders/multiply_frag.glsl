#version 410

in vec2 vCoords;
in vec2 tCoords;
out vec4 fragColor;
uniform sampler2D input0;
uniform sampler2D input1;

void main()
{

    fragColor = vec4(texture(input0, tCoords).rgb * texture(input1, tCoords).rgb, 1.0);
}