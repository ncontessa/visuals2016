#version 410
uniform vec2 iResolution;
uniform float iGlobalTime;
uniform sampler2D iChannel0;
out vec4 fragColor;
in vec2 vCoords;
in vec2 tCoords;

const int   c_samplesX    = 15;  // must be odd
const int   c_samplesY    = 15;  // must be odd

const int   c_halfSamplesX = c_samplesX / 2;
const int   c_halfSamplesY = c_samplesY / 2;

float Gaussian (float sigma, float x)
{
    return exp(-(x*x) / (2.0 * sigma*sigma));
}

vec3 BlurredPixel (in vec2 uv)
{
    float c_sigmaX      = 5.0;
    float c_sigmaY      = 5.0;
    
    float total = 0.0;
    vec3 ret = vec3(0);
    
    for (int iy = 0; iy < c_samplesY; ++iy)
    {
        float fy = Gaussian (c_sigmaY, float(iy) - float(c_halfSamplesY));
        float offsety = float(iy-c_halfSamplesY) / iResolution.y;
        for (int ix = 0; ix < c_samplesX; ++ix)
        {
            float fx = Gaussian (c_sigmaX, float(ix) - float(c_halfSamplesX));
            float offsetx = float(ix-c_halfSamplesX) / iResolution.x;
            total += fx * fy;
            ret += texture(iChannel0, uv + vec2(offsetx, offsety)).rgb * fx*fy;
        }
    }
    return ret / total;
}

void main()
{
    vec2 fragCoord = tCoords * iResolution;
    vec2 uv = fragCoord.xy / iResolution.xy;
    fragColor = vec4(BlurredPixel(uv), 1.0);
}