//Noise animation - Electric
//by nimitz (stormoid.com) (twitter: @stormoid)

//The domain is displaced by two fbm calls one for each axis.
//Turbulent fbm (aka ridged) is used for better effect.


#version 410
uniform vec2 iResolution;
uniform float iGlobalTime;
uniform sampler2D iChannel0;
uniform float colorMix;
uniform float intensity;
out vec4 fragColor;
in vec2 vCoords;
in vec2 tCoords;
 float seed;
// horizontal Gaussian blur passfloat pi = atan(1.0)*4.0;
float pi = atan(1.0)*4.0;
float tau = atan(1.0)*8.0;

float scale = 1.0 / 6.0;

float epsilon = 1e-3;
float infinity = 1e6;

//Settings
//Uses cheaper arcs for common sweep angles (90 & 180 degrees).
#define USE_CHEAP_ARCS

#define TEXT_COLOR   vec3(1.00, 0.20, 0.10)
#define BORDER_COLOR vec3(0.05, 0.20, 1.00)

#define BRIGHTNESS 0.004
#define THICKNESS  0.002
#define HASHSCALE .1031

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

float noise( in vec3 x )
{
    vec3 p = floor(x);
    vec3 f = fract(x);
    f = f*f*(3.0-2.0*f);
    vec2 uv = (p.xy+vec2(37.0,17.0)*p.z) + f.xy;
    vec2 rg = texture( iChannel0, (uv+ 0.5)/256.0, -100.0 ).yx;
    return -1.0+2.0*mix( rg.x, rg.y, f.z );
}

float rand(float p)
{
	vec3 p3  = fract(vec3(p) * HASHSCALE);
    p3 += dot(p3, p3.yzx + 19.19);
    return fract((p3.x + p3.y) * p3.z);
}


//Checks if a and b are approximately equal.
bool ApproxEqual(float a, float b)
{
    return abs(a - b) <= epsilon;
}

//Distance to a line segment,
float dfLine(vec2 start, vec2 end, vec2 uv)
{
//	start *= scale;
//	end *= scale;
    
	vec2 line = end - start;
	float frac = dot(uv - start,line) / dot(line,line);
	return distance(start + line * clamp(frac, 0.0, 1.0), uv);
}

//Distance to an arc.
float dfArc(vec2 origin, float start, float sweep, float radius, vec2 uv)
{
	origin *= scale;
	radius *= scale;  
	uv -= origin;
    
	uv *= mat2(cos(start), sin(start),-sin(start), cos(start));
	
    #ifdef USE_CHEAP_ARCS
        if(ApproxEqual(sweep, pi)) //180 degrees
        {
            float d = abs(length(uv) - radius) + step(uv.y, 0.0) * infinity;
            d = min(d, min(length(uv - vec2(radius, 0)), length(uv + vec2(radius, 0))));
            return d;
        }
        else if(ApproxEqual(sweep, pi/2.0)) //90 degrees
        {
            float d = abs(length(uv) - radius) + step(min(uv.x, uv.y), 0.0) * infinity;
            d = min(d, min(length(uv - vec2(0, radius)), length(uv - vec2(radius, 0))));
            return d;
        }
        else //Others
        {
            float offs = (sweep / 2.0 - pi);
            float ang = mod(atan(uv.y, uv.x) - offs, tau) + offs;
            ang = clamp(ang, min(0.0, sweep), max(0.0, sweep));

            return distance(radius * vec2(cos(ang), sin(ang)), uv); 
        }
    #else
        float offs = (sweep / 2.0 - pi);
        float ang = mod(atan(uv.y, uv.x) - offs, tau) + offs;
        ang = clamp(ang, min(0.0, sweep), max(0.0, sweep));

        return distance(radius * vec2(cos(ang), sin(ang)), uv);
	#endif
}

vec2 actCurve(in float t, in vec2 from, in vec2 to, in vec2 freq, in vec2 ins) {
		vec2 pos = from + (to - from) * t;
		float speed = 6.0;
		vec3 mult = vec3(200.2, 200.2, 1.0);
		vec3 start = vec3(pos.x, pos.y, iGlobalTime * speed);
		
		pos += vec2(0.0, noise(start * mult)) * intensity;
//		pos += vec2(sin(12. * t), cos(6.17 * t)) * ins;
	return pos;
}

float curve(in vec2 from, in vec2 to, in vec2 freq, in vec2 ins, in vec2 uv) {

	float iterations = 10.0;
	float step = 1.0 / iterations;
	float t = 0.0;
	float dist = infinity;
	vec2 p1 = actCurve(0, from, to, freq, ins);
	vec2 p2;
	for (float i = 0.0; i < iterations; i += 1.0) {	
		
		vec2 pos = from + (to - from) * t;
		p2 = actCurve(t + step, from, to, freq, ins);
		dist = min(dist, dfLine(p1, p2, uv));
		p1 = p2;
		t += step;
	}

	return dist;

}

vec3 dfLogo(vec2 uv)
{
	float dist = infinity;

//	dist = min(dist, dfLine(vec2(-10.,-10.), vec2(10.,10.), uv));
    float tf_text = max(epsilon, iGlobalTime - 0.6);
    float bright_text = BRIGHTNESS * min(1.0, 1.0 - sin(tf_text * pi * 50.0) / (tf_text * pi * 1.3));
    float shade = 0.0;
	float aspect = iResolution.x / iResolution.y;
	
	vec3 color = vec3(0);
	for(int i = 0; i < 10; i++) {
		vec2 orig = vec2(- aspect * 0.42, -0.45 + 2. * i * (1.0 / 20.0));
		vec2 dest = vec2(aspect * 0.42, -0.45 + 2. * i * (1.0 / 20.0));
	
		dist = min(dist, curve(orig, dest, vec2(0., 0.), vec2(0., 0.), uv));
	
	}
		shade = bright_text / max(epsilon, dist - THICKNESS);
		color += mix(vec3(1.0, 0.65, 0.02), vec3(0.02, 0.65, 1.0), colorMix) * shade;	

	return color;
}


void main() 
{
	vec2 fragCoord = tCoords * iResolution;
	vec2 aspect = iResolution.xy / iResolution.y;
	vec2 uv = fragCoord.xy / iResolution.y - aspect/2.0;
	seed = floor(iGlobalTime);
	
	
//    vec2 offs = vec2(9.0, 1.5) * scale/2.0;
  
	vec2 offs = vec2(0.0, 0.0);
    float dist = 0.0;
    float shade = 0.0;
    vec3 color = vec3(0);
    
    //Flicker fade in effect.
    float tf_text = max(epsilon, iGlobalTime - 0.6);
    float bright_text = BRIGHTNESS * min(1.0, 1.0 - sin(tf_text * pi * 50.0) / (tf_text * pi * 1.3));
    
    float tf_bord = max(epsilon, iGlobalTime - 0.5);
    float bright_bord = BRIGHTNESS * min(1.0, 1.0 - sin(tf_bord * pi * 50.0) / (tf_bord * pi * 1.3));
    
    //"Shadertoy"

	color = dfLogo(uv + offs);
	
    
    //Border
//    dist = dfBorder(uv + offs);
	
//	shade = bright_bord / max(epsilon, dist - THICKNESS);
	
//	color += BORDER_COLOR * shade;
	
	fragColor = vec4(color , 1.0);
}