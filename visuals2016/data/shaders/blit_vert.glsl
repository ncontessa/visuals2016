#version 410
in vec2 coords;
out vec2 vCoords;
out vec2 tCoords;

uniform vec2 tScale;
uniform vec2 tOffset;

void main() {
	vCoords = coords;
    tCoords = (coords + vec2(1.0, 1.0)) * 0.5;
    vCoords *= tScale;
    vCoords += tOffset;
    gl_Position.xy = vCoords;
	gl_Position.zw = vec2(0.0, 1.0);
}