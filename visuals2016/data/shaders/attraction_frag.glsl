#version 410
uniform vec2 iResolution;
uniform float iGlobalTime;
uniform sampler2D iChannel0;
uniform vec3 color;
out vec4 fragColor;
in vec2 vCoords;
in vec2 tCoords;
uniform float mixColor;
uniform float scene;
uniform float amount;

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

float noise( in vec3 x )
{
    vec3 p = floor(x);
    
    vec3 f = fract(x);
    
    f = f*f*(3.0-2.0*f);
    
    vec2 uv = (p.xy+vec2(37.0,17.0)*p.z) + f.xy;
    vec2 rg = texture( iChannel0, (uv+0.5)/256.0, -100.0).yx;
    return mix( rg.x, rg.y, f.z);
}

void main()
{
	vec2 p = tCoords * iResolution;
    float 
        e = iGlobalTime*.3+5.,
        m = 0.,
        y;
    
    vec2 
        s = iResolution.xy,
		v = 2.*(2.*p.xy-s)/s.y;
    
	const int num = 160;
	vec2 f = vec2(sqrt(float(num)) * (iResolution.x / iResolution.y), sqrt(float(num)) * (iResolution.x / iResolution.y));
	const vec3 color1 = vec3(53. / 255.0, 36.0/255.0, 20.0/255.0);
	const vec3 color2 = vec3(5.0 / 255.0, 4.0/255.0, 2.0/255.0);
	vec3 col = vec3(0.);
	
//	float m;
	
    for(int i=0;i<num;i++)
    {
		vec3 color1, color2;
       	y = 6.28*(mod(i, float(num) * 0.5))/float(num) * 2.0;
		float j = float(i) / float(num);
		vec2 offs = vec2((iResolution.x / iResolution.y) * 0.8, 0.0);
		
		if(i < (num * 0.5)) {
			color1 = hsv2rgb(vec3(iGlobalTime + j, 1.0, 0.05));
			color2 = hsv2rgb(vec3(iGlobalTime + j, 1.0, 0.01));
			offs *= -1.0;
		} else {
			color1 = hsv2rgb(vec3(-iGlobalTime + j + 0.5, 1.0, 0.05));
			color2 = hsv2rgb(vec3(-iGlobalTime + j + 0.5, 1.0, 0.01));
		
		}
		vec2 t = vec2(cos(y), sin(y)) * (0.4 + abs(noise(vec3(y * 10.0, iGlobalTime * 4.0 + iGlobalTime * 1.47, 19.0)))) * (amount + 0.8);
		t += offs;
		
		vec2 t2 = vec2(cos(y + iGlobalTime * 0.2), sin(y + iGlobalTime * 0.2));
		t2.x += noise(vec3(i * 10.0, iGlobalTime * 6.0, 38.0))*  0.2;	
		t2.y += noise(vec3(i * 17.0, iGlobalTime * 8.0, 19.0))*  0.2;	
		
		
		s = v - mix(t, t2, scene);
        
        m = 2e-2/dot(s,s);
	    col += mix(color1, color2, mixColor) * m;
        
    }
    
	
    fragColor.rgb = col;
}