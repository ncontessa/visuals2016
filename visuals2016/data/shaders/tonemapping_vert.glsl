#version 410
in vec2 coords;
out vec2 vCoords;
out vec2 tCoords;
void main() {
    vCoords = coords;
    tCoords = (coords + vec2(1.0, 1.0)) * 0.5;
    gl_Position.xy = coords;
    gl_Position.zw = vec2(0.0, 1.0);
}