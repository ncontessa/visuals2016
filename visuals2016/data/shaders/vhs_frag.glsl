//Noise animation - Electric
//by nimitz (stormoid.com) (twitter: @stormoid)

//The domain is displaced by two fbm calls one for each axis.
//Turbulent fbm (aka ridged) is used for better effect.


#version 410
uniform vec2 iResolution;
uniform float iGlobalTime;
uniform sampler2D iChannel0;
out vec4 fragColor;
in vec2 vCoords;
in vec2 tCoords;

highp float rand(vec2 co)
{
    highp float a = 12.9898;
    highp float b = 78.233;
    highp float c = 43758.5453;
    highp float dt= dot(co.xy ,vec2(a,b));
    highp float sn= mod(dt,3.14);
    return fract(sin(sn) * c);
}

void main()
{
	vec2 fragCoord = tCoords * iResolution;
	vec2 uv = fragCoord.xy / iResolution.xy;
	// Flip Y Axis
	
	highp float magnitude = 0.0016;
	
	
	// Set up offset
	vec2 offsetRedUV = uv;
	offsetRedUV.x = uv.x + rand(vec2(iGlobalTime*0.03,uv.y*0.42)) * 0.001;
	offsetRedUV.x += sin(rand(vec2(iGlobalTime*0.2, uv.y)))*magnitude;
	
	vec2 offsetGreenUV = uv;
	offsetGreenUV.x = uv.x + rand(vec2(iGlobalTime*0.004,uv.y*0.002)) * 0.004;
	offsetGreenUV.x += sin(iGlobalTime*9.0)*magnitude;
	
	vec2 offsetBlueUV = uv;
	offsetBlueUV.x = uv.y;
	offsetBlueUV.x += rand(vec2(cos(iGlobalTime*0.01),sin(uv.y)));
	
	// Load Texture
	float r = texture(iChannel0, offsetRedUV).r;
	float g = texture(iChannel0, offsetGreenUV).g;
	float b = texture(iChannel0, uv).b;
	
	fragColor = vec4(r,g,b,1);
	
}