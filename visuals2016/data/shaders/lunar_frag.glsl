#version 410
uniform vec2 iResolution;
uniform float iGlobalTime;
uniform sampler2D iChannel0;
out vec4 fragColor;
in vec2 vCoords;
in vec2 tCoords;
const float TERR_WGT = 16.0 ; // Weight of the line that makes up the terrain.

// Channel definitions.
#define NOISE_TEX iChannel0

// FBM distance estimation sample differential.
#define EPSILON .005
#define SPEED 0.3
#define VSPEED 0.2

/* 
    The seminal RNGF.
 */
float rand( in float t ){
    return fract(sin(t*1230.114) * 43758.5453);
}

/* 
    A simpler FBM for non-crucial samples, such as for
    the camera zoom.
 */
float partialFBM( in vec2 uv )
{
    
    float result = texture(NOISE_TEX, uv    ).r*1.000;
    result += texture(NOISE_TEX, uv*2.0).r*.5000;
    result += texture(NOISE_TEX, uv*4.01).r*.2500;
    return result * .5714; // * (1.0/1.75);
}

/* 
    The FBM that gives rise to the terrain. (literally)
 */
float fbm( in vec2 uv )
{
    float result = texture(NOISE_TEX, uv    ).r*1.000;
    result += texture(NOISE_TEX, uv*2.0).r*.5000;
    result += texture(NOISE_TEX, uv*4.01).r*.2500;
    result += texture(NOISE_TEX, uv*8.0).r*.1250;
    result += texture(NOISE_TEX, uv*16.).r*.0625;
    return result * .533333; // * (1.0/1.875);
}

/* 
    The distance to the lunar surface, or rather 
    to a transformed FBM.
 */
float distLunarSurface( in vec2 uv )
{
    uv.x *= .0125;
    vec2 q = uv; q.y=0.0;
    float f = fbm(q);
    return uv.y - f*f*1.;
}

/*
    Returns the distance to the surface with less high frequency
    noise. Used to make the camera less scizophrenic.
*/
float distSoftLunarSurface( in vec2 uv )
{
    uv.x *= .0125;
    vec2 q = uv; q.y=0.0;
    float f = partialFBM(q);
    return uv.y - f*f*1.;
}

/*
    Draws the lunar surface.
*/
float drawLunarSurface( in vec2 uv )
{
    return 1.0 - step( TERR_WGT/iResolution.x, abs(distLunarSurface(uv)) );
}

vec4 drawPlayfield( in vec2 uv)
{
    float lunar = drawLunarSurface(uv);//-vec4(1,0,1,0)*pad;
    return vec4(lunar);
}

/*
    Shadertoy's entry function.
*/
void main()
{    
	vec2 fragCoord = tCoords * iResolution;
    
	vec2 uv = fragCoord.xy / iResolution.xy;
    vec2 p, s, h = uv; // For the playfield, ship, and hud since they all need different transforms.
    uv = uv*2.0 - 1.0;
	uv.x *= iResolution.x/iResolution.y; //fix aspect ratio
    p = uv; s = uv;
	p.x += iGlobalTime * SPEED;
	p.y += 0.3;
    vec4 pfld = drawPlayfield(p);
    
    // Alpha everything together and ship it.
	fragColor = pfld;
}