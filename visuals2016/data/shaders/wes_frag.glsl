#version 410
uniform vec2 iResolution;
uniform float iGlobalTime;
out vec4 fragColor;
in vec2 vCoords;
uniform float zoomFactor;
in vec2 tCoords;// Written by GLtracy
 
 // http://www.pouet.net/prod.php?which=57245
//

#define t iGlobalTime
#define r iResolution.xy

void main(){
	vec2 fragCoord = tCoords * iResolution;
	vec3 c;
	float l,z=t * 1.5;
	for(int i=0;i<3;i++) {
		vec2 uv,p=(fragCoord.xy/r);
		uv=p;
		p-=.5;
		p.x*=r.x/r.y;
        
        p *= zoomFactor;
        
		z+=.06;
		l=length(p);
		uv+=p/l*(sin(z + 0.2)+(1.8 + sin(z + 0.5) * cos(z + 0-7)))*abs(sin(l*9.-z*2. + 1.3));
		c[i]= .01/length(abs(mod(uv,1.)-0.5));
	}
	fragColor=vec4(c/l,.0) * vec4(0.8, 0.6, 0.3, 1.0);
}
