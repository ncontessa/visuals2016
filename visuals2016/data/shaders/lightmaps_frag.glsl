#version 410
uniform vec2 iResolution;
uniform float iGlobalTime;
uniform sampler2D iChannel0;
uniform sampler2D iChannel1;
uniform sampler2D iChannel2;
uniform sampler2D iChannel3;
uniform float pos1x;
uniform float pos1y;
uniform float amount1;
uniform float pos2x;
uniform float pos2y;
uniform float amount2;
uniform vec3 color1;
uniform vec3 color2;

out vec4 fragColor;
in vec2 vCoords;
in vec2 tCoords;// Written by GLtracy


void main( )
{
	vec2 fragCoord = tCoords * iResolution;
	vec2 iMouse = iResolution / vec2(2.0, 2.0);
	vec2 uv = fragCoord.xy / iResolution.xy;
    vec2 pixelSize = 1. / iResolution.xy;
    vec2 aspect = vec2(1.,iResolution.y/iResolution.x);

//    vec4 noise = texture(iChannel3, fragCoord.xy / vec2(256.0, 256.0) + fract(vec2(42,56)*iGlobalTime));
    
	vec2 lightSize=vec2(1.7);

    // get the gradients from the blurred image

//	vec2 displacement = vec2(dx.x,dy.x)*lightSize; // using only the red gradient as displacement vector
	float light1 = pow(max(1.-distance(pos1x+(uv-0.5)*aspect*lightSize ,pos1y+(iMouse.xy*pixelSize-0.5)*aspect*lightSize),0.),4.);
	float light2 = pow(max(1.-distance(pos2x+(uv-0.5)*aspect*lightSize ,pos2y+(iMouse.xy*pixelSize-0.5)*aspect*lightSize),0.),4.);
	light1 = pow(light1 * amount1, 1.0/2.2);
	light2 = pow(light2 * amount2, 1.0/2.2);
	light1 *= 5.0;
	
    // and add the light map
    fragColor.rgb = light1 * color1 + light2 * color2; 
	
	//gl_FragColor = texture(sampler_prev, pixel); // bypass    
}