#version 410
uniform vec2 iResolution;
uniform float iGlobalTime;
uniform sampler2D iChannel0;
uniform vec3 color;
out vec4 fragColor;
in vec2 vCoords;
in vec2 tCoords;
uniform float mixColor;
uniform float amount;
uniform float freq;

float noise( in vec3 x )
{
    vec3 p = floor(x);
    
    vec3 f = fract(x);
    
    f = f*f*(3.0-2.0*f);
    
    vec2 uv = (p.xy+vec2(37.0,17.0)*p.z) + f.xy;
    vec2 rg = texture( iChannel0, (uv+0.5)/256.0, -100.0).yx;
    return mix( rg.x, rg.y, f.z);
}

void main()
{
	vec2 p = tCoords * iResolution;
    float 
        e = iGlobalTime*.3+5.,
        m = 0.,
        y;
    
    vec2 
        s = iResolution.xy,
		v = 2.*(2.*p.xy-s)/s.y;
    
	const int num = 200;
	const vec3 color1 = vec3(453.0 / 255.0, 36.0/255.0, 20.0/255.0);
	const vec3 color2 = vec3(53.0 / 255.0, 36.0/255.0, 20.0/255.0);
	
	
    for(int i=0;i<num;i++)
    {
		float ratio = (iResolution.x / iResolution.y) * 2.1;
		vec2 t;
		t.x	= -ratio + ratio * 2.0 * (float(i) / float(num));
		t.y = 2.0 * (amount + 0.05) * (noise(vec3(t.x * freq, iGlobalTime * 6.0 + freq * 5.0 * amount, 39.0)) - 0.5);
		s = v - t;
        
		float q = mix(1e-3, 2e-3, mixColor);
		
        m += (q)/dot(s,s);
        
    }
    
	vec3 col = mix(color2, color1, mixColor);
	
    fragColor = 
            mix(
                vec4(col, 1.0) * m, 
                vec4(0.0), 
                clamp(m, 0., 0.4)
            );
			
	fragColor = pow(fragColor, vec4(0.454545));
}