#version 410

in vec2 vCoords;
in vec2 tCoords;
out vec4 fragColor;
uniform vec3 color;
uniform sampler2D input0;

void main()
{
    fragColor = vec4(pow(texture(input0, tCoords).rgb, vec3(2.2)) * color, texture(input0, tCoords).a);
}