#version 410

in vec2 vCoords;
in vec2 tCoords;
out vec4 fragColor;

uniform vec3 color;

void main()
{
    fragColor = vec4(color, 1.0);
}