#version 410
uniform sampler3D tone;
uniform sampler2D input0;
uniform float amount;
out vec4 fragColor;
in vec2 vCoords;
in vec2 tCoords;


void main() {
	
	vec4 inColor = texture(input0, tCoords);

	vec3 mappedColor = texture(tone, inColor.rbg).rgb;

//	vec3 mappedColor = vec3(1.0, 0.0, 0.0);
	fragColor.a = inColor.a;
	fragColor.rgb = mix(mappedColor, inColor.rgb, amount);

}