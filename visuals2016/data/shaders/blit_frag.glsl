#version 410

in vec2 vCoords;
in vec2 tCoords;
out vec4 fragColor;
uniform sampler2D input0;

void main()
{

    fragColor = texture(input0, tCoords);
}