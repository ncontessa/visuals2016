#version 410

in vec2 vCoords;
in vec2 tCoords;
out vec4 fragColor;

uniform sampler2D input0;
uniform float saturation;

float intensity(vec3 color) {
    return (0.299*color.r + 0.587*color.g + 0.114*color.b);
}

void main()
{
    vec3 color = texture(input0, tCoords).rgb;
	fragColor.a = texture(input0, tCoords).a;
	fragColor.rgb = mix(vec3(intensity(color)), color, saturation);
	
}