#version 410
uniform vec2 iResolution;
uniform float iGlobalTime;
uniform sampler2D iChannel0;
uniform sampler2D iChannel1;
uniform sampler2D iChannel2;
uniform sampler2D iChannel3;
out vec4 fragColor;
in vec2 vCoords;
in vec2 tCoords;// Written by GLtracy


void main( )
{
	vec2 fragCoord = tCoords * iResolution;
	vec2 iMouse = iResolution / vec2(2.0, 2.0);
	vec2 uv = fragCoord.xy / iResolution.xy;
    vec2 pixelSize = 1. / iResolution.xy;
    vec2 aspect = vec2(1.,iResolution.y/iResolution.x);

//    vec4 noise = texture(iChannel3, fragCoord.xy / vec2(256.0, 256.0) + fract(vec2(42,56)*iGlobalTime));
    
	vec2 lightSize=vec2(4.);

    // get the gradients from the blurred image
	vec2 d = pixelSize*2.;
	vec4 dx = (texture(iChannel2, uv + vec2(1,0)*d) - texture(iChannel2, uv - vec2(1,0)*d))*0.5;
	vec4 dy = (texture(iChannel2, uv + vec2(0,1)*d) - texture(iChannel2, uv - vec2(0,1)*d))*0.5;

	// add the pixel gradients
	d = pixelSize*1.;
	dx += texture(iChannel0, uv + vec2(1,0)*d) - texture(iChannel0, uv - vec2(1,0)*d);
	dy += texture(iChannel0, uv + vec2(0,1)*d) - texture(iChannel0, uv - vec2(0,1)*d);

	vec2 displacement = vec2(dx.x,dy.x)*lightSize; // using only the red gradient as displacement vector
	float light = pow(max(1.-distance(0.5+(uv-0.5)*aspect*lightSize + displacement,0.5+(iMouse.xy*pixelSize-0.5)*aspect*lightSize),0.),4.);

	// recolor the red channel
	vec4 rd = vec4(texture(iChannel0,uv+vec2(dx.x,dy.x)*pixelSize*8.).x)*vec4(2.0,1.5,0.2,1.0)-vec4(1.0,1.0,1.0,1.0);

    // and add the light map
    fragColor = mix(rd,vec4(1.0,1.,1.,1.), light*0.75*vec4(1.-texture(iChannel0,uv+vec2(dx.x,dy.x)*pixelSize*8.).x)); 
	
	//gl_FragColor = texture(sampler_prev, pixel); // bypass    
}