//Noise animation - Electric
//by nimitz (stormoid.com) (twitter: @stormoid)

//The domain is displaced by two fbm calls one for each axis.
//Turbulent fbm (aka ridged) is used for better effect.


#version 410
uniform vec2 iResolution;
uniform float iGlobalTime;
uniform sampler2D iChannel0;
out vec4 fragColor;
in vec2 vCoords;
in vec2 tCoords;
 float seed;
// horizontal Gaussian blur passfloat pi = atan(1.0)*4.0;
float pi = atan(1.0)*4.0;
float tau = atan(1.0)*8.0;

float scale = 1.0 / 6.0;

float epsilon = 1e-3;
float infinity = 1e6;

//Settings
//Uses cheaper arcs for common sweep angles (90 & 180 degrees).
#define USE_CHEAP_ARCS

#define TEXT_COLOR   vec3(1.00, 0.20, 0.10)
#define BORDER_COLOR vec3(0.05, 0.20, 1.00)

#define BRIGHTNESS 0.004
#define THICKNESS  0.002
#define HASHSCALE .1031

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

float rand(float p)
{
	vec3 p3  = fract(vec3(p) * HASHSCALE);
    p3 += dot(p3, p3.yzx + 19.19);
    return fract((p3.x + p3.y) * p3.z);
}


//Checks if a and b are approximately equal.
bool ApproxEqual(float a, float b)
{
    return abs(a - b) <= epsilon;
}

//Distance to a line segment,
float dfLine(vec2 start, vec2 end, vec2 uv)
{
//	start *= scale;
//	end *= scale;
    
	vec2 line = end - start;
	float frac = dot(uv - start,line) / dot(line,line);
	return distance(start + line * clamp(frac, 0.0, 1.0), uv);
}

//Distance to an arc.
float dfArc(vec2 origin, float start, float sweep, float radius, vec2 uv)
{
	origin *= scale;
	radius *= scale;  
	uv -= origin;
    
	uv *= mat2(cos(start), sin(start),-sin(start), cos(start));
	
    #ifdef USE_CHEAP_ARCS
        if(ApproxEqual(sweep, pi)) //180 degrees
        {
            float d = abs(length(uv) - radius) + step(uv.y, 0.0) * infinity;
            d = min(d, min(length(uv - vec2(radius, 0)), length(uv + vec2(radius, 0))));
            return d;
        }
        else if(ApproxEqual(sweep, pi/2.0)) //90 degrees
        {
            float d = abs(length(uv) - radius) + step(min(uv.x, uv.y), 0.0) * infinity;
            d = min(d, min(length(uv - vec2(0, radius)), length(uv - vec2(radius, 0))));
            return d;
        }
        else //Others
        {
            float offs = (sweep / 2.0 - pi);
            float ang = mod(atan(uv.y, uv.x) - offs, tau) + offs;
            ang = clamp(ang, min(0.0, sweep), max(0.0, sweep));

            return distance(radius * vec2(cos(ang), sin(ang)), uv); 
        }
    #else
        float offs = (sweep / 2.0 - pi);
        float ang = mod(atan(uv.y, uv.x) - offs, tau) + offs;
        ang = clamp(ang, min(0.0, sweep), max(0.0, sweep));

        return distance(radius * vec2(cos(ang), sin(ang)), uv);
	#endif
}

vec2 actCurve(in float t, in vec2 from, in vec2 to, in vec2 freq, in vec2 ins) {
		vec2 pos = from + (to - from) * t;
		pos += vec2(sin(12. * t), cos(6.17 * t)) * ins;
	return pos;
}

float curve(in vec2 from, in vec2 to, in vec2 freq, in vec2 ins, in vec2 uv) {

	float iterations = 40.0;
	float step = 1.0 / iterations;
	float t = 0.0;
	float dist = infinity;
	for (float i = 0.0; i < iterations; i += 1.0) {	
		
		vec2 pos = from + (to - from) * t;
		vec2 p1 = actCurve(t, from, to, freq, ins);
		vec2 p2 = actCurve(t + step, from, to, freq, ins);
		
		dist = min(dist, dfLine(p1, p2, uv));
		t += step;
	}

	return dist;

}

vec3 dfLogo(vec2 uv)
{
	float dist = infinity;

//	dist = min(dist, dfLine(vec2(-10.,-10.), vec2(10.,10.), uv));
    float tf_text = max(epsilon, iGlobalTime - 0.6);
    float bright_text = BRIGHTNESS * min(1.0, 1.0 - sin(tf_text * pi * 50.0) / (tf_text * pi * 1.3));
    float shade = 0.0;
	
	vec3 color = vec3(0);
	for(int i = 0; i < 20; i++) {

		float angle = tau * (rand(seed + i * 47.0));
		vec2 orig = vec2(cos(angle), sin(angle)) * 2.0;
		dist = min(dist, curve(-orig, orig, vec2(rand(seed + i * 17.0), rand(seed + i * 29.0)), vec2(rand(seed + i * 7.0), rand(seed + i * 37.0)), uv));
	
	}
		shade = bright_text / max(epsilon, dist - THICKNESS);
		color += hsv2rgb(vec3(rand(seed  * 0.4 + 6. * 1.0), 1.0, 1.0)) * shade;	

	return color;
}

float dfBorder(vec2 uv)
{
    float dist = infinity;
    
	dist = min(dist, dfLine(vec2(0.0,1.533), vec2(8.733,1.533), uv));
	dist = min(dist, dfLine(vec2(9.133,1.133), vec2(9.133,0.067), uv));
	dist = min(dist, dfLine(vec2(8.733,-0.333), vec2(4.467,-0.333), uv));
	dist = min(dist, dfLine(vec2(-0.333,0.067), vec2(-0.333,1.133), uv));
	dist = min(dist, dfLine(vec2(0.067,1.400), vec2(4.333,1.400), uv));
	dist = min(dist, dfLine(vec2(9.000,1.133), vec2(9.000,0.067), uv));
	dist = min(dist, dfLine(vec2(8.733,-0.200), vec2(0.067,-0.200), uv));
	dist = min(dist, dfLine(vec2(-0.200,0.067), vec2(-0.200,1.133), uv));
	dist = min(dist, dfLine(vec2(4.333,-0.333), vec2(0.067,-0.333), uv));
	dist = min(dist, dfLine(vec2(4.467,1.400), vec2(8.733,1.400), uv));
	dist = min(dist, dfArc(vec2(8.733,1.133),0.000, 1.571, 0.400, uv));
	dist = min(dist, dfArc(vec2(8.733,0.067),4.712, 1.571, 0.400, uv));
	dist = min(dist, dfArc(vec2(0.067,0.067),3.142, 1.571, 0.400, uv));
	dist = min(dist, dfArc(vec2(0.067,1.133),1.571, 1.571, 0.400, uv));
	dist = min(dist, dfArc(vec2(8.733,1.133),0.000, 1.571, 0.267, uv));
	dist = min(dist, dfArc(vec2(8.733,0.067),4.712, 1.571, 0.267, uv));
	dist = min(dist, dfArc(vec2(0.067,0.067),3.142, 1.571, 0.267, uv));
	dist = min(dist, dfArc(vec2(0.067,1.133),1.571, 1.571, 0.267, uv));
    
    return dist;
}


void main() 
{
	vec2 fragCoord = tCoords * iResolution;
	vec2 aspect = iResolution.xy / iResolution.y;
	vec2 uv = fragCoord.xy / iResolution.y - aspect/2.0;
	seed = floor(iGlobalTime);
	
	
//    vec2 offs = vec2(9.0, 1.5) * scale/2.0;
  
	vec2 offs = vec2(0.0, 0.0);
    float dist = 0.0;
    float shade = 0.0;
    vec3 color = vec3(0);
    
    //Flicker fade in effect.
    float tf_text = max(epsilon, iGlobalTime - 0.6);
    float bright_text = BRIGHTNESS * min(1.0, 1.0 - sin(tf_text * pi * 50.0) / (tf_text * pi * 1.3));
    
    float tf_bord = max(epsilon, iGlobalTime - 0.5);
    float bright_bord = BRIGHTNESS * min(1.0, 1.0 - sin(tf_bord * pi * 50.0) / (tf_bord * pi * 1.3));
    
    //"Shadertoy"

	color = dfLogo(uv + offs);
	
    
    //Border
//    dist = dfBorder(uv + offs);
	
//	shade = bright_bord / max(epsilon, dist - THICKNESS);
	
//	color += BORDER_COLOR * shade;
	
	fragColor = vec4(color , 1.0);
}