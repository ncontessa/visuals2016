#version 410

in vec2 vCoords;
in vec2 tCoords;
out vec4 fragColor;
uniform float blend0;
uniform float blend1;

uniform sampler2D input0;
uniform sampler2D input1;

void main()
{

    fragColor = vec4(texture(input0, tCoords) * blend0 +
                       texture(input1, tCoords) * blend1);
}