#version 410
uniform vec2 iResolution;
uniform float iGlobalTime;
uniform sampler2D iChannel0;
out vec4 fragColor;
in vec2 vCoords;
in vec2 tCoords;
uniform float c;

// Jason Allen Doucette
// http://xona.com/jason/
//
// 3D Tunnel
// May 16, 2016

// ---- SETTINGS ----------------------------------------------------------------

#define Z_SCALE 0.2

uniform float SPEED_IN; 
uniform float SPEED_ROTATE;
#define ROTATE_MAGNITUDE 0.3

// http://xona.com/colorlist/
#define FADE_COLOR vec3(c, c, c)
#define FADE_POWER 1.0


// ---- CONSTANTS ----------------------------------------------------------------

#define PI 3.1415926535897932384626

// ---- CODE ----------------------------------------------------------------

void main()
{
	vec2 fragCoord = tCoords * iResolution;
    // isotropic scaling, ensuring entire texture fits into the view.
    // After this, you should consider fragCoord = 0..1, usually,
    // aside from overflow for wide-screen.
    float minRes = min(iResolution.x, iResolution.y);
    fragCoord /= minRes;
    
    // center point on screen
    vec2 center = (iResolution.xy / minRes) / 2.0;
    
    // angle from center
    vec2 dCenter = center - fragCoord.xy;
    float angle = atan(dCenter.y, dCenter.x) / PI;
    
    // distance from center
    float distancePixFromCen = sqrt(dCenter.x * dCenter.x + dCenter.y * dCenter.y);
    
    // 3D perspective: 1/Z = constant
    float zCamera = Z_SCALE / distancePixFromCen;
    
    // static texture coordinates
    fragCoord.xy = vec2(angle, zCamera);
    
    // move in Z -- into the tunnel
    fragCoord.y += iGlobalTime * SPEED_IN;
    
    // move in X -- rotate
    fragCoord.x += iGlobalTime * SPEED_ROTATE;
    
    // textured
    fragColor = texture(iChannel0, fragCoord);
    
    // fade into distance
    // need formula that changes z distnace = 0..infinity, into a fade = 0..1
    // y=1/x, from x=1..infinity, y=1..0, thus:
    // y=1-(1/x), from x=1..infinity, gets y=0..1
   
    // 0=no fade, 1=full fade
    float fade = 1.0 - (1.0 / (1.0 + zCamera * FADE_POWER));
        
    fragColor.rgb = fragColor.rgb * (1. - fade) + FADE_COLOR.rgb * (fade);
}
