#version 410
//base
uniform sampler2D input0;
in vec2 texCoords;
in vec2 blurTexCoords[14];
out vec4 fragColor;

void main() {
    vec4 glowColor = vec4(0.0);
    glowColor += texture(input0, blurTexCoords[ 0])*0.0044299121055113265;
    glowColor += texture(input0, blurTexCoords[ 1])*0.00895781211794;
    glowColor += texture(input0, blurTexCoords[ 2])*0.0215963866053;
    glowColor += texture(input0, blurTexCoords[ 3])*0.0443683338718;
    glowColor += texture(input0, blurTexCoords[ 4])*0.0776744219933;
    glowColor += texture(input0, blurTexCoords[ 5])*0.115876621105;
    glowColor += texture(input0, blurTexCoords[ 6])*0.147308056121;
    glowColor += texture(input0, texCoords)*0.159576912161;
    glowColor += texture(input0, blurTexCoords[ 7])*0.147308056121;
    glowColor += texture(input0, blurTexCoords[ 8])*0.115876621105;
    glowColor += texture(input0, blurTexCoords[ 9])*0.0776744219933;
    glowColor += texture(input0, blurTexCoords[10])*0.0443683338718;
    glowColor += texture(input0, blurTexCoords[11])*0.0215963866053;
    glowColor += texture(input0, blurTexCoords[12])*0.00895781211794;
    glowColor += texture(input0, blurTexCoords[13])*0.0044299121055113265;
    
    fragColor = clamp(glowColor, 0.0, 1.0);
}