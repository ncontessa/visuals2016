// (c) sonictooth@gmail.com
#version 410
in vec2 vCoords;
out vec2 texCoords;
uniform bool direction;
uniform vec2 iResolution;
out vec2 blurTexCoords[14];

void main() {
    float texWidth = (1.0 / iResolution.x);
    float texHeight = (1.0 / iResolution.y);
    texCoords = (vCoords + vec2(1.0)) * 0.5;
    if(direction) {
        blurTexCoords[ 0] = texCoords + vec2(-7.0 * texWidth, 0.0);
        blurTexCoords[ 1] = texCoords + vec2(-6.0 * texWidth, 0.0);
        blurTexCoords[ 2] = texCoords + vec2(-5.0 * texWidth, 0.0);
        blurTexCoords[ 3] = texCoords + vec2(-4.0 * texWidth, 0.0);
        blurTexCoords[ 4] = texCoords + vec2(-3.0 * texWidth, 0.0);
        blurTexCoords[ 5] = texCoords + vec2(-2.0 * texWidth, 0.0);
        blurTexCoords[ 6] = texCoords + vec2(-1.0 * texWidth, 0.0);
        blurTexCoords[ 7] = texCoords + vec2( 1.0 * texWidth, 0.0);
        blurTexCoords[ 8] = texCoords + vec2( 2.0 * texWidth, 0.0);
        blurTexCoords[ 9] = texCoords + vec2( 3.0 * texWidth, 0.0);
        blurTexCoords[10] = texCoords + vec2( 4.0 * texWidth, 0.0);
        blurTexCoords[11] = texCoords + vec2( 5.0 * texWidth, 0.0);
        blurTexCoords[12] = texCoords + vec2( 6.0 * texWidth, 0.0);
        blurTexCoords[13] = texCoords + vec2( 7.0 * texWidth, 0.0);
    } else {
        blurTexCoords[ 0] = texCoords + vec2(0.0, -7.0 * texHeight);
        blurTexCoords[ 1] = texCoords + vec2(0.0, -6.0 * texHeight);
        blurTexCoords[ 2] = texCoords + vec2(0.0, -5.0 * texHeight);
        blurTexCoords[ 3] = texCoords + vec2(0.0, -4.0 * texHeight);
        blurTexCoords[ 4] = texCoords + vec2(0.0, -3.0 * texHeight);
        blurTexCoords[ 5] = texCoords + vec2(0.0, -2.0 * texHeight);
        blurTexCoords[ 6] = texCoords + vec2(0.0, -1.0 * texHeight);
        blurTexCoords[ 7] = texCoords + vec2(0.0,  1.0 * texHeight);
        blurTexCoords[ 8] = texCoords + vec2(0.0,  2.0 * texHeight);
        blurTexCoords[ 9] = texCoords + vec2(0.0,  3.0 * texHeight);
        blurTexCoords[10] = texCoords + vec2(0.0,  4.0 * texHeight);
        blurTexCoords[11] = texCoords + vec2(0.0,  5.0 * texHeight);
        blurTexCoords[12] = texCoords + vec2(0.0,  6.0 * texHeight);
        blurTexCoords[13] = texCoords + vec2(0.0,  7.0 * texHeight);
    }
    
    gl_Position.xy = vCoords;
    gl_Position.zw = vec2(0.0, 1.0);
}
