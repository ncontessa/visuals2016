#version 410
// Created by inigo quilez - iq/2013
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.


// This shader computes the distance to the Mandelbrot Set for everypixel, and colorizes
// it accoringly.
// 
// Z -> Z�+c, Z0 = 0. 
// therefore Z' -> 2�Z�Z' + 1
//
// The Hubbard-Douady potential G(c) is G(c) = log Z/2^n
// G'(c) = Z'/Z/2^n
//
// So the distance is |G(c)|/|G'(c)| = |Z|�log|Z|/|Z'|
//
// More info here: http://www.iquilezles.org/www/articles/distancefractals/distancefractals.htm
uniform vec2 resolution;
uniform vec2 displacement;
uniform float zoom;
uniform sampler2D tex;
uniform sampler2D tex2;
uniform float time;

in vec2 vCoords;
in vec2 tCoords;
out vec4 fragColor;

void fractal() {
    vec2 p = vCoords;
    //    vec2 p = -1.0 + 2.0 * fragCoord.xy / iResolution.xy;
    p.x *= resolution.x/resolution.y;
    
    // animation
    //	float tz = 0.5 - 0.5*cos(0.225*1.0) + zoom;
    float tz = zoom;
    float zoo = pow( 0.5, 13.0*tz);
    
    vec2 c = (vec2(-0.05,.6805) +displacement) + (p *zoo);
    
    // iterate
    vec2 z  = vec2(0.0);
    float m2 = 0.0;
    vec2 dz = vec2(0.0);
    for( int i=0; i<256; i++ )
    {
        if( m2>1024.0 ) continue;
        
        // Z' -> 2�Z�Z' + 1
        dz = 2.0*vec2(z.x*dz.x-z.y*dz.y, z.x*dz.y + z.y*dz.x) + vec2(1.0,0.0);
        
        // Z -> Z� + c
        z = vec2( z.x*z.x - z.y*z.y, 2.0*z.x*z.y ) + c;
        
        m2 = dot(z,z);
    }
    
    // distance
    // d(c) = |Z|�log|Z|/|Z'|
    float d = 0.5*sqrt(dot(z,z)/dot(dz,dz))*log(dot(z,z));
    
    
    // do some soft coloring based on distance
    d = clamp( 8.0*d/zoo, 0.0, 1.0 );
    d = pow( d, 0.25 );
    d = max(d, 0.0);
    vec3 col = vec3( d );
    
    fragColor += vec4( col, 1.0 ) * 0.3;
}

void main()
{

    fragColor = vec4(texture(tex, tCoords + vec2(sin(time))) * 0.5+
                       texture(tex2, tCoords - vec2(sin(time))) * 0.5);
//    fractal();
	//	fragColor = vec4(1.0, 0, 0, 1);
}