#include <GLFW/glfw3.h>
#include <AntTweakBar.h>
#include "log.h"
#include "midi.h"
#include "config.h"

const int MIDI_BUFFER_SIZE = 512;
static midiEvent_t _midiEvents[MIDI_BUFFER_SIZE];
static int _numEvents;

#ifdef _WIN32
#include <windows.h>

HMIDIIN _handle;
static double _startTime;
static CRITICAL_SECTION _midiCriticalSection;

static void CALLBACK MidiInProc(
	HMIDIIN   hMidiIn,
	UINT      wMsg,
	DWORD_PTR dwInstance,
	DWORD_PTR dwParam1,
	DWORD_PTR dwParam2
	) {
	midiEvent_t *midiEvent;
	uint8_t status;
	switch (wMsg) {
	case MIM_DATA:
		status = dwParam1 & 0xf0;
		if (status == MIDI_STATUS_NOTE_OFF
			|| status == MIDI_STATUS_NOTE_ON
			|| status == MIDI_STATUS_PROGRAM_CHANGE) {

			EnterCriticalSection(&_midiCriticalSection);
            if (_numEvents >= MIDI_BUFFER_SIZE) {
                log_warning("Dropping MIDI data, numEvents = %d >= MIDI_BUFFER_SIZE", _numEvents);
                return;
            }

            midiEvent = &_midiEvents[_numEvents];
			midiEvent->midiData = dwParam1;
			midiEvent->timestamp = _startTime + ((double)dwParam2 / 1000.0);
			_numEvents++;
			LeaveCriticalSection(&_midiCriticalSection);
			if (script_get_config()->dumpMidi) {
				log_debug("MIM_DATA dwParam1: %x dwParam2: %x", dwParam1, dwParam2);
			}

		}
		else {
			if (script_get_config()->dumpMidi) {
				log_debug("MIM_DATA dwParam1: %x dwParam2: %x", dwParam1, dwParam2);
			}
		}
		break;
	case MIM_MOREDATA:
		if (script_get_config()->dumpMidi) {
			log_debug("MIM_MOREDATA dwParam1: %x dwParam2: %x", dwParam1, dwParam2);
		}
		break;
	}
}

static int _midi_init_win32() {
	if (_handle) {
		log_warning("MIDI already opened?");
	}
	int numDevs = midiInGetNumDevs();
	log_info("MIDI inputs: %d", numDevs);
	for (int i = 0; i < numDevs; i++) {
		MIDIINCAPSA caps;
		midiInGetDevCapsA(i, &caps, sizeof(caps));
		log_info("Device %d: %s", i, caps.szPname);
	}

	if (!InitializeCriticalSectionAndSpinCount(&_midiCriticalSection, 0x800L)) {
		log_fatal("Error trying to initalize Critical section");
		return 0;
	}

	return 1;
}

static int _midi_open_device_win32(int deviceNumber) {
	if (_handle) {
		log_warning("MIDI already opened?");
	}
	MMRESULT result = midiInOpen(&_handle, deviceNumber, (DWORD_PTR)MidiInProc, NULL, CALLBACK_FUNCTION | MIDI_IO_STATUS);

	if (result != MMSYSERR_NOERROR) {
		return 0;
	}
	result = midiInStart(_handle);
	_startTime = glfwGetTime();
	log_info("MIDI input started on %f", _startTime);
	if (result != MMSYSERR_NOERROR) {
		return 0;
	}
	return 1;
}

static int _midi_shutdown_win32() {
	if (!_handle) {
		log_warning("MIDI shutdown but handle is null!");
		return 1;
	}

	midiInClose(_handle);
	_handle = 0;
	return 1;
}

#else /* ifdef _WIN32 */

#include <RtMidi.h>
#include <pthread.h>

RtMidiIn *_rtMidiIn = NULL;
double _startTime = 0.0;
pthread_mutex_t _lock;

static int _midi_init_rtmidi() {
    if(_rtMidiIn) {
            log_warning("MIDI already opened?");
        return 1;
    }
    _rtMidiIn = new RtMidiIn;
    if(!_rtMidiIn) {
        log_error("Couldn't new RtMidiIn!");
        return 0;
    }
    log_info("Midi IN ports detected: %d", _rtMidiIn->getPortCount());
    for(int i = 0; i < _rtMidiIn->getPortCount(); i++) {
        log_info("MIDI port #%d: %s", i, _rtMidiIn->getPortName(i).c_str());
    }
    
    if(pthread_mutex_init(&_lock, NULL)) {
        log_error("pthread_mutex_init > 0");
        return 0;
    }
    
    return 1;
}

void _midi_callback(double timeStamp, std::vector<unsigned char> *message, void *userData) {
    if(script_get_config()->dumpMidi) {
        log_debug("Midi message with size %d %f", message->size(), glfwGetTime());
    }
    if(message->size() > 4) {
        log_warning("message size > 4, dropping it");
    } else {
        uint32_t value = 0;
        uint8_t status = (*message)[0] & 0xf0;
        
        if (status == MIDI_STATUS_NOTE_OFF
            || status == MIDI_STATUS_NOTE_ON
            || status == MIDI_STATUS_PROGRAM_CHANGE) {
            
            for(int i = 0; i < message->size(); i++) {
                value |= ((uint32_t)(*message)[i]) << (i*8);
            }
            
            double thisTime = glfwGetTime();
            
            // CRITICAL SECTION

            if(pthread_mutex_lock(&_lock)) {
                log_error("pthread_mutex_lock > 0");
                return;
            }
            
            if (_numEvents >= MIDI_BUFFER_SIZE) {
                log_warning("Dropping MIDI data, numEvents = %d >= MIDI_BUFFER_SIZE", _numEvents);
                return;
            }
            
            midiEvent_t* midiEvent = &_midiEvents[_numEvents];
            midiEvent->midiData = value;
            midiEvent->timestamp = thisTime;
            _numEvents++;
            
            if(pthread_mutex_unlock(&_lock)) {
                log_error("pthread_mutex_unlock > 0");
                return;
            }

            if(script_get_config()->dumpMidi) {
                log_info("%X %f", midiEvent->midiData, midiEvent->timestamp);
            }
            // EXIT CRITICAL SECTION
            }
        }
}

static int _midi_open_device_rtmidi(int deviceNumber) {
    if(!_rtMidiIn) {
        log_error("rtMidi = NULL in open_device()");
        return 0;
    }
    int portCount = _rtMidiIn->getPortCount();
    if(deviceNumber >= portCount) {
        log_error("deviceNumber >= portCount! deviceNumber = %d portCount = %d", deviceNumber, portCount);
        return 0;
    }
    _rtMidiIn->openPort(deviceNumber);
    log_info("Opening MIDI port #%d: %s", deviceNumber, _rtMidiIn->getPortName(deviceNumber).c_str());
    _startTime = glfwGetTime();
    _rtMidiIn->setCallback(&_midi_callback);
    _rtMidiIn->ignoreTypes(false, false, false);
    return 1;
    
}

static int _midi_shutdown_rtmidi() {
    _rtMidiIn->cancelCallback();
    if(_rtMidiIn) {
        delete _rtMidiIn;
    }
    return 1;
}

#endif /* ifdef _WIN32 */

int midi_init() {
#ifdef _WIN32
	return _midi_init_win32();
#else
    return _midi_init_rtmidi();
#endif
}

int midi_open_device(int deviceNumber) {
#ifdef _WIN32
	return _midi_open_device_win32(deviceNumber);
#else
    return _midi_open_device_rtmidi(deviceNumber);
#endif
}

int midi_frame_reset() {
    _numEvents = 0;
#ifdef _WIN32
	if (_handle) {
		LeaveCriticalSection(&_midiCriticalSection);
	}
#else
    if(pthread_mutex_unlock(&_lock)) {
        log_error("pthread_mutex_lock > 0");
        return 0;
    }
#endif
	return 1;
}

void midi_force_program_change(int program, int channel) {
    midiEvent_t *midiEvent;
#ifdef _WIN32
	if (_handle) {
		EnterCriticalSection(&_midiCriticalSection);
	}
#else
    if(pthread_mutex_lock(&_lock)) {
        log_error("pthread_mutex_lock > 0");
        return;
    }
#endif
    midiEvent = &_midiEvents[_numEvents];
	midiEvent->midiData = (((channel - 1) & 0xf) | 0xC0 | ((program & 0xff) << 8));
	midiEvent->timestamp = glfwGetTime();
	_numEvents++;
#ifdef _WIN32
	if (_handle) {
		LeaveCriticalSection(&_midiCriticalSection);
	}
#else
    if(pthread_mutex_unlock(&_lock)) {
        log_error("pthread_mutex_unlock > 0");
        return;
    }
#endif
}

void midi_force_note(int note, int channel) {
    midiEvent_t *midiEvent;
//	log_info("midi_force_note %d %d", note, channel);
#ifdef _WIN32
	if (_handle) {
		EnterCriticalSection(&_midiCriticalSection);
	}
#else
    if(pthread_mutex_lock(&_lock)) {
        log_error("pthread_mutex_lock > 0");
        return;
    }
#endif
    double time = glfwGetTime();
    midiEvent = &_midiEvents[_numEvents];
    midiEvent->midiData = (((channel - 1) & 0xf) | 0x80 | ((note & 0xff) << 8)) | (120 << 16);
    midiEvent->timestamp = time - 0.005;
    if(script_get_config()->dumpMidi) {
        log_info("%X %f", midiEvent->midiData, midiEvent->timestamp);
    }
    _numEvents++;
    midiEvent = &_midiEvents[_numEvents];
    midiEvent->midiData = (((channel - 1) & 0xf) | 0x90 | ((note & 0xff) << 8));
    midiEvent->timestamp = time;
    if(script_get_config()->dumpMidi) {
        log_info("%X %f", midiEvent->midiData, midiEvent->timestamp);
    }
    _numEvents++;
#ifdef _WIN32
	if (_handle) {
		LeaveCriticalSection(&_midiCriticalSection);
	}
#else
    if(pthread_mutex_unlock(&_lock)) {
        log_error("pthread_mutex_unlock > 0");
        return;
    }
#endif
    
    
    
}

int midi_get_frame_data(const midiEvent_t** events) {
#ifdef _WIN32
	if (_handle) {
		EnterCriticalSection(&_midiCriticalSection);
	}
#else
    if(pthread_mutex_lock(&_lock)) {
        log_error("pthread_mutex_lock > 0");
        return 0;
    }
#endif
	*events = _midiEvents;
	return _numEvents;
}

int midi_shutdown() {
#ifdef _WIN32
	return _midi_shutdown_win32();
#else
    return _midi_shutdown_rtmidi();
#endif
}