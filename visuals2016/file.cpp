#include <stdio.h>
#include "log.h"
#include "file.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

char *file_read_as_string(const char* filename) {
	FILE *fp = fopen(filename, "rb");
	if (!fp) {
		return NULL;
	}

	fseek(fp, 0, SEEK_END);
	size_t size = ftell(fp);
	char *buffer = (char*)malloc(size + 1);
	if (NULL == buffer) {
		return NULL;
	}
	fseek(fp, 0, SEEK_SET);
	size_t readBytes = fread(buffer, 1, size, fp);
	fclose(fp);
	if (readBytes != size) {
		free(buffer);
		return NULL;
	}
	buffer[size] = '\0';

	return buffer;
}
