//
//  script.hpp
//  visuals2016mac
//
//  Created by Niccolò on 10/01/16.
//  Copyright © 2016 sonictooth. All rights reserved.
//

#ifndef script_hpp
#define script_hpp


int script_init();
int script_frame(double time, const gfx_render_settings *settings);
int script_reload();
void script_shutdown();
int script_load_assets();
int script_load_config();


#endif /* script_hpp */
