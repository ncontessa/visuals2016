//
//  parameters.hpp
//  visuals2016mac
//
//  Created by Niccolò on 10/01/16.
//  Copyright © 2016 sonictooth. All rights reserved.
//

#ifndef parameters_hpp
#define parameters_hpp

#include <glm/glm.hpp>
#include "render.h"

enum {
    PARAM_FLOAT = 1,
    PARAM_COLOR3F,
    PARAM_COLOR4F,
    PARAM_DIRECTION3F
};

typedef struct {
	float x, y, z;
} vec3_t;

typedef struct {
	float x, y, z, w;
} vec4_t;

const int PARAM_NAME_SIZE = 64;
	typedef struct {
		int type;
		char name[PARAM_NAME_SIZE];
		union {
			float x;
			vec3_t v3;
			vec4_t v4;
		} value;

	} parameter_t;

void parameter_init();
parameter_t *parameter_get(const char* name, int type, const char *options);
void parameter_bind(render_command_buffer *rcb, parameter_t *parameter, program_t *program, const char* name);
void parameter_shutdown();

#endif /* parameters_hpp */
