//
//  render.cpp
//  visuals2016mac
//
//  Created by Niccolò on 04/01/16.
//  Copyright © 2016 sonictooth. All rights reserved.
//

#include <stdio.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <math.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <memory.h>
#include "log.h"
#include "file.h"
#include "gfx.h"
#include "render.h"


using namespace glm;

extern int g_dumpRenderBuffer;

static GLuint _fsQuadVao = 0;
static GLuint _fsQuadVbo = 0;

void render_init() {
    vec2 quad[] = { { -1.0f, -1.0f },
        { -1.0f, 1.0f },
        { 1.0f, 1.0f },
        { 1.0f, -1.0f } };
    
    glGenBuffers(1, &_fsQuadVbo);
    glBindBuffer(GL_ARRAY_BUFFER, _fsQuadVbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quad), quad, GL_STATIC_DRAW);
    
    glGenVertexArrays(1, &_fsQuadVao);
    glBindVertexArray(_fsQuadVao);
    glEnableVertexAttribArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, _fsQuadVbo);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
}

void render_command_buffer_reset(render_command_buffer *rcb) {
    memset(rcb->buffer, 0, sizeof(rcb->buffer));
    rcb->pointer = rcb->buffer;
}

void render_command_buffer_push_int(render_command_buffer *rcb, int value) {
    (*((int*)rcb->pointer)) = value;
    rcb->pointer += sizeof(value);
}

void render_command_buffer_push_float(render_command_buffer *rcb, float value) {
    (*((float*)rcb->pointer)) = value;
    rcb->pointer += sizeof(value);
}

void render_command_buffer_push_pointer(render_command_buffer *rcb, const void* pointer) {
    (*(const void**)rcb->pointer) = pointer;
    rcb->pointer += sizeof(pointer);
}

void render_command_bind_framebuffer(render_command_buffer *rcb, const framebuffer_t *framebuffer) {
    render_command_buffer_push_int(rcb, RENDER_COMMAND_BIND_FRAMEBUFFER);
    render_command_buffer_push_pointer(rcb, framebuffer);
}

void render_command_bind_program(render_command_buffer *rcb, const program_t *program) {
    render_command_buffer_push_int(rcb, RENDER_COMMAND_BIND_PROGRAM);
    render_command_buffer_push_pointer(rcb, program);
}

void render_command_bind_uniform1i(render_command_buffer *rcb, const program_t *program, const char* uniformName, int i) {

    render_command_buffer_push_int(rcb, RENDER_COMMAND_BIND_UNIFORM_INT);
    render_command_buffer_push_int(rcb, program_get_uniform_location(program, uniformName));
    render_command_buffer_push_int(rcb, i);
}


void render_command_bind_uniform1f(render_command_buffer *rcb, const program_t *program, const char* uniformName, float x) {
    render_command_buffer_push_int(rcb, RENDER_COMMAND_BIND_UNIFORM_FLOAT);
    render_command_buffer_push_int(rcb, program_get_uniform_location(program, uniformName));
    render_command_buffer_push_float(rcb, x);
}


void render_command_bind_uniform2f(render_command_buffer *rcb, int location, float x, float y) {
    render_command_buffer_push_int(rcb, RENDER_COMMAND_BIND_UNIFORM_FLOAT2);
    render_command_buffer_push_int(rcb, location);
    render_command_buffer_push_float(rcb, x);
    render_command_buffer_push_float(rcb, y);
}

void render_command_bind_uniform2f(render_command_buffer *rcb, const program_t *program, const char* uniformName, float x, float y) {
    render_command_buffer_push_int(rcb, RENDER_COMMAND_BIND_UNIFORM_FLOAT2);
    render_command_buffer_push_int(rcb, program_get_uniform_location(program, uniformName));
    render_command_buffer_push_float(rcb, x);
    render_command_buffer_push_float(rcb, y);
}
void render_command_bind_uniform3f(render_command_buffer *rcb, const program_t *program, const char* uniformName, float x, float y, float z) {
    render_command_buffer_push_int(rcb, RENDER_COMMAND_BIND_UNIFORM_FLOAT3);
    render_command_buffer_push_int(rcb, program_get_uniform_location(program, uniformName));
    render_command_buffer_push_float(rcb, x);
    render_command_buffer_push_float(rcb, y);
    render_command_buffer_push_float(rcb, z);
}

void render_command_bind_uniform4f(render_command_buffer *rcb, const program_t *program, const char* uniformName, float x, float y, float z, float w) {
    render_command_buffer_push_int(rcb, RENDER_COMMAND_BIND_UNIFORM_FLOAT4);
    render_command_buffer_push_int(rcb, program_get_uniform_location(program, uniformName));
    render_command_buffer_push_float(rcb, x);
    render_command_buffer_push_float(rcb, y);
    render_command_buffer_push_float(rcb, z);
    render_command_buffer_push_float(rcb, w);
}

void render_command_bind_texture(render_command_buffer *rcb, int target, const texture_t *texture) {
    render_command_buffer_push_int(rcb, RENDER_COMMAND_BIND_TEXTURE);
    render_command_buffer_push_int(rcb, target);
    render_command_buffer_push_pointer(rcb, texture);
}


void render_command_fullscreen_quad(render_command_buffer *rcb) {
    render_command_buffer_push_int(rcb, RENDER_COMMAND_RENDER_FULLSCREEN_QUAD);
}


static float get_float(uint8_t **buf) {
    float res = *((float*)*buf);
    *buf += sizeof(float);
    return res;
}

static int get_int(uint8_t **buf) {
    int res = *((int*)*buf);
    *buf += sizeof(int);
    return res;
}

static void* get_pointer(uint8_t **buf) {
    void *res = *((void**)*buf);
    *buf += sizeof(void*);
    return res;
}

static int _doDump = 0;

void render_command_buffer_render(render_command_buffer *rcb) {
    uint8_t *current = rcb->buffer;
    if(g_dumpRenderBuffer || _doDump) {
        render_command_buffer_dump(rcb);
    }
    
    while(current < rcb->pointer) {
        program_t *p;
        texture_t *t;
        framebuffer_t *f;
		int command = get_int(&current);
		int i1, i2, i3, i4;
		float f1, f2, f3, f4;
			switch(command) {
            case RENDER_COMMAND_RENDER_FULLSCREEN_QUAD:
                render_fullscreen_quad();
				break;
            case RENDER_COMMAND_BIND_PROGRAM:
                p = (program_t*) get_pointer(&current);
                glUseProgram(p->handle);
				break;
            case RENDER_COMMAND_BIND_UNIFORM_FLOAT:
				i1 = get_int(&current);
				f1 = get_float(&current);
                glUniform1f(i1, f1);
				break;
            case RENDER_COMMAND_BIND_UNIFORM_FLOAT2:
				i1 = get_int(&current);
				f1 = get_float(&current);
				f2 = get_float(&current);
				glUniform2f(i1, f1, f2);
				break;
            case RENDER_COMMAND_BIND_UNIFORM_FLOAT3:
				i1 = get_int(&current);
				f1 = get_float(&current);
				f2 = get_float(&current);
				f3 = get_float(&current);
				glUniform3f(i1, f1, f2, f3);
				break;
            case RENDER_COMMAND_BIND_UNIFORM_FLOAT4:
				i1 = get_int(&current);
				f1 = get_float(&current);
				f2 = get_float(&current);
				f3 = get_float(&current);
				f4 = get_float(&current);
				glUniform4f(i1, f1, f2, f3, f4);
				break;
            case RENDER_COMMAND_BIND_UNIFORM_INT:
				i1 = get_int(&current);
				i2 = get_int(&current);
				glUniform1i(i1, i2);
				break;
            case RENDER_COMMAND_BIND_TEXTURE:
                glActiveTexture(GL_TEXTURE0 + get_int(&current));
                t = (texture_t*)get_pointer(&current);
                glBindTexture(GL_TEXTURE_2D, t ? t->texId : 0);
				break;
            case RENDER_COMMAND_BIND_FRAMEBUFFER:
                f = (framebuffer_t*)get_pointer(&current);
                glBindFramebuffer(GL_FRAMEBUFFER, f ? f->fbId: 0);
                if (f) {
					glViewport(0, 0, f->texture->width, f->texture->height);
				}
				else {
#ifdef _WIN32
                    glViewport(0, 0, gfx_get_render_settings()->fbWidth, gfx_get_render_settings()->fbHeight);
#else
                    
                    glViewport(0, 0, gfx_get_render_settings()->fbWidth, gfx_get_render_settings()->fbHeight);
#endif
                }
				break;
            default:
                log_error("unimplemented render command: %d", command);
                break;
        }


    }
    assert(!glGetError());
	glFlush();
}

void render_command_buffer_dump(render_command_buffer *rcb) {
    uint8_t *current = rcb->buffer;
    log_debug("########RENDER_COMMAND_BUFFER_START########");
    program_t *p = NULL;
    texture_t *t = NULL;
    framebuffer_t *f = NULL;
    int textureUnit = 0;
	int i1, i2;
	float f1, f2, f3, f4;
    while(current < rcb->pointer) {
        switch(int command = get_int(&current)) {
            case RENDER_COMMAND_RENDER_FULLSCREEN_QUAD:
                log_debug("RENDER_COMMAND_RENDER_FULLSCREEN_QUAD");
                break;
            case RENDER_COMMAND_BIND_PROGRAM:
                p = (program_t*) get_pointer(&current);
                log_debug("RENDER_COMMAND_RENDER_BIND_PROGRAM: %s %d", p->name, p->handle);
                break;
            case RENDER_COMMAND_BIND_UNIFORM_FLOAT:
				i1 = get_int(&current);
				f1 = get_float(&current);
				log_debug("RENDER_COMMAND_RENDER_BIND_UNIFORM_FLOAT: %s %f",
                          program_get_uniform_name_from_location(p, i1),
                          f1);
                break;
            case RENDER_COMMAND_BIND_UNIFORM_FLOAT2:
				i1 = get_int(&current);
				f1 = get_float(&current);
				f2 = get_float(&current);
				log_debug("RENDER_COMMAND_RENDER_BIND_UNIFORM_FLOAT2: %s %f %f",
                          program_get_uniform_name_from_location(p, i1),
                          f1, f2);
                break;
            case RENDER_COMMAND_BIND_UNIFORM_FLOAT3:
				i1 = get_int(&current);
				f1 = get_float(&current);
				f2 = get_float(&current);
				f3 = get_float(&current);
				log_debug("RENDER_COMMAND_RENDER_BIND_UNIFORM_FLOAT3: %s %f %f %f",
                          program_get_uniform_name_from_location(p, i1),
                          f1,
                          f2,
                          f3);
                break;
            case RENDER_COMMAND_BIND_UNIFORM_FLOAT4:
				i1 = get_int(&current);
				f1 = get_float(&current);
				f2 = get_float(&current);
				f3 = get_float(&current);
				f4 = get_float(&current);
				log_debug("RENDER_COMMAND_RENDER_BIND_UNIFORM_FLOAT4: %s %f %f %f %f",
                          program_get_uniform_name_from_location(p, i1),
                          f1,
                          f2,
                          f3,
                          f4);
                break;
            case RENDER_COMMAND_BIND_UNIFORM_INT:
				i1 = get_int(&current);
				i2 = get_int(&current);
				log_debug("RENDER_COMMAND_RENDER_BIND_UNIFORM_INT: %s %d",
                          program_get_uniform_name_from_location(p, i1),
                          i2);
                break;
            case RENDER_COMMAND_BIND_TEXTURE:
                textureUnit = get_int(&current);
                t = (texture_t*)get_pointer(&current);
                log_debug("RENDER_COMMAND_RENDER_BIND_TEXTURE toTarget: %d %s %d", textureUnit, t ? t->name : "<null texture>", t ? t->texId : 0);
                break;
            case RENDER_COMMAND_BIND_FRAMEBUFFER:
                f = (framebuffer_t*)get_pointer(&current);
                
                log_debug("RENDER_COMMAND_RENDER_BIND_FRAMEBUFFER %s %d", f ? f->texture->name : "<screen>", f ? f->fbId : 0);
                break;
            default:
                log_error("unimplemented render command: %d", command);
                break;
        }
    }
    log_debug("########RENDER_COMMAND_BUFFER_END########");
}

void render_fullscreen_quad() {
    glBindVertexArray(_fsQuadVao);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}


