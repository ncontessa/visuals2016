//
//  parameters.cpp
//  visuals2016mac
//
//  Created by Niccolò on 10/01/16.
//  Copyright © 2016 sonictooth. All rights reserved.
//

#include <AntTweakBar.h>
#include "gfx.h"
#include "parameters.h"
#include "log.h"

const int MAX_PARAMETERS = 128;
static parameter_t _parameters[MAX_PARAMETERS];
static int _numParameters = 0;
static TwBar *_bar = NULL;

void parameter_init() {
    _bar = TwNewBar("Parameters");
    if(!_bar) {
        log_fatal("TwNewBar returned NULL");
    }
}

parameter_t *parameter_get(const char* name, int type, const char *options) {
    assert(_bar);
    assert(_numParameters < MAX_PARAMETERS);
    
    for(int i = 0; i < _numParameters; i++) {
        if(!strcmp(name, _parameters[i].name) && type == _parameters[i].type) {
            return &(_parameters[i]);
        }
    }
    
    TwType twType;
    switch(type) {
        case PARAM_FLOAT:
            twType = TW_TYPE_FLOAT;
            break;
        case PARAM_COLOR3F:
            twType = TW_TYPE_COLOR3F;
            break;
        case PARAM_COLOR4F:
            twType = TW_TYPE_COLOR4F;
            break;
        case PARAM_DIRECTION3F:
            twType = TW_TYPE_DIR3F;
            break;
        default:
            assert(0);
            break;
    }
    parameter_t *result = &_parameters[_numParameters];
    if(!TwAddVarRW(_bar, name, twType, &result->value, options)) {
        log_fatal("TwAddVarRW failed");
        return NULL;
    }
    
    result->type = type;
    strncpy(result->name, name, sizeof(result->name));
    _numParameters++;
    return result;
}

void parameter_bind(render_command_buffer *rcb, parameter_t *parameter, program_t *program, const char* name) {
    assert(parameter->type);
    switch(parameter->type) {
        case PARAM_FLOAT:
            render_command_bind_uniform1f(rcb, program, name, parameter->value.x);
            break;
        case PARAM_DIRECTION3F:
            render_command_bind_uniform3f(rcb, program, name, parameter->value.v3.x, parameter->value.v3.y, parameter->value.v3.z);
            break;
        case PARAM_COLOR3F:
            render_command_bind_uniform3f(rcb, program, name, parameter->value.v3.x, parameter->value.v3.y, parameter->value.v3.z);
            break;
        case PARAM_COLOR4F:
            render_command_bind_uniform4f(rcb, program, name, parameter->value.v4.x, parameter->value.v4.y, parameter->value.v4.z, parameter->value.v4.w);
            break;
        default:
            assert(0);
            break;
    }
    
}

void parameter_shutdown() {
    memset(_parameters, 0, sizeof(_parameters));
    _numParameters = 0;
    TwDeleteBar(_bar);
    _bar = NULL;
    
}
