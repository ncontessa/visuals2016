
dofile("tableprint.lua")

function make_node(type, subtype, inputs)
   local result = {}
   result.type = type
   result.subtype = subtype
   result.inputs = inputs or {}
   return result
end

function make_render_node(inputs)
   local result = make_node("render", "quad", inputs)
   return result
end

function quad(program_name, inputs)
   local result = make_render_node(inputs)
   result.subtype = "quad"
   result.program = vsl.program_get("data/shaders/"..program_name)
   result.name = program_name
   return result
end


function make_function_node(fun, inputs)
   local result =  make_node("var", "function", inputs)
   result.eval_function = fun
   return result
end

var_f = make_function_node

function make_constant_node(value)
   local result = make_node("var", "function", {})
   result.eval_function = function () return value end
   return result
end

var_k = make_constant_node

function make_midi_node(note, channel, inputs)
   local result = make_node("var", "midi", inputs)
   result.note = note
   result.channel = channel
   result.lastOn = -1.0
   result.lastOff = -1.0
end

var_m = make_midi_node

function add(a, b)
   return a + b
end

function eval_var(var_node)

   if(type(var_node) == "function") then
      return var_node()
   end

   if(type(var_node) == "number") then
      return var_node
   end

   if(type(var_node) == "table" and var_node.type == nil) then
      return var_node
   end
   
   if(var_node.eval_value) then
      return var_node.eval_value
   end

   if(var_node.parameter) then
      return vsl.parameter_get(var_node.parameter)
   end

   local arguments = {}
   for i, child in ipairs(var_node.inputs) do
      table.insert(arguments, eval_var(child))
   end

   if(var_node.subtype == "function") then
      var_node.eval_value = var_node.eval_function(table.unpack(arguments))
      return var_node.eval_value
   elseif(var_node.subtype == "midi") then
      return error("MIDI UNIMPLEMENTED")
   end
end

function is_render_node(node)
   if type(node) == "table" and (node.type == "render" or node.type == "texture") then
      return true
   else
      return false
   end
end

function render_walk_node(node, root)
   if(node.type == "render") then
      -- first of all collect inputs and render them...
      local result_tex = {}
      local result_vars = {}
      for key, value in pairs(node.inputs) do
	 if is_render_node(value) then
	       table.insert(result_tex, {tex = render_walk_node(value), uniformName = key})
	 else
	    result_vars[key] = eval_var(value)
	 end
      end

      -- then render this node...
      if(node.program) then
	 vsl.bind_program(node.program)
      end
      
      for i, tex in ipairs(result_tex) do
	 vsl.bind_texture(i - 1, tex.tex)
	 vsl.bind_uniform_int(node.program, tex.uniformName, i - 1)
      end

      for k,v in pairs(result_vars) do
	 vsl.bind_uniform(node.program, k, v)
      end

      local fb = nil
      if(root) then
	 vsl.bind_framebuffer(vsl.framebuffer_default())
      else
	 fb = vsl.framebuffer_get()
	 vsl.bind_framebuffer(fb)
      end
      
      if(node.subtype == "quad") then
	 vsl.render_fullscreen_quad()
      end

      if(root) then
	 return nil
      else
	 return vsl.texture_from_framebuffer(fb)
      end
      
   elseif(node.type == "texture") then
      return node.texture
   elseif(node.type == "simulation") then
      print("running simulation..")
      if(#node.inputs ~= 1) then error("simulation nodes only can have 1 input!") end
      return render_walk_node(node.inputs[0])
   else
      tprint(node)
      error("Unimplemented node!")
   end
end

function walk_node_by_type(node, node_type, fun)

   if(type(node) ~= "table") then
      return
   end

   if(node.type == node_type) then
      fun(node)
   end

   if(node.inputs) then
      for k, v in pairs(node.inputs) do
	 walk_node_by_type(v, node_type, fun)
      end
   end
end

function make_texture_node(filename)
   local result = {}
   result.name = filename
   result.type = "texture"
   result.inputs = {}
   result.texture, result.width, result.height = vsl.texture_get(filename)
   return result
end

tex = make_texture_node

function make_parameter_node(name, par_type, options)
   local result = {}
   result.type = "var"
   result.parameter = vsl.parameter_create(name, par_type, options)
   result.subtype = "var"
   return result
end

par = make_parameter_node

function par_f(func, par)
   local result = var_f(func)
   result.input = {par}
   return result
end


iResolution = function() return _iResolution end
global_time = var_f(function () return iGlobalTime end)
clouds = quad("clouds", { iChannel0=tex("data/tex16.png"), iResolution=iResolution,
				       iGlobalTime = global_time, dir=par("direction", "dir3f")})

color_ = quad("fixedcolor", {color = par("color", "color3f")})


local input_texture = tex("data/textures/space.jpg")
local input_ratio = input_texture.width / input_texture.height

function blur(input)
   local pass1 = quad("blur", {input0 = input, direction = 0, iResolution = iResolution})
   local pass2 = quad("blur", {input0 = pass1, direction = 1, iResolution = iResolution})
   return pass2
end


local ratio = var_f(function (zoom) return { 1.0 / zoom * (_iResolution[1] / _iResolution[2]) * (1.0 / input_ratio), (1.0 / zoom)} end, {par("zoom", "float", "min=1.0 max=10.0 step = 0.1")})

local vec2_zero = {0, 0}
local blit = quad("blit", {tOffset = vec2_zero,  tScale = ratio, input0 = input_texture})
-- local threshold = par("threshold", "float", "min=0.0 max=1.0 step=0.01")
par_freq = par("freq", "float")

local select = quad("select_by_intensity", {threshold = 0, input0 = blit})
local gaussian = blur(select)
local flicker = var_f(function(freq) return (math.sin(iGlobalTime * freq) + 0.0) * 1.0 end, {par_freq})
graph = quad("blend", {input0 = blit, input1 = gaussian, blend0 = 1.0, blend1 = flicker})


--graph = quad("data/shaders/blend", {input0=tex("data/tex2.png"), input1=clouds, blend = var_f(function () return (math.sin(iGlobalTime) + 1 ) * 0.5 end)})




-- graph = quad("data/shaders/blend", {input0=clouds, input1=color_, blend = par("blend", "float", "min=0.0 max=1.0 step=0.01")})
--graph = quad("blend", {input0=select, input1=color_,
--		       blend0 = 1.0,
--		      blend1 = 1.0})


function render(time, render_settings)

   _iResolution = {render_settings.virtualWidth, render_settings.virtualHeight}
   
   iGlobalTime = time
   walk_node_by_type(graph, "var", function (node) node.eval_value = nil end)
   vsl.reset_frame()
   render_walk_node(graph, true)
   vsl.render()
end





