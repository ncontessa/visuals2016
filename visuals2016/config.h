#ifndef __CONFIG_H__
#define __CONFIG_H__

typedef struct {
	int virtualWidth;
	int virtualHeight;
	bool forceVirtual;
	int fbWidth;
	int fbHeight;
	bool showUi;
	bool fullscreen;
	int monitor;
	bool dumpMidi;
	int midiInput;
	int defaultScene;
	int programChangeChannel;
	bool loaded;
	int effectiveWidth;
	int effectiveHeight;

} config_t;
const config_t *script_get_config();

#endif // __CONFIG_H__