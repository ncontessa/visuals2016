#ifndef __MIDI_H__
#define __MIDI_H__

#include <stdint.h>

int midi_init();

int midi_open_device(int deviceNumber);

int midi_frame_reset();

const int MIDI_STATUS_NOTE_OFF = 0x80;
const int MIDI_STATUS_NOTE_ON = 0x90;
const int MIDI_STATUS_PROGRAM_CHANGE = 0xc0;

typedef struct {
	double timestamp;
	uint32_t midiData;
} midiEvent_t;

int midi_get_frame_data(const midiEvent_t **events);
void midi_force_program_change(int program, int channel);
void midi_force_note(int note, int channel);
int midi_shutdown();


#endif