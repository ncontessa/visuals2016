#ifdef _WIN32
#include <windows.h>
#endif
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <time.h>


#include "log.h"


static FILE *flog = NULL;
static void _log_output_string(const char* string) {
#ifdef _WIN32
	OutputDebugStringA(string);
#endif
	printf("%s", string);
	if (flog) {
		fprintf(flog, "%s", string);
		fflush(flog);
	}
}

static const char* _log_get_time() {
	static char timeBuffer[128];
	time_t currentTime;
	struct tm* timeInfo;
	time(&currentTime);

	timeInfo = localtime(&currentTime);
	strftime(timeBuffer, sizeof(timeBuffer), "%Y-%m-%d %H:%M:%S", timeInfo);
	return timeBuffer;
}

static void _log_format(const char* heading, const char* formatString, va_list args) {
	static char messageBuffer[16 * 1024];
	static char finalBuffer[16 * 1024];
	vsnprintf(messageBuffer, sizeof(messageBuffer), formatString, args);
	snprintf(finalBuffer, sizeof(finalBuffer),  "%s %s: %s\n", _log_get_time(), heading, messageBuffer);
	_log_output_string(finalBuffer);
}

void log_info(const char* formatString, ...) {
	va_list argptr;
	va_start(argptr, formatString);
	_log_format("INFO", formatString, argptr);
	va_end(argptr);
}

void log_warning(const char* formatString, ...) {
	va_list argptr;
	va_start(argptr, formatString);
	_log_format("WARNING", formatString, argptr);
	va_end(argptr);
}

void log_error(const char* formatString, ...) {
	va_list argptr;
	va_start(argptr, formatString);
	_log_format("ERROR", formatString, argptr);
	va_end(argptr);
}

void log_fatal(const char* formatString, ...) {
	va_list argptr;
	va_start(argptr, formatString);
	_log_format("FATAL", formatString, argptr);
	va_end(argptr);
    exit(1);
}

void log_debug(const char* formatString, ...) {
	va_list argptr;
	va_start(argptr, formatString);
	_log_format("DEBUG", formatString, argptr);
	va_end(argptr);
}

void log_init(const char* filename) {
	flog = fopen(filename, "wt+");
}

void log_close() {
	fflush(flog);
	fclose(flog);
	flog = NULL;
}