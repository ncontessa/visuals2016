// visuals2016.cpp : definisce il punto di ingresso dell'applicazione.
//

#include <stdio.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <math.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <time.h>
#include <memory.h>
#include <AntTweakBar.h>
#ifdef _WIN32
#include <windows.h>
#else
#import <IOKit/pwr_mgt/IOPMLib.h>
#endif
#include "log.h"
#include "gfx.h"
#include "file.h"
#include "screenshot.h"
#include "render.h"
#include "parameters.h"
#include "script.h"
#include "midi.h"
#include "config.h"

using namespace glm;

// Globals!!
const double SCRIPT_RELOAD_WAIT_TIME = 1.0;
int g_dumpRenderBuffer = 0;
int g_showUI = 1;
int g_vsync = 1;
bool g_doScript = true;
double g_lastReloadTime;


// WES ANDERSON fatto
// PERDONA E DIMENTICA
// Coppie rifare

// AURORA splash di batteria?
// FINIRA' some kind of tunnel

// bond rifare
// sfortuna invertire i colori
// test screen

// AURORA (intro piano vox, poi band)
// QUESTO NOSTRO GRANDE AMORE
// WES ANDERSON
// I PARIOLINI DI 18 ANNI
// LE COPPIE o HIPSTERIA o ASPERGER
// NON C'E' NIENTE DI TWEE
// PROTOBODHISATTVA
// UNA COSA STUPIDA
// SPARIRE (solo)
// CALABI YAU (intro piano vox, poi full band)
// COME VERA NABOKOV
// POST PUNK
// IL POSTO PIU' FREDDO
// SFORTUNA
// NON FINIRA'
// FINIRA'

// CORSO TRIESTE (intro piano vox, poi full band)
// PERDONA E DIMENTICA
// VELLEITA'
// LEXOTAN

// REQUIRED

// FIGATE
// detailed stats?? ARB_timer_query
// better dumping of textures etc?
// better default values for tw positioning & sizing

void try_reload_script() {
    g_lastReloadTime = glfwGetTime();
    log_info("Reloading scripts...");
    if(script_load_assets()) {
        g_doScript = true;
	}
	else {
		g_doScript = false;
	}
    
}


void set_size(GLFWwindow *win) {
    int w, h;
    glfwGetFramebufferSize(win, &w, &h);
    int ww, wh;
    glfwGetWindowSize(win, &ww, &wh);
    TwWindowSize(w, h);
    gfx_reset(w, h, ww, wh);
}


void mousebutton_callback(GLFWwindow *win, int button, int action, int mods) {
    TwEventMouseButtonGLFW(button, action);
}


static int32_t _currentProgram;
void TW_CALL set_program(const void *value, void *clientData)
{
	_currentProgram = *((int32_t*)(value));
	midi_force_program_change(_currentProgram, script_get_config()->programChangeChannel);
}

void TW_CALL get_program(void *value, void *clientData)
{
	*((int32_t*)(value)) = _currentProgram;
}

static int32_t _currentChannel = 16;
static char _currentNote[4] = "C1";

int note_name_to_number(const char* noteName) {
    int result = -1;
    size_t length = strlen(noteName);
    
    if(length < 2 || length > 3) {
        return -1;
    }
    
    
    switch(noteName[0]) {
            case 'C':
            result = 0;
            break;
            case 'D':
            result = 2;
            break;
            case 'E':
            result = 4;
            break;
            case 'F':
            result = 5;
            break;
            case 'G':
            result = 7;
            break;
            case 'A':
            result = 9;
            break;
            case 'B':
            result = 11;
            break;
        default:
            return -1;
    }
    
    if(length == 3) {
        if(noteName[1] == '#') {
            result += 1;
        } else if(noteName[1] == 'b') {
            result -= 1;
        } else {
            return -1;
        }
    }

    
    char octave = ((length == 3) ? noteName[2] : noteName[1]);
    if(octave < '0' || octave > '9') {
        return -1;
    }
    
    octave -= '0';
    result += (octave + 2) * 12;
    return result;
    
}

void TW_CALL set_note(const void *value, void *clientData)
{
    const char* src = (const char*) value;
    strncpy(_currentNote, src, sizeof(_currentNote));
    _currentNote[sizeof(_currentNote) - 1] = '\0';
    int note = note_name_to_number(_currentNote);
    if(note >= 0) {
        midi_force_note(note, _currentChannel);
    }
}

void TW_CALL get_note(void *value, void *clientData)
{
    char* dest = (char*) value;
    strncpy(dest, _currentNote, sizeof(_currentNote));
    dest[sizeof(_currentNote) - 1] = '\0';
}



int TW_CALL MyEventKeyGLFW(int glfwKey, int glfwAction)
{
    int handled = 0;
    int g_KMod = 0;
    

    // Process key pressed
    int k = 0;
    if( glfwAction==GLFW_PRESS )
    {
        int mod = g_KMod;
        int testkp = ((mod&TW_KMOD_CTRL) || (mod&TW_KMOD_ALT)) ? 1 : 0;
        switch( glfwKey )
            {
                    case GLFW_KEY_ESCAPE:
                        k = TW_KEY_ESCAPE;
                        break;
                    case GLFW_KEY_UP:
                        k = TW_KEY_UP;
                        break;
                    case GLFW_KEY_DOWN:
                        k = TW_KEY_DOWN;
                        break;
                    case GLFW_KEY_LEFT:
                        k = TW_KEY_LEFT;
                        break;
                    case GLFW_KEY_RIGHT:
                        k = TW_KEY_RIGHT;
                        break;
                    case GLFW_KEY_TAB:
                        k = TW_KEY_TAB;
                        break;
                    case GLFW_KEY_ENTER:
                        k = TW_KEY_RETURN;
                        break;
                    case GLFW_KEY_BACKSPACE:
                        k = TW_KEY_BACKSPACE;
                        break;
                    case GLFW_KEY_INSERT:
                        k = TW_KEY_INSERT;
                        break;
                    case GLFW_KEY_DELETE:
                        k = TW_KEY_DELETE;
                        break;
                    case GLFW_KEY_PAGE_UP:
                        k = TW_KEY_PAGE_UP;
                        break;
                    case GLFW_KEY_PAGE_DOWN:
                        k = TW_KEY_PAGE_DOWN;
                        break;
                    case GLFW_KEY_HOME:
                        k = TW_KEY_HOME;
                        break;
                    case GLFW_KEY_END:
                        k = TW_KEY_END;
                        break;
                    case GLFW_KEY_KP_ENTER:
                        k = TW_KEY_RETURN;
                        break;
                    case GLFW_KEY_KP_DIVIDE:
                        if( testkp )
                            k = '/';
                        break;
                    case GLFW_KEY_KP_MULTIPLY:
                        if( testkp )
                            k = '*';
                        break;
                    case GLFW_KEY_KP_SUBTRACT:
                        if( testkp )
                            k = '-';
                        break;
                    case GLFW_KEY_KP_ADD:
                        if( testkp )
                            k = '+';
                        break;
                    case GLFW_KEY_KP_DECIMAL:
                        if( testkp )
                            k = '.';
                        break;
                    case GLFW_KEY_KP_EQUAL:
                        if( testkp )
                            k = '=';
                        break;
                }
            }
            
            if( k>0 )
                handled = TwKeyPressed(k, 0);
    return handled;
}


void command_input(GLFWwindow *win, int key, int action, int mods) {
    if(action == GLFW_RELEASE){
        return;
    }
    
	if (key == GLFW_KEY_C && !mods) {
		midi_force_note(note_name_to_number("C1"), 16);
	} else if (key == GLFW_KEY_D && !mods) {
		midi_force_note(note_name_to_number("D1"), 16);
	}
	else if (key == GLFW_KEY_E && !mods) {
		midi_force_note(note_name_to_number("E1"), 16);
	}
	else if (key == GLFW_KEY_F && !mods) {
		midi_force_note(note_name_to_number("F1"), 16);
	}
	else if (key == GLFW_KEY_G && !mods) {
		midi_force_note(note_name_to_number("G1"), 16);
	}
	else if (key == GLFW_KEY_A && !mods) {
		midi_force_note(note_name_to_number("C1"), 13);
	}
	else if (key == GLFW_KEY_K && !mods) {
		midi_force_note(0, 5);
	}
	else if (key == GLFW_KEY_S && !mods) {
		midi_force_note(1, 5);
	}
	else if (key == GLFW_KEY_H && !mods) {
		midi_force_note(3, 5);
	}
	else if (key == GLFW_KEY_P && !mods) {
		midi_force_note(5, 5);
	}
	else if (key == 47 && !mods) {
		_currentProgram--;
		midi_force_program_change(_currentProgram, script_get_config()->programChangeChannel);
	}
	else if (key == 93 && !mods) {
		_currentProgram++;
		midi_force_program_change(_currentProgram, script_get_config()->programChangeChannel);
	}
	else if(key == GLFW_KEY_ESCAPE) {
        glfwSetWindowShouldClose(win, 1);
    } else if(key == GLFW_KEY_D && mods & GLFW_MOD_CONTROL) {
        g_dumpRenderBuffer = 1;
    } else if(key == GLFW_KEY_V && mods & GLFW_MOD_CONTROL) {
        g_vsync = !g_vsync;
        glfwSwapInterval(g_vsync);
    } else if(key == GLFW_KEY_U && mods & GLFW_MOD_CONTROL) {
        g_showUI = !g_showUI;
		if (g_showUI) {
			glfwSetInputMode(win, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		}
		else {
			glfwSetInputMode(win, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
		}

    } else if(key == GLFW_KEY_R && mods & GLFW_MOD_CONTROL) {
        try_reload_script();
    }

}

void key_callback(GLFWwindow *win, int key, int scancode, int action, int mods) {
    command_input(win, key, action, mods);
    MyEventKeyGLFW(key, action);
}

void char_callback(GLFWwindow *win, unsigned int codePoint) {
    TwEventCharGLFW(codePoint, GLFW_PRESS);
    TwEventCharGLFW(codePoint, GLFW_RELEASE);
}

void cursor_callback(GLFWwindow *win, double x, double y) {
    const gfx_render_settings *settings = gfx_get_render_settings();
    double hRatio = (double)settings->fbWidth / (double)settings->windowWidth;
    double vRatio = (double)settings->fbHeight / (double)settings->windowHeight;
    TwMouseMotion(x * hRatio, y * vRatio);
}

void scroll_callback(GLFWwindow *win, double x, double y) {
    if(y == 0.0f) {
        return;
    }
    TwMouseWheel(y > 0.0f ? 1 : -1);
}

void error_callback(int error, const char* description) {
	log_error("GLFW error: %d %s", error, description);
}

void framebuffersize_callback(GLFWwindow* win, int width, int height) {
    set_size(win);
}


void resize_callback(GLFWwindow *win, int width, int height) {
    set_size(win);
}



int main(int argc, char** argv)
{

    
    
	log_init("visuals2016.txt");
#ifdef _WIN32
    SetThreadExecutionState(ES_SYSTEM_REQUIRED | ES_DISPLAY_REQUIRED);
#else
    
    //  NOTE: IOPMAssertionCreateWithName limits the string to 128 characters.
    CFStringRef reasonForActivity= CFSTR("visuals 2016");
    
    IOPMAssertionID assertionID;
    IOReturn success = IOPMAssertionCreateWithName(kIOPMAssertionTypeNoDisplaySleep,
                                                   kIOPMAssertionLevelOn, reasonForActivity, &assertionID);
    if (success == kIOReturnSuccess)
    {
        log_info("succesfully preventing sleep");
    }
#endif
    glfwInit();
	log_info("Started...");
	if (!script_init()) {
		exit(-1);
	}

#if 0
    const int SIZE = 32;
    uint8_t image[SIZE * SIZE * SIZE * 3];
    const float EXPONENT = 1.0;
    float r = 0.0f, g = 0.0f, b = 0.0f;

    for(int i = 0; i < SIZE; i++) {
        b = powf((1.0f / (float)SIZE) * (float)i, EXPONENT);
        for(int j = 0; j < SIZE; j++) {
            g = powf((1.0f / (float)SIZE) * (float)j, EXPONENT);
            for(int k = 0; k < SIZE; k++) {
                r = powf((1.0f / (float)SIZE) * (float)k, EXPONENT);
                
                int index = (k * 3) + (j * SIZE * 3) + (i * SIZE * SIZE * 3);
                image[index + 0] = (uint8_t)(r * 255.0f);
                image[index + 1] = (uint8_t)(g * 255.0f);
                image[index + 2] = (uint8_t)(b * 255.0f);
                
            }
        }
    }
    
    stbi_write_tga("rgb.tga", SIZE*SIZE, SIZE, 3, image);
    
    
    return 1;
#endif
    
    
	// TODO: check errors here
#ifndef _WIN32
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif
//	glfwWindowHint(GLFW_SRGB_CAPABLE, GL_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
	glfwWindowHint(GLFW_AUTO_ICONIFY, GL_FALSE);

	glfwSetErrorCallback(error_callback);

	if (!script_load_config()) {
		exit(-1);
	}

	if (!midi_init()) {
		exit(-1);
	}

	const config_t* config = script_get_config();
	_currentProgram = config->defaultScene;
	gfx_config(config->forceVirtual, config->virtualWidth, config->virtualHeight);
	g_showUI = config->showUi;

	if (config->midiInput >= 0) {
		if (!midi_open_device(config->midiInput)) {
			log_fatal("Error opening MIDI input #%d", config->midiInput);
			exit(-1);
		}
	}

	int monitorCount;
	GLFWmonitor ** monitors = glfwGetMonitors(&monitorCount);
	{
		for (int i = 0; i < monitorCount; i++) {
			log_info("Monitor %d, name %s", i, glfwGetMonitorName(monitors[i]));
			int vidmodeCount;
			const GLFWvidmode *vidmodes = glfwGetVideoModes(monitors[i], &vidmodeCount);
			if (vidmodes) {
				for (int j = 0; j < vidmodeCount; j++) {
					log_info("Mode %d: %d x %d x %d",
						j,
						vidmodes[j].width,
						vidmodes[j].height,
						vidmodes[j].blueBits + vidmodes[j].redBits + vidmodes[j].greenBits);
				}
			}
		}

	}

	GLFWwindow *win;
	// note: vidmodes are listed in screen coordinates, not pixels!

	if (config->fullscreen) {
		win = glfwCreateWindow(config->fbWidth, config->fbHeight, "Hello", monitors[config->monitor < monitorCount ? config->monitor : 0], NULL);
        
        
//        glfwSetWindowMonitor(win, monitors[config->monitor < monitorCount ? config->monitor : 0], 0, 0, config->fbWidth,
//                             config->fbHeight, 60);
	}
	else {
		win = glfwCreateWindow(config->fbWidth, config->fbHeight, "Hello", NULL, NULL);
		glfwSetWindowPos(win, 20, 20);
	}

	glfwMakeContextCurrent(win);
	glewExperimental = GL_TRUE;
	GLenum err = glewInit();
	glGetError();
	if (err != GLEW_OK) {
		log_fatal("Error initializing GLEW : %s", glewGetErrorString(err));
		return -1;
	}
	gfx_init();

	assert(!glGetError());
	assert(!glGetError());

	{
		int fbWidth, fbHeight;
		int windowWidth, windowHeight;
		glfwGetWindowSize(win, &windowWidth, &windowHeight);
		glfwGetFramebufferSize(win, &fbWidth, &fbHeight);
		int hRatio = fbWidth / windowWidth, vRatio = fbHeight / windowHeight;
		glfwSetWindowSize(win, fbWidth / hRatio, fbHeight / vRatio);
		set_size(win);
        log_info("fbWidth=%d fbHeight=%d windowWidth=%d windowHeight=%d", fbWidth,
                 fbHeight,
                 windowWidth,
                 windowHeight);
    }



    glfwSetFramebufferSizeCallback(win, framebuffersize_callback);
    glfwSetMouseButtonCallback(win, mousebutton_callback);
    glfwSetCursorPosCallback(win, cursor_callback);
    glfwSetScrollCallback(win, scroll_callback);
    glfwSetKeyCallback(win, key_callback);
    glfwSetCharCallback(win, char_callback);
    
    
    assert(!glGetError());

    assert(!glGetError());
	log_info("GL_VENDOR: %s", glGetString(GL_VENDOR));
    assert(!glGetError());
	log_info("GL_RENDERER: %s", glGetString(GL_RENDERER));
    assert(!glGetError());
	log_info("GL_VERSION: %s", glGetString(GL_VERSION));
    assert(!glGetError());
#if 0
	log_info("GL_EXTENSIONS: %s", glGetString(GL_EXTENSIONS));
#endif
    assert(!glGetError());
    set_size(win);
	if (gfx_get_render_settings()->fbWidth > gfx_get_render_settings()->windowWidth) {
		TwDefine(" GLOBAL fontscaling=3 fontsize=2"); // High-DPI (Retina)
	}
	else {
		TwDefine(" GLOBAL fontscaling=2 fontsize=2"); // Non-retina (Windows)
	}

	TwInit(TW_OPENGL_CORE, NULL);

	render_init();
    parameter_init();
    screenshot_init("visuals2016");

	if (!script_load_assets()) {
		exit(-1);
	}

    TwDefine(" GLOBAL fontsize=1");

    TwBar* bar = TwNewBar("Stats");
    char frametime[64] = "";
    TwAddVarRO(bar, "FrameTime", TW_TYPE_CSSTRING(64), &frametime, NULL);
	TwAddVarCB(bar, "Program", TW_TYPE_INT32, set_program, get_program, NULL, "min=0 max=127");
    TwAddVarRW(bar, "Channel", TW_TYPE_INT32, &_currentChannel, "min=1 max=16");
    TwAddVarCB(bar, "Note", TW_TYPE_CSSTRING(sizeof(_currentNote)), set_note, get_note, NULL, "");
    
	if (g_showUI) {
		glfwSetInputMode(win, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}
	else {
		glfwSetInputMode(win, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	}
    
    log_info("fbWidth=%d fbHeight=%d windowWidth=%d windowHeight=%d", gfx_get_render_settings()->fbWidth,
                                                                    gfx_get_render_settings()->fbHeight,
                                                                    gfx_get_render_settings()->windowWidth,
                                                                    gfx_get_render_settings()->windowHeight);
    
	while (!glfwWindowShouldClose(win)) {
        double startFrame = glfwGetTime();
        g_dumpRenderBuffer = 0;
		glfwPollEvents();
        framebuffer_reset();
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT);
		assert(!glGetError());
        if(g_doScript) {
          if(!script_frame(glfwGetTime(), gfx_get_render_settings())) {
                log_warning("Script errored, waiting %f seconds...", SCRIPT_RELOAD_WAIT_TIME);
                g_doScript = false;
                g_lastReloadTime = glfwGetTime();
            }
			

        } else {
            if((glfwGetTime() - g_lastReloadTime) > SCRIPT_RELOAD_WAIT_TIME) {
                try_reload_script();
            }
        }
		assert(!glGetError());

        if(g_showUI) {
            TwDraw();
			glGetError();
		}
		assert(!glGetError());
		glfwSwapBuffers(win);
		double endFrame = glfwGetTime();
		snprintf(frametime, sizeof(frametime), "%.2lf", (endFrame - startFrame) * 1000.0f);
		if ((endFrame - startFrame) * 1000.0 > 17.00) {
//			log_info("frametime: %.2f", (endFrame - startFrame) * 1000.0f);
		}

	}
    script_shutdown();
	glfwDestroyWindow(win);
	glfwTerminate();
    
#ifndef _WIN32
    // sleep again
    success = IOPMAssertionRelease(assertionID);
#endif
    
	log_close();
	return 0;
}

