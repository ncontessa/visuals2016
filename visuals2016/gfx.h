#ifndef __GFX_H__
#define __GFX_H__

#include <GL/glew.h>


const int MAX_NAME_SIZE = 64;
typedef struct {
	char name[MAX_NAME_SIZE];
	GLenum type;
	GLint location;
	GLint size;
} uniformDesc_t;

typedef uniformDesc_t attributeDesc_t;

const int PROGRAM_NAME_SIZE = 64;
const int PROGRAM_MAX_UNIFORMS = 16;
const int PROGRAM_MAX_ATTRIBUTES = 16;
typedef struct {
	char name[PROGRAM_NAME_SIZE];
	GLuint vertexShader;
	GLuint fragmentShader;
	GLuint handle;
	int nAttributes;
	attributeDesc_t attributes[PROGRAM_MAX_ATTRIBUTES];
	int nUniforms;
	uniformDesc_t uniforms[PROGRAM_MAX_UNIFORMS];
} program_t;

GLuint program_get_attrib_location(const program_t* program, const char* name);
GLuint program_get_uniform_location(const program_t* program, const char* name);
const char *program_get_uniform_name_from_location(const program_t *program, int location);
const char *program_get_attribute_name_from_location(const program_t *program, int location);
bool program_load(const char* path, program_t *result);
program_t* program_get(const char* path);
GLuint shader_load(const char* filename, GLenum shaderType);
void program_free(program_t* program);
void shader_free(GLuint shader);

const int MAX_TEXTURE_NAME = 64;
typedef struct {
    int width, height;
    int components;
    int filter, wrap;
    char name[MAX_TEXTURE_NAME];
    GLuint texId;
} texture_t;

enum {
    FILTER_NEAREST,
    FILTER_LINEAR,
    FILTER_MIPMAP
};

enum {
    WRAP_REPEAT,
    WRAP_CLAMP
};

void texture_alloc(texture_t* result, int width, int height, int components, int filter, int wrap);
void texture_upload_data(const texture_t *result, void *data);
void texture_upload_image_data(const texture_t *result, const char* filename);
texture_t *texture_get(const char *filename, int filter = FILTER_MIPMAP, int wrap = WRAP_REPEAT);
texture_t *texture_get(int width, int height, int components, int filter = FILTER_MIPMAP, int wrap = WRAP_REPEAT);

typedef struct {
    texture_t *texture;
    GLuint fbId;
} framebuffer_t;

void framebuffer_alloc(framebuffer_t *result, int width, int height, int components);
const framebuffer_t *framebuffer_get();
void framebuffer_reset();
const framebuffer_t *framebuffer_get_static(int width, int height, bool filterNearest);

typedef struct {
    int fbWidth, fbHeight;
    int windowWidth, windowHeight;
    int virtualWidth, virtualHeight;
    bool forceVirtual;
} gfx_render_settings;

const gfx_render_settings* gfx_get_render_settings();
void gfx_reset(int fbWidth, int fbHeight, int windowWidth, int windowHeight);
void gfx_init();
void gfx_config(bool forceVirtual, int virtualWidth, int virtualHeight);
void gfx_free_assets();
#endif